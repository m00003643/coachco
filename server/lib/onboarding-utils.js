'use strict';
var nodemailer = require('nodemailer'),
      shortid = require('shortid'),
      htmlToText = require('html-to-text'),
      conf = require('./conf');

exports.generateInviteCode = function generateInviteCode() {
    // ShortId gives us random keys, but our username field only allows lower case letters and numbers.
    // so use ShortId but alter the output to fit our needs...
    shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_');
    var tmp = 'i'+ shortid.generate(); // username must start with a letter not a number, so just force one
    var re = /-/gi;
    var tmp = tmp.replace(re, 'a');  // convert any - charactgers to a
    re = /_/gi;
    var tmp = tmp.replace(re, 'b'); // convert any _ characters to _
    return  tmp.toLowerCase();  // Convert entire thing to lower case and return...
}