'use strict';
var nodemailer = require('nodemailer'),
      htmlToText = require('html-to-text'),
      conf = require('./conf');

exports.sendnotificationmail = function sendnotificationmail(opts) { 
  /* OPTS will be a Json with these keys:
     protocol, baseUrl, email , inviteCode, firstName, lastName , coachFirstName, coachLastName, companyName, 
     reason, brandEmailLogo, emailTemplate

     reason will be one of 'welcomeEmail', 'notificationNewAssessment'
   */

  // Create the email text to send using the template provided, substituting in values from OPTS
  let textHtml = opts.emailTemplate;

  textHtml = textHtml.replace(/%BRAND-COMPANY%/gi, opts.companyName);
  textHtml = textHtml.replace(/%CLIENT-FIRSTNAME%/gi, opts.firstName);
  textHtml = textHtml.replace(/%CLIENT-LASTNAME%/gi, opts.lastName);
  textHtml = textHtml.replace(/%COACH-FIRSTNAME%/gi, opts.coachFirstName);
  textHtml = textHtml.replace(/%COACH-LASTNAME%/gi, opts.coachLastName);
  textHtml = textHtml.replace(/%CLIENT-EMAIL%/gi, opts.email);
  textHtml = textHtml.replace(/%INVITE-CODE%/gi, opts.inviteCode);
  textHtml = textHtml.replace(/%BRAND-EMAIL-LOGO%/gi, "<img src='" + opts.brandEmailLogo + "'/>" ); 
  textHtml = textHtml.replace(/%ACCEPT-URL%/gi, opts.protocol + '://' + opts.baseUrl + '/register');
  textHtml = textHtml.replace(/%BASE-URL%/gi, opts.protocol + '://' + opts.baseUrl );
  textHtml = textHtml.replace(/%LOGIN-URL%/gi, opts.protocol + '://' + opts.baseUrl + '/login');

  // Create a PlainText version of email text also...
  let textPlain = htmlToText.fromString(textHtml, {wordwrap: 130, tables: true, ignoreHref: true, ignoreImage: true});

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
      host: conf.mail.host,
      port: conf.mail.port,
      secure: conf.mail.secure,
      auth: {
        user: conf.mail.user,
        pass: conf.mail.pass
      },
      tls: {
        rejectUnauthorized: false // do not fail on invalid certs
      }
  });
  
  // Our subject line will depend on what the reason / purpose of this email is...
  let txtSubject = '';
  if (opts.reason == 'notificationNewAssessment') {
    txtSubject = 'New Assessment';    
  } else if (opts.reason == 'welcomeEmail') {
    txtSubject = 'Welcome to ' + opts.companyName;
  }

  // setup email data with unicode symbols
  let mailOptions = {
      from: '"' + opts.companyName + '" ' + conf.mail.replyaddress, // sender address
      to: opts.email, // list of receivers
      subject: txtSubject, // Subject line
      text: textPlain,
      html: textHtml
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
  });

}