var conf = require('./conf');

module.exports = require('knex')({
    client: conf.db.client,
    connection: {
        host: conf.db.host,
        user: conf.db.user,
        password: conf.db.password,
        database: conf.db.database,
        charset: conf.db.charset
    }
});
