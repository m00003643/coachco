const aws = require('aws-sdk'),
      path = require('path'),
      shortid = require('shortid'),
      conf = require('./conf');

/*
 * s3DeleteObjectByUrl
 * 
 * Attempts to remove an object from the CoachCo S3 storage bucket based on its fully qualified URL 
 * as it would be stored in the Assets table.
 * 
 */

exports.s3DeleteObjectByUrl = function s3DeleteObjectByUrl( Url ) { 
    let S3_BUCKET = conf.s3.uploadBucket;
    let S3_FOLDER = conf.s3.uploadFolder;

    // Our stored URL looks like this: https://coachco.s3.amazonaws.com/assets/4/SampleWheel.pdf
    // Trim off the front bit up until the uploadFolder location (ie, "assets") to get the key value
    // (ie, assets/4/SampleWheel.pdf)
    let S3_KEY =  Url.slice(Url.indexOf(S3_FOLDER), Url.length);

    var  s3 = new aws.S3();     

    var params = {
        Bucket: S3_BUCKET,
        Key: S3_KEY,
    };

    let retVal = true;
    s3.deleteObject(params, function(err, data) {
      if (err) { 
        retVal = false;
        // console.log(err, err.stack);
      }
    });
    return retVal;
}

exports.s3GetSignedUrl = async function s3GetSignedUrl( fName, fType, userId ) {  

    let fileName = fName;
    let fileType = fType;
    let curUser = userId;

    // Get S3 Bucketname from config (ie, 'coachco')
    let S3_BUCKET = conf.s3.uploadBucket;
    // Construct S3 'folder' as base value (from config, ie 'assets') / current user /
    let S3_FOLDER = conf.s3.uploadFolder + path.sep + curUser + path.sep;
    // Assign a unique filename for storage in S3... Uses random(ish) ShortId string plus original filename 
    // so SomeFile.PNG gets changed to B17KVmzkZ-SomeFile.PNG
    let S3_Key = S3_FOLDER + shortid.generate() + '-' + path.basename(fileName);

    var s3 = new aws.S3();

    const s3Params = {
        Bucket: S3_BUCKET,
        Key: S3_Key,
        ContentType: fileType,
        ACL: 'public-read-write',
        Expires: conf.s3.defaultExpires             // In seconds, allowing for time for the uplaod to COMPLETE...
    };

    var returnData = {};

    s3.getSignedUrl('putObject', s3Params, (err, data) => {
        if(err){ console.log(err); }

        returnData = {
          signedRequest: data,
          url: `https://${S3_BUCKET}.s3.amazonaws.com/${S3_Key}`
        };
    });
      
    return returnData;

}
