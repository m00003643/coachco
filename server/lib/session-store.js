const fs = require('fs'),
	session = require('express-session'),
	conf = require('./conf');

exports.getStore = function getStore() {
	const storeType = conf.app.session.type,
		storeOpts = conf.app.session.opts || {};

	switch (storeType) {
		case 'file':
			const FileStore = require('connect-file-store')({ session: session });

			const sessionDir = require('path').join(
				require('os').tmpdir(),
				storeOpts.directory || 'coachco-sessions'
			);

			if (!fs.existsSync(sessionDir)) {
				fs.mkdir(sessionDir);
			}

			return new FileStore({ path: sessionDir });

		case 'postgres':
			const KnexSessionStore = require('connect-session-knex')(session);

			return new KnexSessionStore({
				knex: require('./knex'),
				tablename: storeOpts.tablename // optional. Defaults to 'sessions'
			});

		default:
			throw new Error('Unrecognized session store type:', storeType);
	}
}