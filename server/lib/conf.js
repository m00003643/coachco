var argv = require('optimist').argv,
    DEFAULT_CONFIG = 'config.json';

var configFileName = argv.c || argv.config || DEFAULT_CONFIG;

console.log('Loading configurations from "%s"', configFileName);

var configPath = require('path').resolve(configFileName);

var conf = module.exports = require(configPath);

// let server port be overridden (used by Elastic Beanstalk)
conf.app.port = process.env.PORT || conf.app.port;