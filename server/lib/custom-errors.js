var customErrors = module.exports = {
    ResourceNotFoundError: ResourceNotFoundError,
    AuthenticationFailureInactiveUserError: AuthenticationFailureInactiveUserError,
    AuthenticationFailureError: AuthenticationFailureError,
    AuthenticationRequiredError: AuthenticationRequiredError,
    ResourceValidationError: ResourceValidationError,
    BadRequestError: BadRequestError
};

function AuthenticationFailureInactiveUserError(msg) {
    this.message = msg || 'Failed To Properly Authenticate The Request';
    this.stack = Error().stack;
}
AuthenticationFailureInactiveUserError.prototype = Object.create(Error.prototype);
AuthenticationFailureInactiveUserError.prototype.name = 'AuthenticationFailureInactiveUserError';

function AuthenticationFailureError(msg) {
    this.message = msg || 'Failed To Properly Authenticate The Request';
    this.stack = Error().stack;
}
AuthenticationFailureError.prototype = Object.create(Error.prototype);
AuthenticationFailureError.prototype.name = 'AuthenticationFailureError';

function AuthenticationRequiredError(msg) {
    this.message = msg || 'This action requires an authenticated user.';
    this.stack = Error().stack;
}
AuthenticationRequiredError.prototype = Object.create(Error.prototype);
AuthenticationRequiredError.prototype.name = 'AuthenticationRequiredError';

function ResourceNotFoundError(msg) {
    this.message = msg || 'The Specified Resource Does Not Exist';
    this.stack = Error().stack;
}
ResourceNotFoundError.prototype = Object.create(Error.prototype);
ResourceNotFoundError.prototype.name = 'ResourceNotFoundError';

function ResourceValidationError(msg, validationErrors) {
    this.message = msg || 'Resource Failed Validation';
    this.stack = Error().stack;
    this.validationErrors = validationErrors;
}
ResourceValidationError.prototype = Object.create(Error.prototype);
ResourceValidationError.prototype.name = 'ResourceValidationError';

function BadRequestError(msg) {
    this.message = msg || 'Bad Request';
    this.stack = Error().stack;
}
BadRequestError.prototype = Object.create(Error.prototype);
BadRequestError.prototype.name = 'BadRequestError';
