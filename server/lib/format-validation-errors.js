/**
 * Accepts an array of joi validation errors in the form of:
 *   [
 *     {
 *       message: 'error message',
 *       path: 'key1',
 *       type: '...',
 *       context: { ... }
 *     },
 *     {
 *       message: 'error message',
 *       path: 'key2',
 *       type: '...',
 *       context: { ... }
 *     },
 *     ...
 *   ]
 *
 * ...and returns a flattened object in the form of:
 *   {
 *     key1: 'error message',
 *     key2: 'error message'
 *   }
 */
module.exports = function(errors) {
  if (!errors) return {};

  const ret = {};

  errors.forEach(error => ret[error.path] = error.message);

  return ret;
};
