const  json2csv = require('json2csv');

exports.exportLog = function exportLog( format, logArray ) { 

  var result = '';
  if (format == 'csv') {
    // Loop thru our array of objects, remove keypairs we don't want to include in the export...
    for (i=0; i < logArray.length; i++) {
      delete logArray[i].created;
      delete logArray[i].updated;
      delete logArray[i].id;
    }
    result = json2csv({ data: logArray });
  } else if (format == 'pdf') {
    // TBD
    result = null;
  } 
  return result; 
}
