const
  express = require('express'),
  session = require('express-session'),
  http = require('http'),
  https = require('https'),
  fs = require('fs'),
  path = require('path'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  conf = require('./lib/conf');

const init = function init(cb) {

  // Listen for unhandled rejection events...
  /*
  process.on('unhandledRejection', (reason, p) => {
    if (reason.message.includes("ECONNREFUSED")) {
      console.log('WARNING - UNHANDLED REJECTION :: ', reason.message, ' -- Is the database server running?');
    } else {
      console.log('WARNING - UNHANDLED REJECTION :: ', reason);
    }
  });
  */
 
  const app = express();

  app.set('env', conf.app.environment);
  app.set('port', process.env.PORT || conf.app.port);
  app.set('views', './server/views');
  app.set('view engine', 'pug');
  app.set('trust proxy', true);



  app.use(cookieParser());

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // configure webpack-dev-middleware if we're in development mode
  if (app.get('env') === 'development') {
    const webpackConfig = require('../config/webpack.config.dev.js'),
        compiler = require('webpack')(webpackConfig);

    app.use(require("webpack-dev-middleware")(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath
    }));

    app.use(require("webpack-hot-middleware")(compiler));
  }

  // establish our session handling
  app.use(session({
    ...conf.app.session.opts,
    store: require('./lib/session-store').getStore()
  }));

  app.use(require('./routes/middleware/serve-spa-html'));

  app.use(require('./routes/middleware/json-envelope'));

  // We need to serve static documents...
  console.log('Serving dynamic assets from S3 bucket ' + conf.s3.uploadBucket + '/' + conf.s3.uploadFolder);
  console.log('Serving static assets from : ' + path.join(__dirname, 'static/'));
  app.use('/static', express.static(path.join(__dirname, 'static')));
  
  // load route handlers
  // Get array of all js file located in ./routes directory
  //
  var routeHandlers = fs.readdirSync(path.resolve(__dirname, './routes'))
    .filter(function(fileName) {
      return fileName.substr(-3) === '.js';
    }).map(function(fileName) {
      return './routes/' + fileName;
    });

  // Register route handlers just found with Express
  //
  // SPECIAL CASE - We need to load the auth.js routes first, even if they are not found first in the array
  // beause we define global routes like /api/* there and they have to be addded before /api/foo 
  app.use(require('./routes/auth'));
  routeHandlers.forEach(function(routeHandler) {
      // skip over auth.js, we did it manually above...
      if (!routeHandler.includes('./routes/auth.js')) {
        app.use(require(routeHandler));
      }
  });

  app.use(require('./routes/middleware/error-handler'));

  // Start the HTTP listener...
  http.createServer(app).listen(app.get('port'), conf.app.host, () => {
    console.log(`The environment profile is ${app.get('env')}`);
    console.log('Session Storage set to ' + conf.db.client + ':' + conf.db.host + ':' + conf.db.database + ':' + conf.db.user +':'+ conf.app.session.opts.tablename)
    console.log(`HTTP Coach Co server listening on port ${app.get('port')}`);
    cb && cb(null, app);
  });

  // If we are running locally, and have the NODE_LOCAL environment variable set, start a HTTPS server also.  This wont work
  // on Heroku.   Don't care what NODE_LOCAL is set to - if it exists at all we assume we are local.
  if (process.env.NODE_LOCAL) {
    // Setup our SSL certficates...   Note these are self signed for development / testing for now...
    var https_options = {
      key: fs.readFileSync('./ssl/server.key'),
      cert: fs.readFileSync('./ssl/server.crt')
    };
    // Start the HTTPS listener...
    https.createServer(https_options, app).listen(app.get('port')+1, conf.app.host, () => {
      console.log(`HTTPS Coach Co server listening on port ${app.get('port')+1}`);
    });
  }
};

if (conf.testMode) {
  module.exports = init;
} else {
  init();
}
