const assetsService = module.exports = {},
    Joi = require('joi'),
    aws = require('aws-sdk'),
    path = require('path'),
    conf = require('../lib/conf'),
    errors = require('../lib/custom-errors'),
    s3Funcs = require('../lib/s3funcs'),
    Assets = require('./service-models/assets-model');

assetsService.getAssetById = async function getAssetById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const asset = await Assets.where({ id }).fetch(opts);

    if (!asset) throw new errors.ResourceNotFoundError();

    return asset;
};

assetsService.getAssetsByUser = async function getAssetsByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await Assets.where('user_id', '=', userId).fetchAll(opts);
};

assetsService.createAsset = async function createAsset(assetProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete assetProps.id;

    return await new Assets(assetProps).save();
};

assetsService.updateAsset = async function updateAsset(id, assetUpdates) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }
    
    const asset = await assetsService.getAssetById(id);

    // If the update changes the assetUrl, we should remove the physical file on S3 refered to before the update first...
    if ( (assetUpdates) && (assetUpdates.hasOwnProperty('assetUrl')) ) {
      if (assetUpdates.assetUrl != asset.get('assetUrl') ) {
        await s3Funcs.s3DeleteObjectByUrl(asset.get('assetUrl'));
      }
    }

    asset.setAllowed(assetUpdates);

    return await asset.save();
};

assetsService.deleteAsset = async function deleteAsset(id) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    // Get a copy of the asset record we are about to delete
    const asset = await assetsService.getAssetById(id);

    // Tell S3 to delete the physical file...
    if ( (asset) && (asset.get('assetUrl').length > 0) ) {
      await s3Funcs.s3DeleteObjectByUrl(asset.get('assetUrl'));
    }
    
    // Remove record of asset from the database...
    return await asset.destroy({ id });
};

/* OBSOLETE - We store Logo Asset URL in Branding field of databse for coaches now */
/*
assetsService.getLogoAssetByUser = async function getAssetsByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await Assets.where({user_id: userId, asset_type: 'L'}).fetch(opts);
};
*/
