const assessmentService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    Assessment = require('./service-models/assessment-model');

assessmentService.getAssessmentById = async function getAssessmentById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const assessment = await Assessment.where({ id }).fetch(opts);

    if (!assessment) throw new errors.ResourceNotFoundError();

    return assessment;
};

assessmentService.getAssessmentsByUser = async function getAssessmentsByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await Assessment.where('user_id', '=', userId).fetchAll(opts);
};

assessmentService.getAssessmentsSharedWithUser = async function getAssessmentsSharedWithUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await Assessment.query(function(qb) {
      qb.select('assessment.id', 'app_user.coach_id', 'assessment.user_id', 'app_user.lastname', 'app_user.firstname', 'assessment.assessment');
      qb.innerJoin('app_user', 'assessment.user_id', 'app_user.id');
      // Note we need to use whereRaw() here to use the PG JSONB ->> syntax.  Using standard where() throws errors inside of Bookshelf
      qb.whereRaw("user_id in (select id from app_user where coach_id in (select id from app_user where coach_id = ?) ) and assessment->>'mentorReviewable' = 'Y' ", [userId]);
      // console.log(qb.toString());
    }).fetchAll(opts);
};

assessmentService.createAssessment = async function createAssessment(assessmentProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete assessmentProps.id;

    return await new Assessment(assessmentProps).save();
};

assessmentService.updateAssessment = async function updateAssessment(id, assessmentUpdates) {
    const assessment = await assessmentService.getAssessmentById(id);

    assessment.setAllowed(assessmentUpdates);

    return await assessment.save();
};

assessmentService.deleteAssessment = async function deleteAssessment(id) {
    // throw new Error('NOT IMPLEMENTED');
    const assessment = await assessmentService.getAssessmentById(id);
    return await assessment.destroy({ id });
};
