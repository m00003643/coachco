const clientAssetsService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    ClientAssets = require('./service-models/client-assets-model');

clientAssetsService.getClientAssetsByUser = async function getClientAssetsByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await ClientAssets.where('user_id', '=', userId).fetchAll(opts);
};

clientAssetsService.createCientAsset = async function createCientAsset(assetProps) {
    throw new Error('NOT IMPLEMENTED');
};

clientAssetsService.updateCientAsset= async function updateCientAsset(id, assetUpdates) {
    throw new Error('NOT IMPLEMENTED');
};

clientAssetsService.deleteCientAsset = async function deleteCientAsset(id) {
    // @todo: need to determine how to handle deleting assets
    throw new Error('NOT IMPLEMENTED');
};
