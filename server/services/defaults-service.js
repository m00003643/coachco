const defaultsService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    Defaults = require('./service-models/defaults-model');

defaultsService.getDefaultById = async function getDefaultById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const d = await Defaults.where({ id }).fetch(opts);

    if (!d) throw new errors.ResourceNotFoundError();

    return d;
};

defaultsService.getDefaultByName = async function getDefaultByName(name, opts) {
    return await Defaults.where('default_name', '=', name).fetch(opts);
};

defaultsService.createDefault = async function createDefault(defaultProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert
    delete defaultProps.id;

    return await new Defaults(defaultProps).save();
};

defaultsService.updateDefault = async function updateDefault(id, defaultUpdates) {
    const d = await defaultsService.getDefaultById(id);

    d.setAllowed(defaultUpdates);

    return await d.save();
};

defaultsService.deleteDefault = async function deleteDefault(id) {
    const d = await defaultsService.getDefaultById(id);
    return await d.destroy({ id });
};
