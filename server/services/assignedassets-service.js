const assignedAssetsService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    AssignedAssets = require('./service-models/assigned-assets-model');

assignedAssetsService.getAssignedAssetById = async function getAssignedAssetById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const assignedasset = await AssignedAssets.where({ id }).fetch(opts);

    if (!assignedasset) throw new errors.ResourceNotFoundError();

    return assignedasset;
};

assignedAssetsService.getAssignedAssetsByUser = async function getAssignedAssetsByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await AssignedAssets.where('user_id', '=', userId).fetchAll(opts);
};

assignedAssetsService.createAssignedAsset = async function createAssignedAsset(assignedAssetProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete assignedAssetProps.id;

    return await new AssignedAssets(assignedAssetProps).save();
};

assignedAssetsService.updateAssignedAsset= async function updateAssignedAsset(id, assignedAssetUpdates) {
    const assignedAsset = await assignedAssetsService.getAssignedAssetById(id);

    assignedAsset.setAllowed(assignedAssetUpdates);

    return await assignedAsset.save();
};

assignedAssetsService.deleteAssignedAsset = async function deleteAssignedAsset(id) {
    // @todo: need to determine how to handle deleting assets
    // throw new Error('NOT IMPLEMENTED');
    const assignedAsset = await assignedAssetsService.getAssignedAssetById(id);
    return await assignedAsset.destroy({ id });
};
