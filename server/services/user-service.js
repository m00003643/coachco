const userService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    User = require('./service-models/user-model'),
    s3Funcs = require('../lib/s3funcs'),
    conf = require('../lib/conf'),
    defaultsService = require('../services').defaults,
    assessmentService = require('../services').assessment,
    clientLogService = require('../services').clientlog,
    clientAssetsService = require('../services').clientassets;
    
userService.getUserById = async function getUserById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const user = await User.where({ id }).fetch(opts);

    if (!user) throw new errors.ResourceNotFoundError();

    return user;
};

userService.getUserByUsername = async function getUserByUsername(username) {
    const user = await User.where({ username: username.toLowerCase() }).fetch();

    if (!user) throw new errors.ResourceNotFoundError();

    return user;
};

userService.getUserByEmail = async function getUserByEmail(email) {
    const user = await User.where({ email: email }).fetch();

    if (!user) throw new errors.ResourceNotFoundError();

    return user;
};

userService.getUsersByCoach = async function getUsersByCoach(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const users = await User.where('coach_id', '=', id).fetchAll(opts);

    if (!users) throw new errors.ResourceNotFoundError();

    return users;
};

userService.createUser = async function createUser(userProps) {
    // @todo: might need to use _.pick or similar to only load model with the
    // white-listed properties. Need to see what the default Model constructor
    // does and what happens in validation first.
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete userProps.id;

    return await new User(userProps).save();
};

userService.updateUser = async function updateUser(id, userUpdates) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }
    
    const user = await userService.getUserById(id);

    // If update is changing the branding logo file url, delete the original phyical file from S3 first...
    // But don't delete if this record refers to the stock / default logo...
    if (user) {
      if ( (userUpdates) && (userUpdates.hasOwnProperty('branding')) ) {
        let updatedBranding = userUpdates.branding;
        // Does the update contain a branding logo Url?
        if ( (updatedBranding) && (updatedBranding.hasOwnProperty('companyLogo')) ) {
          let beforeBranding = user.get('branding');
          if ( (beforeBranding) && (beforeBranding.hasOwnProperty('companyLogo')) ) {
            // Does the Update change the branding logo Url?
            if (beforeBranding.companyLogo != updatedBranding.companyLogo) {
              // But don't try to delete if it was set to the Stock logo...
              if (beforeBranding.companyLogo != conf.brand.logo) { 
                await s3Funcs.s3DeleteObjectByUrl(beforeBranding.companyLogo);
              }  
            }
          } 
        }
      }
    }

    user.setAllowed(userUpdates);

    return await user.save();
};

userService.deleteUser = async function deleteUser(id) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const user = await userService.getUserById(id);

    // Tell S3 to delete the physical branding logo file before we remove the db record...
    // But don't delete if this record refers to the stock / default logo...
    if (user) {
      let branding = user.get('branding');
      if ( (branding) && (branding.hasOwnProperty('companyLogo')) ) {
        if (branding.companyLogo != conf.brand.logo) { 
          await s3Funcs.s3DeleteObjectByUrl(branding.companyLogo);
        }  
      }
    }

    return await user.destroy({ id });
};

userService.getAuthenticatedUser = async function getAuthenticatedUser(credentials) {
    const result = Joi.validate(credentials, {
        username: Joi.string().required().label('Username'),
        password: Joi.string().required().label('Password')
    }, {
        abortEarly: false
    });

    if (result.error) {
        throw new errors.ResourceValidationError(null, result.error.details);
    }

    const user = await User.where({ username: credentials.username }).fetch();

    if (!user) throw new errors.AuthenticationFailureError('The username or password was incorrect.');

    // If user is not ACTIVE, they can't log in...
    if (user.get('status') !== 'ACTIVE') throw new errors.AuthenticationFailureInactiveUserError('User is not an ACTIVE user...');

    // Username matched, Status is ACTIVE, check password...
    const isMatch = await user.comparePassword(credentials.password);
    if (!isMatch) throw new errors.AuthenticationFailureError('The username or password was incorrect.');

    return user;
};

userService.getUsersByCoachDetailed = async function getUsersByCoachDetailed(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    // Define an array we will stuff our results into as we go...
    const coachDetail = [];

    // Get all of the client models for this coach...
    const users = await User.where('coach_id', '=', id).fetchAll(opts);

    if (!users) throw new errors.ResourceNotFoundError();

    // For each User (a Client), get all its data and add to our result...
    for (i=0; i < users.size(); i++) {
        var client = users.at(i);
        var assessments =  await assessmentService.getAssessmentsByUser(client.get('id'));
        var assets = await clientAssetsService.getClientAssetsByUser(client.get('id'));
        var log = await clientLogService.getClientLogByClient(client.get('id'));
        
        // Build our return object in the desired format compatible with the client UI...  Note .toViewObject() serializes and removes non-public attributes
        var clientDetail = client.toViewObject();
        clientDetail.assessments = assessments.map(assessments => assessments.toViewObject());
        clientDetail.assets = assets.map(assets => assets.toViewObject());
        clientDetail.log = log.map(log => log.toViewObject());
        
        coachDetail.push(clientDetail);
    };

    return coachDetail;
}; 

userService.getUserByIdDetailed = async function getUserByIdDetailed(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const user = await User.where({ id }).fetch(opts);

    if (!user) throw new errors.ResourceNotFoundError();
    
    var assessments =  await assessmentService.getAssessmentsByUser(user.get('id'));
    var assets = await clientAssetsService.getClientAssetsByUser(user.get('id'));
    var log = await clientLogService.getClientLogByClient(user.get('id'));

    const ret = {};  
    ret.id = user.get("id");
    ret.assessments = assessments.map(assessments => assessments.toViewObject());
    ret.assets = assets.map(assets => assets.toViewObject());
    ret.log = log.map(log => log.toViewObject());

    return ret;
};

userService.resetBrandingEmail = async function resetBrandingEmail(id) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }
    
    // Get a model of the user we are going to update
    const user = await userService.getUserById(id);
    // Make sure it has branding info and has the COACH role...
    if ( (user) && (user.get('role') == 'COACH') ) {
      let branding = user.get('branding');
      if (!branding) {
          branding = {};
      }
      // If we have default branding defined in the database, use it.  Otherwise, use the config file setting...
      let d = await defaultsService.getDefaultByName('coachBranding');
      if (d) {
        branding.welcomeEmail = d.get('defaultJson').welcomeEmail;
      } else {
        branding.welcomeEmail = conf.brand.welcomeEmail;
      }
      user.set('branding', branding);
    } else {
      throw new errors.BadRequestError('User must have a role of COACH');
    }

    return await user.save();
};

userService.resetBrandingTemplate = async function resetBrandingTemplate(id, templateName) {
    // Coerce id to a number, check for NaN 
    id = +id;
    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    // Get a model of the user we are going to update
    const user = await userService.getUserById(id);

    // Make sure it has branding info and has the COACH role...
    if ( (user) && (user.get('role') == 'COACH') ) {
      let branding = user.get('branding');
      if (!branding) {
          branding = {};
      }
    
      // If we have default branding defined in the database, use it.  Otherwise, use the config file setting...
      let d = await defaultsService.getDefaultByName('coachBranding');
      if (d) {
        // We should only try to update if templateName is a valid option
        let defaultBranding = d.get('defaultJson');
        if ( defaultBranding.hasOwnProperty(templateName) ) {       
          branding[templateName] = d.get('defaultJson')[templateName];
        }
      } else {
        // We should only try to update if templateName is a valid option
        if ( conf.brand.hasOwnProperty(templateName) ) {       
          branding[templateName] = conf.brand[templateName];
        }
      }

      user.set('branding', branding);
    } else {
      throw new errors.BadRequestError('User must have a role of COACH');
    }

    return await user.save();
};
