const conversationsService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    Conversation = require('./service-models/conversations-model');

conversationsService.getConversationById = async function getConversationById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const c = await Conversation.where({ id }).fetch(opts);

    if (!c) throw new errors.ResourceNotFoundError();

    return c;
};

conversationsService.getConversationByUser = async function getConversationByUser(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    // Use fetchAll() so that we get an Array back, even if it an empty one...
    // const c = await Conversation.where('user_id', '=', id).fetch(opts);
    const c = await Conversation.where('user_id', '=', id).fetchAll(opts);

    if (!c) throw new errors.ResourceNotFoundError();

    return c;
};

conversationsService.getConversationsByCoach = async function getConversationsByCoach(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const c = await Conversation.where('coach_id', '=', id).fetchAll(opts);

    if (!c) throw new errors.ResourceNotFoundError();

    return c;
};

conversationsService.createConversation = async function createConversation(conversationProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert
    delete conversationProps.id;

    return await new Conversation(conversationProps).save();
};

conversationsService.updateConversation = async function updateConversation(id, conversationUpdates) {
    const c = await conversationsService.getConversationById(id);

    c.setAllowed(conversationUpdates);

    return await c.save();
};

conversationsService.deleteConversation = async function deleteConversation(id) {
    const c = await conversationsService.getConversationById(id);
    return await c.destroy({ id });
};

conversationsService.setIndicators = async function setIndicators(id, indicator, newvalue) {
    // Get the current model of the conversation indicated by 'id'...
    let c = await conversationsService.getConversationById(id);

    // Get the conversation field from the model...
    let conv = c.get('conversation');

    // Update the key pair requested by 'indicator' and 'newvalue'...
    conv[indicator] = newvalue;

    // Apply our changes to the conversation JSON field to the model...
    c.set('conversation', conv);

    // Commit our changes of the model to the datbase and return a copy of the model...
    return await c.save();
};
