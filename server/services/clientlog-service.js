const clientLogService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    ClientLog = require('./service-models/client-log-model');

clientLogService.getClientLogById = async function getClientLogById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const log = await ClientLog.where({ id }).fetch(opts);

    if (!log) throw new errors.ResourceNotFoundError();

    return log;
};

clientLogService.getClientLogByUser = async function getClientLogByUser(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await ClientLog.where('user_id', '=', userId).fetchAll(opts);
};

clientLogService.getClientLogByClient = async function getClientLogByClient(userId, opts) {
    userId = +userId;

    if (isNaN(userId)) {
        throw new errors.BadRequestError('userId must be a number');
    }

    return await ClientLog.where('client_id', '=', userId).fetchAll(opts);
};

clientLogService.createClientLog = async function createClientLog(clientlogProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete clientlogProps.id;

    return await new ClientLog(clientlogProps).save();
};

clientLogService.updateClientLog = async function updateClientLog(id, clientLogUpdates) {
    const log = await clientLogService.getClientLogById(id);

    log.setAllowed(clientLogUpdates);

    return await log.save();
};

clientLogService.deleteClientLog = async function deleteClientLog(id) {
    // @todo: need to determine how to handle deleting users
    // throw new Error('NOT IMPLEMENTED');
    
    const log = await clientLogService.getClientLogById(id);
    return await log.destroy({ id });
};