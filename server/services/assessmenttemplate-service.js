const assessmenttemplateService = module.exports = {},
    Joi = require('joi'),
    errors = require('../lib/custom-errors'),
    AssessmentTemplate = require('./service-models/assessment-template-model');

assessmenttemplateService.getAssessmentTemplateById = async function getAssessmentTemplateById(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const assessmenttemplate = await AssessmentTemplate.where({ id }).fetch(opts);

    if (!assessmenttemplate) throw new errors.ResourceNotFoundError();

    return assessmenttemplate;
};

assessmenttemplateService.getAssessmentTeamplatesByCoach = async function getAssessmentTeamplatesByCoach(id, opts) {
    id = +id;

    if (isNaN(id)) {
        throw new errors.BadRequestError('id must be a number');
    }

    const assessmenttemplates = await AssessmentTemplate.where('coach_id', '=', id).fetchAll(opts);

    if (!assessmenttemplates) throw new errors.ResourceNotFoundError();

    return assessmenttemplates;
};

assessmenttemplateService.createAssessmentTemplate = async function createAssessmentTemplate(assessmenttemplateProps) {
    // @IMPORTANT!: make sure there's no id property provided, or this will
    // perform an update instead of insert, and potentially override an existing
    // user.
    delete assessmenttemplateProps.id;

    return await new AssessmentTemplate(assessmenttemplateProps).save();
};

assessmenttemplateService.updateAssessmentTemplate = async function updateAssessmentTemplate(id, assessmenttemplateUpdates) {
    const assessmenttemplate = await assessmenttemplateService.getAssessmentTemplateById(id);

    assessmenttemplate.setAllowed(assessmenttemplateUpdates);

    return await assessmenttemplate.save();
};

assessmenttemplateService.deleteAssessmentTemplate = async function deleteAssessmentTemplate(id) {
    // throw new Error('NOT IMPLEMENTED');
    const at = await assessmenttemplateService.getAssessmentTemplateById(id);
    return await at.destroy({ id });
};
