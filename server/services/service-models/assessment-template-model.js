var Joi = require('joi'),
    CtkModel = require('./ctk-model');

var AssessmentTemplate = module.exports = CtkModel.extend({
    tableName: 'assessment_template',

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'coachId', 'assessmentTemplate', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['coachId', 'assessmentTemplate'],

    validationSchema: {
        id: Joi.number().integer(),
        coachId: Joi.number().integer().required(),
        assessmentTemplate: Joi.allow(null),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
});