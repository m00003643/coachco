var Joi = require('joi'),
    CtkModel = require('./ctk-model');

var Assessment = module.exports = CtkModel.extend({
    tableName: 'assessment',

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'userId', 'assessment', 'created', 'updated'],
    //publicAttributes: ['id', 'userId', 'assessment', 'mentorReviewable', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['userId', 'assessment'],
    // updatableAttributes: ['userId', 'assessment', 'mentorReviewable'],

    validationSchema: {
        id: Joi.number().integer(),
        userId: Joi.number().integer().required(),
        assessment: Joi.allow(null),
        //mentorReviewable: Joi.string().allow('Y','N').required(),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
});