var Joi = require('joi'),
    CtkModel = require('./ctk-model');

var Assets = module.exports = CtkModel.extend({
    tableName: 'client_assets',

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'userId', 'assetId', 'assetName', 'assetUrl'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: [],

    validationSchema: {
        id: Joi.number().integer().required(),
        userId: Joi.number().integer().required(),
        assetId: Joi.number().integer().required(),
        assetName: Joi.string(),
        assetUrl: Joi.string()
    }
}); 