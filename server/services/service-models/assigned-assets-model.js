var Joi = require('joi'),
    CtkModel = require('./ctk-model'),
    Assets = require('./assets-model');

var AssignedAssets = module.exports = CtkModel.extend({
    tableName: 'assigned_assets',

    asset : function() {
        return this.belongsTo(Assets, 'asset_id', 'id');
    },

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'userId', 'assetId', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['userId', 'assetId'],

    validationSchema: {
        id: Joi.number().integer(),
        userId: Joi.number().integer().required(),
        assetId: Joi.number().integer().required(),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
}); 