var Joi = require('joi'),
    CtkModel = require('./ctk-model'),
    AssignedAssets = require('./assigned-assets-model');

var Assets = module.exports = CtkModel.extend({
    tableName: 'assets',

    assignedAssets : function() {
        return this.hasMany(AssignedAssets, 'asset_id');
    },

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'userId', 'assetType', 'assetName', 'assetUrl', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['userId', 'assetType', 'assetName', 'assetUrl'],

    validationSchema: {
        id: Joi.number().integer(),
        userId: Joi.number().integer().required(),
        assetType: Joi.string().allow('A', 'I', 'L').default('A'),
        assetName: Joi.string().min(1).max(25).default('Asset Name'),
        assetUrl: Joi.string().min(1).max(512).default('/static/assets/somefile'),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
}); 