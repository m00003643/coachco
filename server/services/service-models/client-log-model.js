var Joi = require('joi'),
    CtkModel = require('./ctk-model');

var ClientLog = module.exports = CtkModel.extend({
    tableName: 'client_log',

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'userId', 'clientId', 'logDate', 'theme', 'paid', 'logHours', 'groupName', 'groupCount', 'description', 'notes', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['userId', 'clientId', 'logDate', 'theme', 'paid', 'logHours', 'groupName', 'groupCount', 'description', 'notes'],

    validationSchema: {
        id: Joi.number().integer(),
        userId: Joi.number().integer().required(),
        clientId: Joi.number().integer().required(),
        logDate: Joi.date().required(),
        theme: Joi.string().allow('PERSONAL', 'PROFESSIONAL', 'PERFORMANCE', 'BUSINESS', 'RELATIONSHIP', 'MARRIAGE', 'TRANSITION', 'OTHER').default('PERSONAL').required(),
        paid: Joi.string().allow('Y', 'N').default('Y').required(),
        logHours: Joi.number().precision(2).required(),
        groupName: Joi.string().max(25).allow(null),
        groupCount: Joi.number().integer().required(),
        description: Joi.string().max(25).allow(null),
        notes: Joi.string().max(255).allow(null),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
});