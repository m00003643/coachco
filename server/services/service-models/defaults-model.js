var Joi = require('joi'),
    CtkModel = require('./ctk-model');

var Assessment = module.exports = CtkModel.extend({
    tableName: 'app_defaults',

    // NOTE - because CTK-MODEL is doing case conversion from camelCase to snake_case, we use the camelCase version here
    // so if the datbase has a field user_id, we reference it here as userId (remove the underbar, capitalize next character)

    // viewable by anyone
    publicAttributes: ['id', 'defaultName', 'defaultNumber', 'defaultText', 'defaultBoolean', 'defaultJson', 'created', 'updated'],
    //publicAttributes: ['id', 'userId', 'assessment', 'mentorReviewable', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['defaultName', 'defaultNumber', 'defaultText', 'defaultBoolean', 'defaultJson'],
    // updatableAttributes: ['userId', 'assessment', 'mentorReviewable'],

    validationSchema: {
        id: Joi.number().integer(),
        defaultName: Joi.string().max(25).required(),
        defaultText: Joi.string().max(512).allow(null),
        defaultNumber: Joi.number().integer(),
        defaultBoolean: Joi.boolean(),
        defaultJson: Joi.allow(null),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
});