var Joi = require('joi'),
    bcrypt = require('bcrypt'),
    CtkModel = require('./ctk-model');

var User = module.exports = CtkModel.extend({
    tableName: 'app_user',

    preSave: function() {
        return this.hashPassword();
    },

    hashPassword: async function() {
        if (!this.hasChanged('password')) return;

        var hash = await bcrypt.hash(this.get('password'), 8);

        this.set('password', hash);
    },

    // note: this should only be called against instances that have been fetched
    // from the database (since the password gets hashed prior to saving)
    comparePassword: async function(testPass) {
        var hashedPass = this.get('password');
        return await bcrypt.compare(testPass, hashedPass);
    },

    // viewable by anyone
    publicAttributes: ['id', 'username', 'email', 'resetToken', 'role', 'status', 'firstname', 'lastname', 'coachId', 'phone', 'company', 'title', 'website', 'address', 'city', 'state', 'postalcode', 'country', 'notes', 'branding', 'settings', 'master', 'created', 'updated'],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: ['username', 'password', 'resetToken', 'email', 'role', 'status', 'firstname', 'lastname', 'coachId', 'phone', 'company', 'title', 'website', 'address', 'city', 'state', 'postalcode', 'country', 'notes', 'branding', 'settings', 'master'],

    validationSchema: {
        id: Joi.number().integer(),
        email: Joi.string().email().required(),
        // @todo: need to ask Geekin guys for actual username requirements
        username: Joi.string().min(1).max(30).regex(/^[a-z][a-z0-9_]*$/).required(),
        // todo: this will most likely run post-bcrypt, so make sure that we check
        // length prior to then
        password: Joi.string().min(6).required(),
        resetToken: Joi.string().allow(null),
        firstname: Joi.string().max(40).required(),
        lastname: Joi.string().max(40).required(),
        phone: Joi.string().max(18).allow(null),
        company: Joi.string().max(50).allow(null),
        title: Joi.string().max(50).allow(null),
        website: Joi.string().max(512).allow(null),
        address: Joi.string().max(80).allow(null),
        city: Joi.string().max(50).allow(null),
        state: Joi.string().max(2).allow(null),
        postalcode: Joi.string().max(10).allow(null),
        country: Joi.string().max(25).allow(null),
        notes: Joi.string().max(255).allow(null),
        coachId: Joi.number().integer().allow(null),
        role: Joi.string().allow('CLIENT', 'COACH').default('CLIENT'),
        status: Joi.string().allow('ACTIVE', 'INACTIVE', 'PENDING', 'REQUEST').default('ACTIVE'),
        master: Joi.string().allow('Y', 'N').default('N'),
        branding: Joi.allow(null),
        settings: Joi.allow(null),
        created: Joi.date().forbidden(),
        updated: Joi.date().forbidden()
    }
});