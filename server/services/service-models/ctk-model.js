var Joi = require('joi'),
    bookshelf = require('../../lib/bookshelf'),
    errors = require('../../lib/custom-errors'),
    _ = require('lodash');

/**
 * A base model class all other CTK API models inherit from
 */
var CtkModel = module.exports = bookshelf.Model.extend({
    initialize: function() {
        this.on('saving', function() {
            // these are handled database-side
            // @todo: you're not supposed to play with these directly, need to
            // figure out if this.set('created', undefined) or similar works
            delete this.attributes.created;
            delete this.attributes.updated;

            this.validateSave();

            // note: this should remain after validation, as that allows, e.g.,
            // password constraints to be enforced prior to then bcryptifing it
            if (this.preSave) {
                // preSave implementations should return a promise
                return this.preSave();
            }
        });
    },

    // convert camelCase to snake_case before persisting
    format: function(attrs) {
        return _.reduce(attrs, function(ret, val, key) {
            var snakeCased = key.replace(/([A-Z])/g, $1 => '_' + $1.toLowerCase());
            ret[snakeCased] = val;
            return ret;
        }, {});
    },

    // convert snake_case to camelCase when hydrating
    parse: function(attrs) {
        return _.reduce(attrs, function(ret, val, key) {
            var camelCased = key.replace(/(_[a-z])/g, $1 => $1[1].toUpperCase());
            ret[camelCased] = val;
            return ret;
        }, {});
    },

    validateSave: function() {
        Joi.validate(this.attributes, this.validationSchema, {
            abortEarly: false,
            convert: true
        }, (err, result) => {
            if (err) {
                throw new errors.ResourceValidationError(null, err.details);
            }

            // note: `result` may have had some defaults values applied or value types
            // coerced by the validation schema, so update our values again before continuing
            this.set(result);
        });
    },

    // called set() using white-listed attributes only
    setAllowed: function(updates) {
        return this.set(_.pick(updates, this.updatableAttributes));
    },

    toViewObject: function() {
        var ret = _.pick(this.attributes, this.publicAttributes);

        // need to recurse over any relations that are also public attributes
        // @note: right now all our relations are Arrays, but that may not
        // always be the case. might need to code for single model relations
        // at some point in the future
        Object.keys(this.relations)
            .filter(relation => this.publicAttributes.includes(relation))
            .forEach(relation => {
                ret[relation] = this.related(relation).models.map(model => {
                    return model.toViewObject();
                });
            });

        return ret;
    },

    //
    // sub-classes should override the following:
    //

    // viewable by anyone
    publicAttributes: [],

    // whitelisted attributes that can be updated after creation
    updatableAttributes: [],

    // a joi validation schema
    validationSchema: {}

});