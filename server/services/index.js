var services = module.exports = {};

require('fs').readdirSync(__dirname)
	.filter(_ => _.endsWith('-service.js'))
	.forEach(_ => services[_.split('-')[0]] = require(`./${_}`));