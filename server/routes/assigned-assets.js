const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    assignedAssetsService = require('../services').assignedassets,
    userService = require('../services').user,
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/assignedassets/:id
 * 
 * @api {get} /api/assignedassets/:id Get Assigned Assets By Id
 * @apiVersion 0.1.0
 * @apiName Get Assigned Assets By Id
 * @apiGroup Assigned Assets
 * @apiDescription This route retrieves the full assigned asset object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the Assigned Assets to retrieve
 * @apiSuccess {json} assignedasset Json object containing the retrieved Assigned Assets attributes
 * @apiExample Example Usage:
 *   http://localhost/api/assignedassets/1
 */

router.get('/api/assignedassets/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assignedasset = await assignedAssetsService.getAssignedAssetById(req.params.id);

    res.status(200).json(assignedasset.toViewObject());
});

/**
 * ROUTE  /api/assignedassets/user/:id
 * 
 * @api {get} /api/assignedassets/user/:id Get Assigned Assets By User
 * @apiVersion 0.1.0
 * @apiName Get Assigned Assets By User
 * @apiGroup Assigned Assets
 * @apiDescription This route retrieves the a collection of assigned asset objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retrieve Assigned Assets for
 * @apiSuccess {json} assignedassets Json object containing the retrieved Assigned Assets
 * @apiExample Example Usage:
 *   http://localhost/api/assignedassets/user/1
 */

router.get('/api/assignedassets/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assignedassets = await assignedAssetsService.getAssignedAssetsByUser(req.params.id);

    res.status(200).json(assignedassets.map(assignedassets => assignedassets.toViewObject()));
});

/**
 * ROUTE  /api/assignedassets
 * 
 * @api {post} /api/assignedassets Create Assigned Asset
 * @apiVersion 0.1.0
 * @apiName Create Assigned Asset
 * @apiGroup Assigned Assets
 * @apiDescription This route will create a new Assigned Asset in the database
 * @apiParam {json} body Json object containing the Assigned Asset be created
 * @apiSuccess (201) {json} assignedasset Json object containing the created Assigned Asset
 * @apiExample Example Usage:
 *   http://localhost/api/assignedassets
 */

router.post('/api/assignedassets', requireUser, async function(req, res, next) {
    const createdAssignedAsset = await assignedAssetsService.createAssignedAsset(req.body);

    res.status(201).json(createdAssignedAsset.toViewObject());
});

/**
 * ROUTE  /api/assignedassets/:id
 * 
 * @api {patch} /api/assignedassets/:id Update Assigned Asset
 * @apiVersion 0.1.0
 * @apiName Update Assigned Assets
 * @apiGroup Assigned Assets
 * @apiDescription This route will update an existing Assigned Asset in the database
 * @apiParam {number} id The <code>id</code> of the Assigned Asset to be updated
 * @apiParam {json} body Json object containing the Assigned Asset attributes to update
 * @apiSuccess {json} assignedasset Json object containing the updated Assigned Asset
 * @apiExample Example Usage:
 *   http://localhost/api/assignedassets/1
 */

router.patch('/api/assignedassets/:id', requireUser, async function(req, res, next) {

    // Coerce id into a number...
    const id = +req.params.id;

    // Only the coach who assigned the asset (and thus owns it) can update the assignement to the user...
    let a = await assignedAssetsService.getAssignedAssetById(id);
    let u = await userService.getUserById(a.get('userId'));
    if (req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }   

    const assignedasset = await assignedAssetsService.updateAssignedAsset(id, req.body);

    res.status(200).json(assignedasset.toViewObject());
});

/**
 * ROUTE  /api/assignedassets/:id
 * 
 * @api {delete} /api/assignedassets/:id Delete an Assigned Asset
 * @apiVersion 0.1.0
 * @apiName Delete an Assigned Assets
 * @apiGroup Assigned Asset
 * @apiDescription This route will delete an existing Assigned Asset in the database
 * @apiParam {number} id The <code>id</code> of the Assigned Asset to delete
 * @apiExample Example Usage:
 *   http://localhost/api/assignedassets/1
 *
 */

router.delete('/api/assignedassets/:id', requireUser, async function(req, res, next) {
    // for now just send back a 405: Method Not Allowed
    // res.status(405).json(null);

    // Coerce id into a number...
    const id = +req.params.id;

    // Only the coach who assigned the asset (and thus owns it) or the client it is assigned to can remove the assignement to the user...
    let a = await assignedAssetsService.getAssignedAssetById(id);
    let u = await userService.getUserById(a.get('userId'));
    if ( (req.session.user.id !== u.get('coachId')) && (req.session.user.id !== a.get('userId')) ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    }   
    
    const assignedAsset = await assignedAssetsService.deleteAssignedAsset(id);
    res.status(200).json(assignedAsset.toViewObject());
});
