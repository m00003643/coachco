const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    userService = require('../services').user,
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * ROUTE  /api/coachdetail/:id
 * 
 * @api {get} /api/coachdetail/:id Get Coach Details by Id
 * @apiVersion 0.1.0
 * @apiName Get Coach Details by Id
 * @apiGroup Coach Detail
 * @apiDescription This route retrieves the a detailed coach object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the coach to retrieve
 * @apiSuccess {json} User Json object containing the detailed coach information
 * @apiExample Example Usage:
 *   http://localhost/api/coachdetail/1
 */

router.get('/api/coachdetail/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const coachDetail = await userService.getUsersByCoachDetailed(req.params.id);
    res.status(200).json(coachDetail);
});
