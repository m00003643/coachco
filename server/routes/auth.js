const router = module.exports = require('node-async-router')(),
    errors = require('../lib/custom-errors'),
    userService = require('../services').user;

/**
 * ROUTE  /api/login
 * 
 * @api {post} /api/login Login
 * @apiVersion 0.1.0
 * @apiName Login
 * @apiGroup Authentication
 * @apiDescription This is the main login route
 * @apiParam {string} body The credentials of the user attempting the login
 * @apiSuccess {Object} User Returns the full user object of the authenticated user
 * @apiExample Example Usage:
 *   http://localhost/api/login
 */

router.post('/api/login', async function(req, res) {
  const { username, password } = req.body;

  const user = await userService.getAuthenticatedUser({ username, password });

  req.session.user = user;

  res.json(user);
});

/**
 * ROUTE  /api/logout
 * 
 * @api {get} /api/logout Logout
 * @apiVersion 0.1.0
 * @apiName Logout
 * @apiGroup Authentication
 * @apiDescription Logs out the current user, cleans out stored session cookies
 * @apiSuccess none
 * @apiExample Example Usage:
 *   http://localhost/api/logout
 */

router.get('/api/logout', (req, res) => {
  delete req.session.user;
  res.json();
});

/**
 * ROUTE  /api/verifyinvite
 * 
 * @api {post} /api/verifyinvite VerifyInvite
 * @apiVersion 0.1.0
 * @apiName Veryify Invite
 * @apiGroup Authentication
 * @apiDescription This route verifies an invited user against the database
 * @apiParam {string} body The credentials of the user attempting the verifiation
 * @apiSuccess {Object} User Returns the full user object of the validated user
 * @apiExample Example Usage:
 *   http://localhost/api/verifyinvite
 */

router.post('/api/verifyinvite', async function(req, res) {
  // Request supplies Invite Code, Email and desired Username and Password
  const { invitation, email, username, password } = req.body;

  // See if we have a user with the email given...
  var invUser = await userService.getUserByEmail(email);

  // If so, does it's username field match our request code...
  if ( (invUser) && (invitation == invUser.get('username')) ) { 
    // If so, update it so that username and password are the requested, and status is ACTIVE
    let userUpdates = { username: username.toLowerCase(), password: password, status: 'ACTIVE' };
    invUser = await userService.updateUser(invUser.get('id'), userUpdates);
  } else {
    throw new errors.AuthenticationFailureError('Invalid User or Invitation Code');
  }

  // Should we log them in here (ie, set the session)??
  req.session.user = invUser;

  res.status(200).json(invUser.toViewObject());
});
