const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    userService = require('../services').user,
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * ROUTE  /api/clientdetail/:id
 * 
 * @api {get} /api/clientdetail/:id Get Client Details by Id
 * @apiVersion 0.1.0
 * @apiName Get Client Details by Id
 * @apiGroup Client Detail
 * @apiDescription This route retrieves the a detailed client object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the client to retrieve
 * @apiSuccess {json} User Json object containing the detailed client information
 * @apiExample Example Usage:
 *   http://localhost/api/clientdetail/1
 */
router.get('/api/clientdetail/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();
  
    const clientDetail = await userService.getUserByIdDetailed(req.params.id);
    res.status(200).json(clientDetail);
});
