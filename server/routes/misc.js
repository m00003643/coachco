const router = module.exports = require('node-async-router')(),
      requireUser = require('./middleware/require-user'),
      s3funcs = require('../lib/s3funcs'),
      BadRequestError = require('../lib/custom-errors').BadRequestError;


/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/sign-s3/:filename/:filetype
 * 
 * @api {get} /api/sign-s3/:filename/:filetype Get Signed S3 URL
 * @apiVersion 0.1.0
 * @apiName Get Signed S3 URL
 * @apiGroup S3 Functions
 * @apiDescription This route will obtain a signed S3 URL from the server suitable for the client to direct upload a file to
 * @apiParam {string} filename The <code>flename</code> of the file to be uploaded
 * @apiParam {string} filetype The MIME <code>filetype</code> of the file to be uploaded
 * @apiExample Example Usage:
 *   http://localhost/api/sign-s3/somefile.pdf/application/pdf
 *
 */

router.get('/api/sign-s3/:filename/:filetype', requireUser, async function(req, res, next) {
   // Obtain a signed URL from S3 based on our file params are turn it...
   var signedUrl = await s3funcs.s3GetSignedUrl(req.params.filename, req.params.filetype, req.session.user.id);
   if (signedUrl) {
     res.write(JSON.stringify(signedUrl));
   }
   res.end();
});