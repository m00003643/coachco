const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    userService = require('../services').user,
    assetsService = require('../services').assets,
    BadRequestError = require('../lib/custom-errors').BadRequestError,
    notifyutils = require('../lib/notification-utils'),
    obutils = require('../lib/onboarding-utils'),
    shortid = require('shortid'),
    conf = require('../lib/conf');

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/users/:id
 * 
 * @api {get} /api/users/:id Get User By Id
 * @apiVersion 0.1.0
 * @apiName Get User By Id
 * @apiGroup User
 * @apiDescription This route retrieves the full user object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retrieve
 * @apiSuccess {json} User Json object containing the retrieved user attributes
 * @apiExample Example Usage:
 *   http://localhost/api/user2/1
 */

router.get('/api/users/:id', requireUser, async function(req, res, next) {
    if (!/^\d+$/.test(req.params.id)) return next(); // punt on anything that's not a numerical id
    var user = await userService.getUserById(req.params.id);
    res.status(200).json(user.toViewObject());
});

/**
 * ROUTE  /api/users/:username
 * 
 * @api {get} /api/users/:username Get User By Username
 * @apiVersion 0.1.0
 * @apiName Get User By Username
 * @apiGroup User
 * @apiDescription This route retrieves the full user object based on the supplied <code>username</code>
 * @apiParam {string} username The <code>username</code> of the user to retrieve
 * @apiSuccess {json} User Json object containing the retrieved user attributes
 * @apiExample Example Usage:
 *   http://localhost/api/user2/jdoe
 */

router.get('/api/users/:username', requireUser, async function(req, res, next) {
    const user = await userService.getUserByUsername(req.params.username);
    res.status(200).json(user.toViewObject());
});

/**
 * ROUTE  /api/users/coach/:id
 * 
 * @api {get} /api/users/coach/:id Get Users by Coach
 * @apiVersion 0.1.0
 * @apiName Get Users by Coach
 * @apiGroup User
 * @apiDescription This route retrieves the a collection of user objects based on the supplied Coach <code>id</code>
 * @apiParam {number} id The <code>id</code> of the Coach to retrieve users for
 * @apiSuccess {json} Users Json object containing all the client users for the requested Coach
 * @apiExample Example Usage:
 *   http://localhost/api/user2/coach/1
 */

router.get('/api/users/coach/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();
    const users = await userService.getUsersByCoach(req.params.id);
    res.status(200).json(users.map(user => user.toViewObject()));
});

/**
 * ROUTE  /api/users
 * 
 * @api {post} /api/users Create User
 * @apiVersion 0.1.0
 * @apiName Create User
 * @apiGroup User
 * @apiDescription This route will create a new user in the database
 * @apiParam {json} body Json object containing the user to be created
 * @apiSuccess (201) {json} user Json object containing the created user
 * @apiExample Example Usage:
 *   http://localhost/api/user
 */

router.post('/api/users', requireUser, async function(req, res, next) {
    // if MASTER is present and is anything but Y, set to N to make sure it passes the NOT NULL validation...
    if ( (req.body.hasOwnProperty('master')) && (req.body.master != 'Y') ) {
      req.body.master = 'N';
    }

    // If STATUS is set to PENDING, we need to generate an "invite code" which will be stored as the username
    let inviteCode = obutils.generateInviteCode();
    if (req.body['status'] == 'PENDING') {
      req.body['username'] =  inviteCode;
      req.body['password'] =  obutils.generateInviteCode(); // Generate another random string to stuff into password.  Don't care what it is, just has to be not null  I
    } 

    const createdUser = await userService.createUser(req.body);
    
    // If STATUS is PENDING, we need to email the newly invited user to let them know...
    if ( (createdUser) && (createdUser.get('status') === 'PENDING') ) { 

      // Setup options to send to Mailer.  These are the common settings regardless of if our invitee has a Coach defined...
      var opts = {
            reason: 'welcomeEmail',
            protocol: req.protocol, 
            baseUrl: req.get('host'), 
            email: createdUser.get('email'), 
            inviteCode: inviteCode, 
            firstName: createdUser.get('firstname'), 
            lastName: createdUser.get('lastname')
          };
          
      if (createdUser.get('coachId') != null) {
        // Get Coach user object so we can get the email text for that Coach from his/her branding config.
        let coach = await userService.getUserById(createdUser.get('coachId'));
        // opts.welcomeEmail = coach.get('branding').welcomeEmail.join('');
        opts.emailTemplate = coach.get('branding').welcomeEmail.join('');
        opts.companyName = coach.get('branding').companyName;
        opts.coachFirstName = coach.get('firstname');
        opts.coachLastName = coach.get('lastname');
        opts.brandEmailLogo = coach.get('branding').companyLogo;
      } else {
        // Invitee has no Coach defined, so use system defaults instead...
        // opts.welcomeEmail = conf.brand.welcomeEmail.join('');
        opts.emailTemplate = conf.brand.welcomeEmail.join('');
        opts.companyName = conf.brand.company;
        opts.brandEmailLogo = conf.brand.logo;
      }

      // obutils.sendmail( opts );
      notifyutils.sendnotificationmail( opts );

    }

    res.status(201).json(createdUser.toViewObject());
});

/**
 * ROUTE  /api/users/:id
 * 
 * @api {patch} /api/users/:id Update User
 * @apiVersion 0.1.0
 * @apiName Update User
 * @apiGroup User
 * @apiDescription This route will update an existing user in the database
 * @apiParam {number} id The <code>id</code> of the user to be updated
 * @apiParam {json} body Json object containing the user attributes to update
 * @apiSuccess {json} User Json object containing the updated user
 * @apiExample Example Usage:
 *   http://localhost/api/user2/1
 */

router.patch('/api/users/:id', requireUser, async function(req, res, next) {
    // coerce the requested id to a number:
    const id = +req.params.id;

    // Users can edit their own data, additionally a Coach can also edit a Clients data...
    let u = await userService.getUserById(id);
    if (req.session.user.id !== id && req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const user = await userService.updateUser(id, req.body);

    // if the updated record is the logged in user, update the session to match...
    if (user.id == req.session.user.id) {
      req.session.user = user;
    }

    res.status(200).json(user.toViewObject());
});

/**
 * ROUTE  /api/users/:id
 * 
 * @apiIgnore Method Unimplemented
 * @api {delete} /api/users/:id Delete a User
 * @apiVersion 0.1.0
 * @apiName Delete a User
 * @apiGroup User
 * @apiDescription This route will delete an existing user in the database
 * @apiParam {number} id The <code>id</code> of the user to delete
 * @apiExample Example Usage:
 *   http://localhost/api/user2/1
 *
 * @todo: do we actually want to delete the user and all dependent records, or should we use some sort of a tombstone flag?
 * 
 */

router.delete('/api/users/:id', requireUser, async function(req, res, next) {
    // for now just send back a 405: Method Not Allowed
    // res.status(405).json(null);
    
    // coerce the requested id to a number:
    const id = +req.params.id;
    const user = await userService.deleteUser(id);
    res.status(200).json(user.toViewObject());
});

/**
 * ROUTE  /api/users/resetbrandingemail/:id
 * 
 * @api {post} /api/users/resetbrandingemail Reset User Branding Email
 * @apiVersion 0.1.0
 * @apiName Reset User Branding Email
 * @apiGroup User
 * @apiDescription This route will reset the branding Welcome Email for the given user to the system default
 * @apiSuccess (201) {json} user Json object containing the update user
 * @apiExample Example Usage:
 *   http://localhost/api/user/resetbrandingemail/1
 */

router.post('/api/users/resetbrandingemail/:id', requireUser, async function(req, res, next) {
    // coerce the requested id to a number:
    const id = +req.params.id;

    // Users can edit their own data, additionally a Coach can also edit a Clients data...
    let u = await userService.getUserById(id);
    if (req.session.user.id !== id && req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    // Only Coaches have branding information...
    if (u.get('role') != 'COACH') {
        throw new BadRequestError('Target user id must have role of COACH.');
    }
  
    // Update the APP_USER table with system default welcomeEmail text
    let user = await userService.resetBrandingEmail(id);

    // if the updated record is the logged in user, update the session to match...
    if (user.id == req.session.user.id) {
      req.session.user = user;
    }

    res.status(200).json(user.toViewObject());
});

/**
 * ROUTE  /api/users/resetbrandingtemplate/:id/:template
 * 
 * @api {post} /api/users/resetbrandingtemplate Reset User Branding Template
 * @apiVersion 0.1.0
 * @apiName Reset User Branding Template
 * @apiGroup User
 * @apiDescription This route will reset the indicaed branding template for the given user to the system default
 * @apiSuccess (201) {json} user Json object containing the update user
 * @apiExample Example Usage:
 *   http://localhost/api/user/resetbrandingtemplate/1/welcomeEmail
 *   http://localhost/api/user/resetbrandingtemplate/1/assessmentNotificationEmail
 */

router.post('/api/users/resetbrandingtemplate/:id/:templateName', requireUser, async function(req, res, next) {
    // coerce the requested id to a number:
    const id = +req.params.id;
    const templateName = req.params.templateName;

    // Users can edit their own data, additionally a Coach can also edit a Clients data...
    let u = await userService.getUserById(id);
    if (req.session.user.id !== id && req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    // Only Coaches have branding information...
    if (u.get('role') != 'COACH') {
        throw new BadRequestError('Target user id must have role of COACH.');
    }
  
    // Update the APP_USER table with system default welcomeEmail text
    let user = await userService.resetBrandingTemplate(id, templateName );

    // if the updated record is the logged in user, update the session to match...
    if (user.id == req.session.user.id) {
      req.session.user = user;
    }

    res.status(200).json(user.toViewObject());
});
