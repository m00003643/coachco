const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    assessmenttemplateService = require('../services').assessmenttemplate,
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/assessmenttemplates/:id
 * 
 * @api {get} /api/assessmenttemplates/:id Get Assessment Template By Id
 * @apiVersion 0.1.0
 * @apiName Get Assessment Template By Id
 * @apiGroup Assessment Templates
 * @apiDescription This route retrieves the full Assessment Template object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the Assessment Template to retrieve
 * @apiSuccess {json} assessmenttemplate Json object containing the retrieved Assessment Template attributes
 * @apiExample Example Usage:
 *   http://localhost/api/assessmenttemplates/1
 */

router.get('/api/assessmenttemplates/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assessmenttemplate = await assessmenttemplateService.getAssessmentTemplateById(req.params.id);

    res.status(200).json(assessmenttemplate.toViewObject());
});

/**
 * ROUTE  /api/assessmenttemplates/coach/:id
 * 
 * @api {get} /api/assessmenttemplates/coach/:id Get Assessment Template By Coach
 * @apiVersion 0.1.0
 * @apiName Get Assessment Template By Coach
 * @apiGroup Assessment Templates
 * @apiDescription This route retrieves the collection of Assessment Template objects based on the supplied coach <code>id</code>
 * @apiParam {number} id The <code>id</code> of the coach to retrieve Assessment Templates for
 * @apiSuccess {json} assessmenttemplates Json object containing the retrieved Assessment Template attributes
 * @apiExample Example Usage:
 *   http://localhost/api/assessmenttemplates/coach/1
 */

router.get('/api/assessmenttemplates/coach/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assessmenttemplates = await assessmenttemplateService.getAssessmentTeamplatesByCoach(req.params.id);

    res.status(200).json(assessmenttemplates.map(assessmenttemplates => assessmenttemplates.toViewObject()));
    
});

/**
 * ROUTE  /api/assessmenttemplates
 * 
 * @api {post} /api/assessmenttemplates Create Assessment Template
 * @apiVersion 0.1.0
 * @apiName Create Assessment Template
 * @apiGroup Assessment Templates
 * @apiDescription This route will create a new Assessment Template in the database
 * @apiParam {json} body Json object containing the Assessment Template to be created
 * @apiSuccess (201) {json} assessmenttemplate Json object containing the created Assessment Template
 * @apiExample Example Usage:
 *   http://localhost/api/assessmenttemplates
 */

router.post('/api/assessmenttemplates', requireUser, async function(req, res, next) {
    const createdAssessmentTemplate = await assessmenttemplateService.createAssessmentTemplate(req.body);

    res.status(201).json(createdAssessmentTemplate.toViewObject());
});

/**
 * ROUTE  /api/assessmenttemplates/:id
 * 
 * @api {patch} /api/assessmenttemplates/:id Update Assessment Template
 * @apiVersion 0.1.0
 * @apiName Update Assessment Template
 * @apiGroup Assessment Templates
 * @apiDescription This route will update an existing Assessment Template in the database
 * @apiParam {number} id The <code>id</code> of the Assessment Template to be updated
 * @apiParam {json} body Json object containing the Assessment Template attributes to update
 * @apiSuccess {json} assessmenttemplate Json object containing the updated Assessment Template
 * @apiExample Example Usage:
 *   http://localhost/api/assessmenttemplates/1
 */

router.patch('/api/assessmenttemplates/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // You should be logged in as the user who owns this record.  
    let a = await assessmenttemplateService.getAssessmentTemplateById(id);
    if (req.session.user.id !== a.get('coachId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const at = await assessmenttemplateService.updateAssessmentTemplate(id, req.body);

    res.status(200).json(at.toViewObject());
});

/**
 * ROUTE  /api/assessmenttemplates/:id
 * 
 * @api {delete} /api/assessmenttemplates/:id Delete Assessment Template
 * @apiVersion 0.1.0
 * @apiName Delete Assessment Template
 * @apiGroup Assessment Templates
 * @apiDescription This route will delete an existing AssesAssessment Templatesment in the database
 * @apiParam {number} id The <code>id</code> of the Assessment Template to delete
 * @apiExample Example Usage:
 *   http://localhost/api/assessmenttemplates/1
 *
 */

router.delete('/api/assessmenttemplates/:id', requireUser, async function(req, res, next) {
    // for now just send back a 405: Method Not Allowed
    // res.status(405).json(null);

    // Coerce id into a number...
    const id = +req.params.id;

    // Who should be able to delete assessment template records? Probably only the coachId that owns them.
    // You should be logged in as the user who owns this record.  
    let tmpAssessmentTemplate = await assessmenttemplateService.getAssessmentTemplateById(id);
    if (req.session.user.id !== tmpAssessmentTemplate.get('coachId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    }

    const at = await assessmenttemplateService.deleteAssessmentTemplate(id);
    res.status(200).json(at.toViewObject());
});