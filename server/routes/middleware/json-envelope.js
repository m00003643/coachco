module.exports = function (req, res, next) {
    var _json = res.json;

    res.json = function(obj) {
        if (!obj) {
            return _json.call(res, {
                success: true,
                result: null
            });
        }

        if (typeof obj.success === 'boolean' && (obj.errorCode || typeof obj.result !== 'undefined')) {
            return _json.call(res, obj);
        }

        _json.call(res, {
            success: true,
            result: obj
        });
    };

    next();
};