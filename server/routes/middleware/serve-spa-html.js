const assessmentService = require('../../services').assessment;
const userService = require('../../services').user;
const clientAssetsService = require('../../services').clientassets;
const defaultsService = require('../../services').defaults;
const conf = require('../../lib/conf');

module.exports = async (req, res, next) => {
  
  if (!req.accepts('html') || req.url.startsWith('/static') || req.url.startsWith('/api')) {
    return next();
  }

  // const user = req.session.user; 

  // Grab who is logged in from the session object...
  let user = req.session.user;

  // Refresh user record from DB in case it was just Updated it so we get all the new detail fields.
  if (user && req.session.user && req.session.user.hasOwnProperty('id')) { 
    let tmpUser = await userService.getUserById(req.session.user.id);
    // Keep the ID/Username/Password attributes from the session object we already have for sure...
    // @todo : Need to define process by which a user's Username and/or Password get updated and what affect it has here
    delete tmpUser.attributes.id;
    delete tmpUser.attributes.username;
    delete tmpUser.attributes.password;
    // Update the session object to capture any updated detail attributes for the user...
    Object.assign(user, tmpUser.toJSON());
    req.session.user = user;
  }
  
  let assessments = [];
  let clients = [];
  let assets = [];
  let branding = {};

  if (user) {
    assessments = await assessmentService.getAssessmentsByUser(user.id);
    assets = await clientAssetsService.getClientAssetsByUser(user.id);

    if (user.role == 'COACH') {
      // clients = await userService.getUsersByCoach(user.id);
      clients = await userService.getUsersByCoachDetailed(user.id);
      branding = user.branding;
    } else {
      // for clients, get branding for their coach...
      let coach = await userService.getUserById(user.coachId);
      branding = coach.get('branding');
    }
    // @todo: we need to serialize *only* the viewObject version of the user.
  } else {
    // If there is no user (for example, they are the landing / login pages), use default branding...
    let d = await defaultsService.getDefaultByName('coachBranding');
    if (d) {
      branding = d.get('defaultJson');
    } else {
      branding.companyName = conf.brand.company;
      branding.companyLogo = conf.brand.logo;
    }
  }

  return res.render('template', {
    initialState: {
      authenticatedUser: req.session.user || null,
      assessments,
      clients,
      assets,
      branding
    }
  });
};