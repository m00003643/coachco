const formatValidationErrors = require('../../lib/format-validation-errors');

module.exports = function errorHandler(err, req, res, next) {
    var body = { success: false },
        showStack = false

    // handle our custom error types
    switch (err.name) {
        case 'AuthenticationFailureInactiveUserError':
            res.status(403);
            body.errorCode = 'INACTIVE_USER';
            body.errorMessage = err.message;
            break;
        case 'AuthenticationFailureError':
            res.status(403);
            body.errorCode = 'AUTHENTICATION_FAILED';
            body.errorMessage = err.message;
            break;
        case 'AuthenticationRequiredError':
            res.status(401);
            body.errorCode = 'AUTHENTICATION_REQUIRED';
            body.errorMessage = err.message;
            break;
        case 'ResourceNotFoundError':
            res.status(404);
            body.errorCode = 'NOT_FOUND';
            body.errorMessage = err.message;
            break;
        case 'ResourceValidationError':
            res.status(400);
            body.errorCode = 'RESOURCE_VALIDATION_FAILED';
            body.errorMessage = err.message;
            body.validationErrors = formatValidationErrors(err.validationErrors);
            break;
        case 'BadRequestError':
            res.status(400);
            body.errorCode = 'BAD_REQUEST';
            body.errorMessage = err.message;
            break;
        default:
            if (err.detail.includes('Key') && err.detail.includes('already exists') ) {
              res.status(400);
              body.errorCode = 'BAD_REQUEST';
              body.errorMessage = err.message;
            } else {
              res.status(500);
              body.errorCode = 'INTERNAL_SERVER_ERROR';
              body.errorMessage = 'Internal Server Error';
              showStack = true;
            }
    }

    if (showStack) {
        console.error(err.stack);

        if (process.env.NODE_ENV !== 'production') {
            body.stack = err.stack;
        }
    }

    res.json(body);
};