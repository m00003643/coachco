const { AuthenticationRequiredError } = require('../../lib/custom-errors');

module.exports = (req, res, next) => {
  if (!req.session || !req.session.user) {
    throw new AuthenticationRequiredError();
  }

  next();
};
