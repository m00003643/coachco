const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    clientLogService = require('../services').clientlog,
    exportfuncs = require('../lib/exportfuncs'),
    fs = require('fs'),
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/clientlog/:id
 * 
 * @api {get} /api/clientlog/:id Get Client Log By Id
 * @apiVersion 0.1.0
 * @apiName Get Client Log By Id
 * @apiGroup Client Log
 * @apiDescription This route retrieves the full client log object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the client log to retrieve
 * @apiSuccess {json} Log Json object containing the retrieved client log attributes
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/1
 */

router.get('/api/clientlog/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const log = await clientLogService.getClientLogById(req.params.id);

    res.status(200).json(log.toViewObject());
});

/**
 * ROUTE  /api/clientlog/user/:id
 * 
 * @api {get} /api/clientlog/user/:id Get Client Log By User Id
 * @apiVersion 0.1.0
 * @apiName Get Client Log By User Id
 * @apiGroup Client Log
 * @apiDescription This route retrieves the all client log objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retrieve logs for
 * @apiSuccess {json} Logs Json object containing the retrieved client log attributes
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/user/1
 */

router.get('/api/clientlog/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const log = await clientLogService.getClientLogByUser(req.params.id);

    res.status(200).json(log.map(log => log.toViewObject()));
});

/**
 * ROUTE  /api/clientlog/client/:id
 * 
 * @api {get} /api/clientlog/client/:id Get Client Log By Client Id
 * @apiVersion 0.1.0
 * @apiName Get Client Log By Client Id
 * @apiGroup Client Log
 * @apiDescription This route retrieves the all client log objects based on the supplied client <code>id</code>
 * @apiParam {number} id The <code>id</code> of the client to retrieve logs for
 * @apiSuccess {json} Logs Json object containing the retrieved client log attributes
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/client/1
 */

router.get('/api/clientlog/client/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const log = await clientLogService.getClientLogByClient(req.params.id);

    res.status(200).json(log.map(log => log.toViewObject()));
});

/**
 * ROUTE  /api/clientlog
 * 
 * @api {post} /api/clientlog Create Client Log
 * @apiVersion 0.1.0
 * @apiName Create Client Log
 * @apiGroup Client Log
 * @apiDescription This route will create a new client log in the database
 * @apiParam {json} body Json object containing the client log be created
 * @apiSuccess (201) {json} Log Json object containing the created client log
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog
 */

router.post('/api/clientlog', requireUser, async function(req, res, next) {
    const createdClientLog = await clientLogService.createClientLog(req.body);

    res.status(201).json(createdClientLog.toViewObject());
});

/**
 * ROUTE  /api/clientlog/:id
 * 
 * @api {patch} /api/clientlog/:id Update Client Log
 * @apiVersion 0.1.0
 * @apiName Update Client Log
 * @apiGroup Client Log
 * @apiDescription This route will update an existing client log in the database
 * @apiParam {number} id The <code>id</code> of the client log to be updated
 * @apiParam {json} body Json object containing the client log attributes to update
 * @apiSuccess {json} User Json object containing the updated client log
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/1
 */

router.patch('/api/clientlog/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // Only the user who owns the record should be allowed to delete it
    let tmpLog = await clientLogService.getClientLogById(id);
    if (!req.session.user || req.session.user.id !== tmpLog.get('userId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const log = await clientLogService.updateClientLog(id, req.body);

    res.status(200).json(log.toViewObject());
});

/**
 * ROUTE  /api/clientlog/:id
 * 
 * @api {delete} /api/clientlog/:id Delete Client Log
 * @apiVersion 0.1.0
 * @apiName Delete Client Log
 * @apiGroup Client Log
 * @apiDescription This route will delete an existing client log in the database
 * @apiParam {number} id The <code>id</code> of the client log to delete
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/1
 *
 */

router.delete('/api/clientlog/:id', requireUser, async function(req, res, next) {
    // for now just send back a 405: Method Not Allowed
    // res.status(405).json(null);

    // Coerce id into a number...
    const id = +req.params.id;

    // Only the user who owns the record should be allowed to delete it
    let tmpLog = await clientLogService.getClientLogById(id);
    if (!req.session.user || req.session.user.id !== tmpLog.get('userId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    }
    
    const log = await clientLogService.deleteClientLog(id);
    res.status(200).json(log.toViewObject());     
});

/**
 * ROUTE  /api/clientlog/export
 * 
 * @api {post} /api/clientlog/export Export Client Log
 * @apiVersion 0.1.0
 * @apiName Export Client Log
 * @apiGroup Client Log
 * @apiDescription This route will export the supplied array of client log entries
 * @apiParam {json} body An array of JSON Client Log objects
 * @apiSuccess (200) The exported log entries in XLS format
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/export
 *
 */

router.post('/api/clientlog/export', requireUser, async function(req, res, next) {

    var csv = exportfuncs.exportLog('csv', req.body);

    res.set('Content-Type', 'text/csv');
    res.send(csv);

    /*
    var dir = './tmp';
    var fname = 'export.csv';
    var fullname = dir + '/' + fname;
    if (!fs.existsSync(dir)){
      console.log('Making Directory', dir);
      fs.mkdirSync(dir);
    }
    fs.writeFileSync(fullname, csv);
    
    res.attachment(fullname);
    res.download(fullname, fname);
    */

    // res.set('Content-Type', 'application/vnd.ms-excel');
});

/**
 * ROUTE  /api/clientlog/print
 * 
 * @api {post} /api/clientlog/print Print Client Log
 * @apiVersion 0.1.0
 * @apiName Print Client Log
 * @apiGroup Client Log
 * @apiDescription This route will print the supplied array of client log entries
 * @apiParam {json} body An array of JSON Client Log objects
 * @apiSuccess (200) The printed log entries in PDF format
 * @apiExample Example Usage:
 *   http://localhost/api/clientlog/print
 *
 */

router.post('/api/clientlog/print', requireUser, async function(req, res, next) {

    // For now, return 405 - Not Implemented
    res.status(405).json(null);

    // var pdf = exportfuncs.exportLog('pdf', req.body);

    /*
    var dir = './tmp';
    var fname = 'log.pdf';
    var fullname = dir + '/' + fname;
    if (!fs.existsSync(dir)){
      console.log('Making Directory', dir);
      fs.mkdirSync(dir);
    }
    fs.writeFileSync(fullname, csv);
    
    res.download(fullname, fname);
    */

});