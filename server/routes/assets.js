const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    assetsService = require('../services').assets,
    conf = require('../lib/conf'),
    s3funcs = require('../lib/s3funcs'),
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/assets/:id
 * 
 * @api {get} /api/assets/:id Get Assets By Id
 * @apiVersion 0.1.0
 * @apiName Get Assets By Id
 * @apiGroup Assets
 * @apiDescription This route retrieves the full asset object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the asset to retrieve
 * @apiSuccess {json} asset Json object containing the retrieved Asset attributes
 * @apiExample Example Usage:
 *   http://localhost/api/assets/1
 */

router.get('/api/assets/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const asset = await assetsService.getAssetById(req.params.id);

    res.status(200).json(asset.toViewObject());
});

/**
 * ROUTE  /api/assets/user/:id
 * 
 * @api {get} /api/assets/user/:id Get Assets By User
 * @apiVersion 0.1.0
 * @apiName Get Assets By User
 * @apiGroup Assets
 * @apiDescription This route retrieves the a collection of asset objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retrieve Assets for
 * @apiSuccess {json} assets Json object containing the retrieved Assets
 * @apiExample Example Usage:
 *   http://localhost/api/assets/user/1
 */

router.get('/api/assets/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assets = await assetsService.getAssetsByUser(req.params.id);

    res.status(200).json(assets.map(assets => assets.toViewObject()));
});

/**
 * ROUTE  /api/assets
 * 
 * @api {post} /api/assets Create Asset
 * @apiVersion 0.1.0
 * @apiName Create Asset
 * @apiGroup Assets
 * @apiDescription This route will create a new Asset in the database
 * @apiParam {json} body Json object containing the Asset be created
 * @apiSuccess (201) {json} asset Json object containing the created Asset
 * @apiExample Example Usage:
 *   http://localhost/api/assets
 */

router.post('/api/assets', requireUser, async function(req, res, next) {
    const createdAsset = await assetsService.createAsset(req.body);

    res.status(201).json(createdAsset.toViewObject());
});

/**
 * ROUTE  /api/assets/:id
 * 
 * @api {patch} /api/assets/:id Update Asset
 * @apiVersion 0.1.0
 * @apiName Update Asset
 * @apiGroup Assets
 * @apiDescription This route will update an existing Asset in the database
 * @apiParam {number} id The <code>id</code> of the Asset to be updated
 * @apiParam {json} body Json object containing the Asset attributes to update
 * @apiSuccess {json} asset Json object containing the updated Asset
 * @apiExample Example Usage:
 *   http://localhost/api/assets/1
 */
router.patch('/api/assets/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // Only the user who owns the record should be allowed to update it
    let a = await assetsService.getAssetById(id);
    if (!req.session.user || req.session.user.id !== a.get('userId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const asset = await assetsService.updateAsset(id, req.body);

    res.status(200).json(asset.toViewObject());
});

/**
 * ROUTE  /api/assets/:id
 * 
 * @api {delete} /api/assets/:id Delete Asset
 * @apiVersion 0.1.0
 * @apiName Delete Asset
 * @apiGroup Assets
 * @apiDescription This route will delete an existing Asset in the database
 * @apiParam {number} id The <code>id</code> of the Asset to delete
 * @apiExample Example Usage:
 *   http://localhost/api/assets/1
 *
 */

router.delete('/api/assets/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // Only the user who owns the record should be allowed to delete it
    let a = await assetsService.getAssetById(id);
    if (!req.session.user || req.session.user.id !== a.get('userId')) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    }
    
    const asset = await assetsService.deleteAsset(id);
    res.status(200).json(asset.toViewObject()); 
});