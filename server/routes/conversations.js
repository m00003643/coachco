const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    conversationsService = require('../services').conversations,
    userService = require('../services').user,
    BadRequestError = require('../lib/custom-errors').BadRequestError,
    ResourceNotFoundError = require('../lib/custom-errors').ResourceNotFoundError;

/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/conversations/:id
 * 
 * @api {get} /api/conversations/:id Get Conversation By Id
 * @apiVersion 0.1.0
 * @apiName Get Conversation By Id
 * @apiGroup Conversation
 * @apiDescription This route retrieves the full Conversation object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the Conversation to retrieve
 * @apiSuccess {json} conversation Json object containing the retrieved Conversation attributes
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/1
 */

router.get('/api/conversations/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const conversation = await conversationsService.getConversationById(req.params.id);

    res.status(200).json(conversation.toViewObject());
});

/**
 * ROUTE  /api/conversations/user/:id
 * 
 * @api {get} /api/conversations/user/:id Get Conversation By User
 * @apiVersion 0.1.0
 * @apiName Get Conversation By User
 * @apiGroup Conversation
 * @apiDescription This route retrieves a Conversation object based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retreive Conversation for
 * @apiSuccess {json} conversation Json object containing the retrieved Conversation object
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/user/1
 */

router.get('/api/conversations/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const conversation = await conversationsService.getConversationByUser(req.params.id);

    // service now uses fetchAll so will return an array of Models, or an empty array if no conversation...  
    // Pull out the conversation and return it.  If it is empty, throw a an error (since the Service file no longer will)
    let c = {};
    if (conversation.size() > 0) { 
      c = conversation.at(0);   
    } else {
      throw new ResourceNotFoundError();      
    }

    res.status(200).json(c.toViewObject());
});

/**
 * ROUTE  /api/conversations/coach/:id
 * 
 * @api {get} /api/conversations/coach/:id Get Conversation By Coach
 * @apiVersion 0.1.0
 * @apiName Get Conversation By Coach
 * @apiGroup Conversation
 * @apiDescription This route retrieves all Conversation objects based on the supplied coach <code>id</code>
 * @apiParam {number} id The <code>id</code> of the coach to retreive Conversations for
 * @apiSuccess {json} conversation Json object containing the retrieved Conversation objects
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/coach/1
 */

router.get('/api/conversations/coach/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const conversation = await conversationsService.getConversationsByCoach(req.params.id);

    res.status(200).json(conversation.map(conversation => conversation.toViewObject()));
});

/**
 * ROUTE  /api/conversations/usertimestamp/:id
 * 
 * @api {get} /api/conversations/usertimestamp/:id Get Conversation Timestamp By User
 * @apiVersion 0.1.0
 * @apiName Get Conversation Timestamp By User
 * @apiGroup Conversation
 * @apiDescription This route retrieves a Conversation Timestamp based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retreive Conversation for
 * @apiSuccess {json} timestamp of the requested conversation
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/usertimestamp/2
 */

router.get('/api/conversations/usertimestamp/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const conversations = await conversationsService.getConversationByUser(req.params.id);

    // We only need to return the UPDATED field, not the entire object... Note we return an Array with only the one item in it 
    // to make the parser on the client side happy (it works with both the single Client and Coach versions of Timestamp calls)
    let tsArray = [];
    // let tsArrayItem = {'userId': conversation.get('userId'), 'updated':conversation.get('updated')};
    // tsArray.push(tsArrayItem);
    for (i=0; i < conversations.size(); i++) {
      var c = conversations.at(i);
      var tsArrayItem = {'userId': c.get('userId'), 'updated': c.get('updated')};
      tsArray.push(tsArrayItem);
    }

    res.status(200).json(tsArray);
});

/**
 * ROUTE  /api/conversations/coachtimestamp/:id
 * 
 * @api {get} /api/conversations/coachtimestamp/:id Get Conversation Timestamps By Coach
 * @apiVersion 0.1.0
 * @apiName Get Conversation Timestamps By Coach
 * @apiGroup Conversation
 * @apiDescription This route retrieves a Conversation Timestamps based on the supplied coach <code>id</code>
 * @apiParam {number} id The <code>id</code> of the coach to retreive Conversation timestamps for
 * @apiSuccess {json} timestamps of the requested conversations
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/coachtimestamp/1
 */

router.get('/api/conversations/coachtimestamp/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const conversations = await conversationsService.getConversationsByCoach(req.params.id);

    // For each one we found, return just the user and the timestamp.
    let tsArray = [];
    for (i=0; i < conversations.size(); i++) {
      var c = conversations.at(i);
      var tsArrayItem = {'userId': c.get('userId'), 'updated': c.get('updated')};
      tsArray.push(tsArrayItem);
    }

    res.status(200).json(tsArray);
});

/**
 * ROUTE  /api/conversations
 * 
 * @api {post} /api/conversations Create Conversation
 * @apiVersion 0.1.0
 * @apiName Create Conversation
 * @apiGroup Conversation
 * @apiDescription This route will create a new Conversation in the database
 * @apiParam {json} body Json object containing the Conversation to be created
 * @apiSuccess (201) {json} conversation Json object containing the created Conversation
 * @apiExample Example Usage:
 *   http://localhost/api/conversations
 */

router.post('/api/conversations', requireUser, async function(req, res, next) {

    const createdConversation = await conversationsService.createConversation(req.body);

    res.status(201).json(createdConversation.toViewObject());
});

/**
 * ROUTE  /api/conversations/:id
 * 
 * @api {patch} /api/conversations/:id Update Conversation
 * @apiVersion 0.1.0
 * @apiName Update Conversation
 * @apiGroup Conversation
 * @apiDescription This route will update an existing Conversation in the database
 * @apiParam {number} id The <code>id</code> of the Conversation to be updated
 * @apiParam {json} body Json object containing the Conversation attributes to update
 * @apiSuccess {json} conversation Json object containing the updated Conversation
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/1
 */

router.patch('/api/conversations/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // You should be logged in as the user who owns this record, or be their coach... 
    let c = await conversationsService.getConversationById(id);
    let u = await userService.getUserById(c.get('userId'));
    if (req.session.user.id !== c.get('userId') && req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const conversation = await conversationsService.updateConversation(id, req.body);

    res.status(200).json(conversation.toViewObject());
});

/**
 * ROUTE  /api/conversations/:id
 * 
 * @api {delete} /api/conversations/:id Delete Conversation
 * @apiVersion 0.1.0
 * @apiName Delete Conversation
 * @apiGroup Conversation
 * @apiDescription This route will delete an existing Conversation in the database
 * @apiParam {number} id The <code>id</code> of the Conversation to delete
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/1
 *
 */

router.delete('/api/conversations/:id', requireUser, async function(req, res, next) {
    // Coerce id into a number...
    const id = +req.params.id;

    // Who should be able to delete assessment records? Probably NOT the client they are assigned to (userId) 
    // but perhaps the coach that assigned them? Going with Coach who assigned it for now...
    let c = await conversationsService.getConversationById(id);
    let u = await userService.getUserById(c.get('userId'));
    if (req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    } 

    const conversation = await conversationsService.deleteConversation(id);
    res.status(200).json(conversation.toViewObject());    
});

/**
 * ROUTE  /api/conversations/getindicators/:role/:id
 * 
 * @api {get} /api/conversations/getindicators/:role/:id Get Conversation Indicators By User
 * @apiVersion 0.1.0
 * @apiName Get Conversation Indicators By User
 * @apiGroup Conversation
 * @apiDescription This route retrieves a Conversation Indicators based on the supplied user <code>id</code> and <code>role</code>
 * @apiDescription Note that for Coaches, passing in a role of COACH will give you back a summary of conversations with their clients,
 * @apiDescription but passing in a role of CLIENT will give you a summary of their conversation with their own (mentor) coach. 
 * @apiParam {number} id The <code>id</code> of the user to retreive Conversation for
 * @apiParam {string} role The <code>role</code> requested, either COACH or CLIENT
 * @apiSuccess {json} array of indicators of the requested conversation(s)
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/getindicators/CLIENT/2
 *   http://localhost/api/conversations/getindicators/COACH/1
 */

router.get('/api/conversations/getindicators/:role/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    // Get our array of conversation objects based on the requeted ID and ROLE...
    let role = req.params.role.toUpperCase();
    var conversations = [];
    if (role == 'CLIENT') {
      conversations = await conversationsService.getConversationByUser(req.params.id);
    } else if (role == 'COACH') {
      conversations = await conversationsService.getConversationsByCoach(req.params.id);
    } else {
      throw new BadRequestError('Invalid Role (' + req.params.role + ')');
    }

    // We don't want the entire Model object for each result.  Loop thru the models, and build an array with just the info we
    // need to return.   
    let tsArray = [];
    for (i=0; i < conversations.size(); i++) {
      let c = conversations.at(i);
      let conv = c.get('conversation');
      // If the Conversation field does not have these indicatgors keys in it yet, add default values to be returned...
      if (!conv.hasOwnProperty('seenByCoach')) { conv.seenByCoach = true; }
      if (!conv.hasOwnProperty('seenByClient')) { conv.seenByClient = true; }
      var tsArrayItem = {'userId': c.get('userId'), 'created': c.get('created'), 'updated': c.get('updated'), 'seenByCoach': conv.seenByCoach, 'seenByClient': conv.seenByClient};
      tsArray.push(tsArrayItem);
    }

    res.status(200).json(tsArray);
});

/**
 * ROUTE  /api/conversations/setindicators/:indicator/:value/:id
 * 
 * @api {patch} /api/conversations/setindicators/:indicator/:value/:id Set Conversation Indicators By User
 * @apiVersion 0.1.0
 * @apiName Set Conversation Indicators By User
 * @apiGroup Conversation
 * @apiDescription This route sets  Conversation Indicators based on the supplied conversation <code>id</code>
 * @apiParam {number} id The <code>id</code> of the conversation to update
 * @apiParam {string} indicator The <code>indicator</code> to be set, either 'seenByCoach' or 'seenByClient'
 * @apiParam {string} value The <code>value</code> the indicator should be set to, true or false
 * @apiSuccess {json} success 
 * @apiExample Example Usage:
 *   http://localhost/api/conversations/setindicators/seenByCoach/Y/1
 */

router.patch('/api/conversations/setindicators/:indicator/:value/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    // Make sure the indictor supplied makes sense...
    if ( (req.params.indicator != 'seenByCoach') && (req.params.indicator != 'seenByClient') ) {
        throw new BadRequestError('Invalid Indicator (' + req.params.indicator + ')');      
    }

    // Make sure the value supplied makes sense...
    let bValue = ( (req.params.value.toLowerCase() == 'true') || (req.params.value.toLowerCase() == 'y') || (req.params.value == '1'));

    // Call the service to update the requested indicator to the requeted value...
    // const updatedConversation = await conversationsService.setIndicators(req.params.id, req.params.indicator, req.params.value.toUpperCase());
    const updatedConversation = await conversationsService.setIndicators(req.params.id, req.params.indicator, bValue);

    // Return Success with an empty JSON object.
    res.status(200).json({});
});






