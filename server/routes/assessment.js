const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    notifyutils = require('../lib/notification-utils'),
    assessmentService = require('../services').assessment,
    userService = require('../services').user,
    BadRequestError = require('../lib/custom-errors').BadRequestError;
 
/**
 * @apiDefine 201 Success 201
 */

/**
 * ROUTE  /api/assessments/:id
 * 
 * @api {get} /api/assessments/:id Get Assessment By Id
 * @apiVersion 0.1.0
 * @apiName Get Assessment By Id
 * @apiGroup Assessments
 * @apiDescription This route retrieves the full Assessment object based on the supplied <code>id</code>
 * @apiParam {number} id The <code>id</code> of the Assessment to retrieve
 * @apiSuccess {json} assessment Json object containing the retrieved Assessment attributes
 * @apiExample Example Usage:
 *   http://localhost/api/assessments/1
 */

router.get('/api/assessments/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assessment = await assessmentService.getAssessmentById(req.params.id);

    res.status(200).json(assessment.toViewObject());
});

/**
 * ROUTE  /api/assessments/user/:id
 * 
 * @api {get} /api/assessments/user/:id Get Assessments By User
 * @apiVersion 0.1.0
 * @apiName Get Assessments By User
 * @apiGroup Assessments
 * @apiDescription This route retrieves a collection of Assessment objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retreive Assessments for
 * @apiSuccess {json} assessments Json object containing the retrieved Assessment objects
 * @apiExample Example Usage:
 *   http://localhost/api/assessments/user/1
 */

router.get('/api/assessments/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assessments = await assessmentService.getAssessmentsByUser(req.params.id);

    res.status(200).json(assessments.map(assessments => assessments.toViewObject()));
});

/**
 * ROUTE  /api/assessments/sharedwithuser/:id
 * 
 * @api {get} /api/assessments/sharedwithuser/:id Get Assessments Shared with a User
 * @apiVersion 0.1.0
 * @apiName Get Assessments Shared with a User
 * @apiGroup Assessments
 * @apiDescription This route retrieves a collection of Assessment objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retreive Assessments for
 * @apiSuccess {json} assessments Json object containing the retrieved Assessment objects
 * @apiExample Example Usage:
 *   http://localhost/api/assessments/sharedwithuser/1
 */

router.get('/api/assessments/sharedwithuser/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assessments = await assessmentService.getAssessmentsSharedWithUser(req.params.id);

    // Convert collection of assessment (models) to JSON, loop thru them and add some data fields we could not get from the query...
    let aTemp = assessments.toJSON({shallow: true});
    for (i=0;i<aTemp.length;i++) {
      aTemp[i].clientname = aTemp[i].lastname + ', ' + aTemp[i].firstname.slice(0,1);    
      let u = await userService.getUserById(aTemp[i].coachId);
      if (u) {
        aTemp[i].coachname = u.get('firstname') + ' ' + u.get('lastname');    
      } else {
        aTemp[i].coachname = null;
      }
    }
 
    // We can't use toViewObject() from the model here, as it strips out any non public keys from the model.  Our JOIN includes 
    // fields from other places which toViewObject() would strip out.  Return the raw JSON object instead.
    res.status(200).json(aTemp);
});

/**
 * ROUTE  /api/assessments
 * 
 * @api {post} /api/assessments Create Assessment
 * @apiVersion 0.1.0
 * @apiName Create Assessment
 * @apiGroup Assessments
 * @apiDescription This route will create a new Assessment in the database
 * @apiParam {json} body Json object containing the Assessment to be created
 * @apiSuccess (201) {json} assessment Json object containing the created Assessment
 * @apiExample Example Usage:
 *   http://localhost/api/assessments
 */

router.post('/api/assessments', requireUser, async function(req, res, next) {
    const createdAssessment = await assessmentService.createAssessment(req.body);
      
    if ( (createdAssessment) && (req.session.user.settings.sendEmailNotifications.value) ) {
      // Get the user record of who this Assessment is for, so we can grab Name fields for the notification email...
      let u = await userService.getUserById(createdAssessment.get('userId'));

      // Setup options to send to Mailer.
      var opts = {
            reason: 'notificationNewAssessment',
            protocol: req.protocol, 
            baseUrl: req.get('host'), 
            email: u.get('email'), 
            inviteCode: null, // Not needed for Assessment Notifications...
            firstName: u.get('firstname'), 
            lastName: u.get('lastname')
          };
          
      if (u.get('coachId') != null) {
        // Get Coach user object so we can get the email text for that Coach from his/her branding config.
        let coach = await userService.getUserById(u.get('coachId'));

        // @TODO - What if the coach comes back null or without the branding?

        opts.emailTemplate = coach.get('branding').assessmentNotificationEmail.join('');
        opts.companyName = coach.get('branding').companyName;
        opts.coachFirstName = coach.get('firstname');
        opts.coachLastName = coach.get('lastname');
        opts.brandEmailLogo = coach.get('branding').companyLogo;
      } else {
        // Invitee has no Coach defined, so use system defaults instead...

        // @TODO - We should try to get them from the APP_DEFAULTS table before we get them from the config file...

        opts.emailTemplate = conf.brand.assessmentNotificationEmail.join('');
        opts.companyName = conf.brand.company;
        opts.brandEmailLogo = conf.brand.logo;
      }

      console.log('OPTS : ', opts);

      notifyutils.sendnotificationmail( opts );
    }

    res.status(201).json(createdAssessment.toViewObject());
});

/**
 * ROUTE  /api/assessments/:id
 * 
 * @api {patch} /api/assessments/:id Update Assessment
 * @apiVersion 0.1.0
 * @apiName Update Assessment
 * @apiGroup Assessments
 * @apiDescription This route will update an existing Assessment in the database
 * @apiParam {number} id The <code>id</code> of the Assessment to be updated
 * @apiParam {json} body Json object containing the Assessment attributes to update
 * @apiSuccess {json} assessment Json object containing the updated Assessment
 * @apiExample Example Usage:
 *   http://localhost/api/assessments/1
 */

router.patch('/api/assessments/:id', requireUser, async function(req, res, next) {
    // @todo:
    // - loop through all the prompts in the fetched assessment, and store the corresponding values
    //   that were posted

    // Coerce id into a number...
    const id = +req.params.id;

    // You should be logged in as the user who owns this record, or be their coach... 
    let a = await assessmentService.getAssessmentById(id);
    let u = await userService.getUserById(a.get('userId'));
    if (req.session.user.id !== a.get('userId') && req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to update.');
    }

    const assessment = await assessmentService.updateAssessment(id, req.body);

    res.status(200).json(assessment.toViewObject());
});

/**
 * ROUTE  /api/assessments/:id
 * 
 * @api {delete} /api/assessments/:id Delete Assessment
 * @apiVersion 0.1.0
 * @apiName Delete Assessment
 * @apiGroup Assessments
 * @apiDescription This route will delete an existing Assessment in the database
 * @apiParam {number} id The <code>id</code> of the Assessment to delete
 * @apiExample Example Usage:
 *   http://localhost/api/assessments/1
 *
 */

router.delete('/api/assessments/:id', requireUser, async function(req, res, next) {
    // for now just send back a 405: Method Not Allowed
    // res.status(405).json(null);

    // Coerce id into a number...
    const id = +req.params.id;

    // Who should be able to delete assessment records? Probably NOT the client they are assigned to (userId) 
    // but perhaps the coach that assigned them? Going with Coach who assigned it for now...
    let a = await assessmentService.getAssessmentById(id);
    let u = await userService.getUserById(a.get('userId'));
    if (req.session.user.id !== u.get('coachId') ) {
        throw new BadRequestError('You must be authenticated as the user which owns the records you are attempting to delete.');
    } 

    const assessment = await assessmentService.deleteAssessment(id);
    res.status(200).json(assessment.toViewObject());    
});