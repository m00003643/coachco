const router = module.exports = require('node-async-router')(),
    requireUser = require('./middleware/require-user'),
    clientAssetsService = require('../services').clientassets,
    BadRequestError = require('../lib/custom-errors').BadRequestError;

/**
 * ROUTE  /api/clientassets/user/:id
 * 
 * @api {get} /api/clientassets/user/:id Get Client Assets by User
 * @apiVersion 0.1.0
 * @apiName Get Client Assets by User
 * @apiGroup Client Assets
 * @apiDescription This route retrieves the collection of client asset objects based on the supplied user <code>id</code>
 * @apiParam {number} id The <code>id</code> of the user to retrieve client assets for
 * @apiSuccess {json} Log Json object containing the retrieved client assets
 * @apiExample Example Usage:
 *   http://localhost/api/clientassets/user/1
 */

router.get('/api/clientassets/user/:id', requireUser, async function(req, res, next) {
    // punt on anything that's not a numerical id
    if (!/^\d+$/.test(req.params.id)) return next();

    const assets = await clientAssetsService.getClientAssetsByUser(req.params.id);

    res.status(200).json(assets.map(assets => assets.toViewObject()));
});