require('babel-polyfill');
require('babel-register')({
    extensions: ['.js'],
    only: /server/,
    plugins: ['transform-async-to-generator','babel-plugin-transform-object-rest-spread']
});

const assert = require('assert'),
    request = require('supertest'),
    async = require('async'),
    responseAssertions = require('./helpers/response-assertions'),
    resources = require('./helpers/resources'),
    // store some values that get shared between multiple test cases
    cache = Object.create(null),
    appInit = require('../server/app');

let app, agent;

describe('API', function() {
    // increase how long we wait before failing the test
    this.timeout(10000);

    before(function(done) {
        appInit(function(err, theApp) {
            if (err) return done(err);

            app = theApp;
            agent = request.agent(app);
            done();
        });
    });

    //
    // Users
    //

    describe('POST: /api/users', function() {

        it('should work', function(done) {
            async.eachLimit(resources.users, 20, function(user, next) {
                // @note: a single supertest agent gets confused with multiple
                // parallel requests, so we just kick these off outside of the
                // agent itself.
                request(app)
                    .post('/api/users')
                    .send(user)
                    .expect(201)
                    .expect(responseAssertions.successEnvelope)
                    .expect(responseAssertions.resultIsUser)
                    .end(next);
            }, done);
        });

        it('should enforce required keys', function(done) {
            agent.post('/api/users')
                .send({})
                .expect(400)
                .expect(responseAssertions.failureEnvelope)
                .expect(responseAssertions.validationFailed)
                .end(done);
        });

    });

    describe('GET: /api/users/:username', function() {

        it('should return requested user', function(done) {
            agent.get('/api/users/user_7')
                .expect(200)
                .expect(responseAssertions.successEnvelope)
                .expect(responseAssertions.resultIsUser)
                .expect(responseAssertions.resultPropEquals('username', 'user_7'))
                .end(function(err, res) {
                    if (err) return done(err);
                    cache.user7Id = res.body.result.id;
                    done();
                });
        });

        it('should 404 on non-existent username', function(done) {
            agent.get('/api/users/user_999999')
                .expect(404)
                .expect(responseAssertions.failureEnvelope)
                .end(done);
        });

    });

    describe('GET: /api/users/:id', function() {

        it('should return requested user', function(done) {
            agent.get('/api/users/' + cache.user7Id)
                .expect(200)
                .expect(responseAssertions.successEnvelope)
                .expect(responseAssertions.resultIsUser)
                .expect(responseAssertions.resultPropEquals('username', 'user_7'))
                .end(done);
        });

        it('should 404 on non-existent id', function(done) {
            agent.get('/api/users/999999')
                .expect(404)
                .expect(responseAssertions.failureEnvelope)
                .end(done);
        });

    });

    describe('GET: /auth/login', function() {

        it('should fail for invalid credentials', function(done) {
            agent.post('/api/login')
                .send({ username: 'user_1', password: 'wrong-password' })
                .expect(responseAssertions.authenticationFailed)
                .end(done);
        });

        // note: following this test, `agent` will be authenticated as @user_7
        it('should succeed and return user for valid credentials', function(done) {
            agent.post('/api/login')
                .send({ username: 'user_7', password: 'P@ssword7' })
                .expect(200)
                .expect(responseAssertions.successEnvelope)
                .expect(responseAssertions.resultIsUser)
                .expect(responseAssertions.resultPropEquals('username', 'user_7'))
                .end(done);
        });

    });

    describe('PATCH: /api/users/:id', function() {

        before(function(done) {
            // we need another valid user id for one of the tests...
            agent.get('/api/users/user_1')
                .end(function(err, res) {
                    if (err) return done(err);
                    cache.user1Id = res.body.result.id;
                    done();
                });
        });

        it('should 401 if not authenticated', function(done) {
            // note: we're intentionally using `request` instead of our `agent` here to insure
            // that we aren't sending up our session cookie
            request(app)
                .patch('/api/users/' + cache.user7Id)
                .send({ email: 'updated@example.com' })
                .expect(responseAssertions.authenticationRequired)
                .end(done);
        });

        it('should 400 if authenticated as the wrong user', function(done) {
            agent.patch('/api/users/' + cache.user1Id)
                .send({ email: 'updated@example.com' })
                .expect(responseAssertions.badRequest)
                .end(done);
        });

        it('should work if authenticated as the right user', function(done) {
            agent.patch('/api/users/' + cache.user7Id)
                .send({ firstname: 'First7Updated' })
                .expect(200)
                .expect(responseAssertions.resultIsUser)
                .expect(responseAssertions.resultPropEquals('firstname', 'First7Updated'))
                .end(done);
        });

        it('should persist updates', function(done) {
            agent.get('/api/users/user_7')
                .expect(responseAssertions.resultPropEquals('firstname', 'First7Updated'))
                .end(done);
        });

    });

});