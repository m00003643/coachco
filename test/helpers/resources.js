exports.users = [];

// note: 51 so we can test paging (defaults to showing 50 per page)
for (var i = 0; i < 51; i++) {
    exports.users.push({
        username: 'user_' + (i + 1),
        email: 'user_' + (i + 1) + '@example.com',
        password: 'P@ssword' + (i + 1),
        firstname: 'First' + (i + 1),
        lastname: 'Last' + (i + 1)
    });
}