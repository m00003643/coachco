var assert = require('assert');

//
// response envelope assertions
//

exports.successEnvelope = function(res) {
    assert.equal(typeof res.body, 'object');
    assert.strictEqual(res.body.success, true);
    assert.notEqual(typeof res.body.result, 'undefined');
};

exports.failureEnvelope = function(res) {
    assert.equal(typeof res.body, 'object');
    assert.strictEqual(res.body.success, false);
    assert.equal(typeof res.body.errorCode, 'string');
    assert.equal(typeof res.body.errorMessage, 'string');
};

//
// error response assertions
//

exports.authenticationRequired = function(res) {
    assert.strictEqual(res.statusCode, 401);
    assert.strictEqual(res.body.success, false);
    assert.strictEqual(res.body.errorCode, 'AUTHENTICATION_REQUIRED');
}

exports.authenticationFailed = function(res) {
    assert.strictEqual(res.statusCode, 403);
    assert.strictEqual(res.body.success, false);
    assert.strictEqual(res.body.errorCode, 'AUTHENTICATION_FAILED');
}

exports.badRequest = function(res) {
    assert.strictEqual(res.statusCode, 400);
    assert.strictEqual(res.body.success, false);
    assert.strictEqual(res.body.errorCode, 'BAD_REQUEST');
}

exports.validationFailed = function(res) {
    assert.strictEqual(res.body.success, false);
    assert.strictEqual(res.body.errorCode, 'RESOURCE_VALIDATION_FAILED');
    assert.strictEqual(typeof res.body.validationErrors, 'object');
}

//
// general result assertions
//

exports.resultIsNull = function(res) {
    assert.strictEqual(res.body.result, null);
};

exports.resultPropEquals = function(prop, val) {
    return function(res) {
        assert.strictEqual(res.body.result[prop], val);
    };
};

exports.resultHasPropOfType = function(prop, type) {
    return function(res) {
        assert.strictEqual(typeof res.body.result[prop], type);
    };
};

exports.resultIsArrayOrderedById = function(startId) {
    return function(res) {
        var result = res.body.result;
        assert(Array.isArray(result));

        var prevId = startId;

        if (typeof prevId === 'undefined') prevId = result[0].id;

        result.forEach(function(item) {
            assert.strictEqual(item.id, prevId, item.id + ' does not immediately follow ' + prevId);
            prevId++;
        });
    };
};

exports.resultIsArrayOrderedAlphabetically = function(sortProp) {
    return function(res) {
        var result = res.body.result;
        assert(Array.isArray(result));

        var prevVal = '';

        result.forEach(function(item) {
            var curVal = item[sortProp];
            assert(curVal >= prevVal, curVal + ' does not alphabetically follow ' + prevVal);
            prevVal = curVal;
        });
    };
};

//
// user response assertions
//

exports.resultIsUser = function(res) {
    assertIsUser(res.body.result);
};

exports.resultIsArrayOfUsers = function(res) {
    res.body.result.forEach(assertIsUser);
};

function assertIsUser(obj) {
    // just look for the existence of a few properties
    assert.equal(typeof obj.id, 'number');
    assert.equal(typeof obj.username, 'string');
};