import React from 'react';
import { connect } from 'react-redux';
import { AppBar, Button, Input } from 'react-toolbox';
import styles from './app.scss';
import AppStyles from '../common/styles/app.scss';
import { selectors as brandSelectors } from '../administration/branding-state';

class RegisterPage extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			phone: '',
			email: '',
      first_name: '',
      last_name: '',
      comments: '',
			disabled: true
		}

    this.handleClick = this.handleClick.bind(this);
		
	}

	handleClick() {
		console.log(this.state);
	}

	handleChange(name, value) {
		this.setState( {...this.state, [name]: value});
		let email = (name == 'email') ? value : this.state.email;
		let phone = (name == 'phone') ? value : this.state.phone;
		let first_name = (name == 'first_name') ? value : this.state.first_name;
		let last_name = (name == 'last_name') ? value : this.state.last_name;
		let comments = (name == 'comments') ? value : this.state.comments;
		this.setState( {disabled: (email != '' || phone != '' || first_name != '' || last_name != '' || comments != '') ? false : true} );
	}

	render() {

		const { brand } = this.props;

		return (
			<div>
					<AppBar title={brand.companyName} />
		    	<section className={styles.main_panel} >

						<div style={{textAlign:"center"}}>
							<img src={brand.companyLogo} />
							<br />
							<h3>Welcome to {brand.companyName}</h3>
							<p>This site is intended for existing coaches and clients of {brand.companyName}</p>
							<br /><br />
						</div>

						<div className={styles.register_input_fields}>
							<span className={AppStyles.page_title}>Request information or an invitation</span>
							<p>Enter required fields below then
								click &#39;Inquire&#39;</p>
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='First Name' name='first_name' value={this.state.first_name} 
								onChange={this.handleChange.bind(this, 'first_name')} maxLength={15} required />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='Last Name' name='last_name' value={this.state.last_name} 
								onChange={this.handleChange.bind(this, 'last_name')} maxLength={20} required />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='Email' name='email' value={this.state.email} 
								onChange={this.handleChange.bind(this, 'email')} maxLength={35} required />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='Phone' name='phone' value={this.state.phone} 
								onChange={this.handleChange.bind(this, 'phone')} maxLength={16} required />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='Comments' name='comments' value={this.state.comments} 
								onChange={this.handleChange.bind(this, 'comments')} maxLength={2000} 
                multiline rows={5} />
						</div>

						<div className={styles.register_input_fields} style={{textAlign:'center'}}>
							<Button label='Inquire' raised disabled={this.state.disabled} 
								onClick={this.handleClick} />
						</div>

	  	  	</section >
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {

  return {
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(RegisterPage);
