import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router';
import NotFound from './NotFound.jsx';
import Page from '../common/Page.jsx';
import LoginPage from '../auth/LoginPage.jsx';
import AssessmentsPage from '../assessments/AssessmentsPage.jsx';
import AssessmentPage from '../assessments/AssessmentPage.jsx';
import WelcomePage from './WelcomePage.jsx';
import DocumentsPage from '../assets/AssetsPage.jsx';
import ClientsPage from '../clients/ClientsPage.jsx';
import ClientPage from '../clients/ClientPage.jsx';
import LogPage from '../log/LogPage.jsx';
import ClientAssessmentPage from '../clients/ClientAssessmentPage';
import AddClientPage from '../clients/AddClientPage';
import ProfilePage from '../profile/ProfilePage.jsx';
import ProfileWithSettingsPage from '../profile/ProfileWithSettingsPage.jsx';
import LogEntryPage from '../log/LogEntryPage.jsx';
import LogNewEntryPage from '../log/LogNewEntryPage.jsx';
import LogAddPage from '../log/LogAddPage.jsx';
import AdminPage from '../administration/AdminPage.jsx';
import DevTestPage from '../devtest/DevTestPage.jsx';
import AssessmentTemplatesPage from '../administration/AssessmentTemplatesPage.jsx';
import AssetsPage from '../administration/AssetsPage.jsx';
import TemplateEditPage from '../administration/TemplateEditPage.jsx';
import TemplateAddPage from '../administration/TemplateAddPage.jsx';
import UploadAssetPage from '../administration/UploadAssetPage.jsx';
import RegisterPage from './RegisterPage.jsx';
import NewPage from './NewPage.jsx';
import BrandingPage from '../administration/BrandingPage.jsx';
import QuillPage from '../administration/QuillPage.jsx';
import SessionExpiredPage from '../auth/SessionExpiredPage.jsx';
import SharedAssessmentPage from '../clients/SharedAssessmentPage.jsx';
import ChatList from '../chat/ChatList.jsx';
import ChatPage from '../chat/ChatPage.jsx';

import 'react-toolbox/lib/commons.scss';

const Root = ({ store }) => {

  return (
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path='/' component={WelcomePage} />
        <Route path='/login' component={LoginPage} />
        <Route path='/register' component={RegisterPage} />
        <Route path='/new' component={NewPage} />
        <Route path='/assessments/:id' component={AssessmentPage} />
        <Route path='/assessments' component={AssessmentsPage} />
        <Route path='/assessments/shared/:id' component={SharedAssessmentPage} />
        <Route path='/documents' component={DocumentsPage} />
        <Route path='/clients' component={ClientsPage} />
        <Route path='/clients/add' component={AddClientPage} />
        <Route path='/clients/:id' component={ClientPage} />
        <Route path='/clients/:id/assessments/:id2' component={ClientAssessmentPage} />
        <Route path='/profile' component={ProfileWithSettingsPage} />
        <Route path='/log/add/:id' component={LogNewEntryPage} />
        <Route path='/log/add' component={LogAddPage} />
        <Route path='/log/:id' component={LogEntryPage} />
        <Route path='/logs' component={LogPage} />
        <Route path='/admin' component={AdminPage} />
        <Route path='/admin/assessment-templates' component={AssessmentTemplatesPage} />
        <Route path='/admin/assessment-templates/add' component={TemplateAddPage} />
        <Route path='/admin/assessment-templates/:id' component={TemplateEditPage} />
        <Route path='/admin/branding' component={BrandingPage} />
        <Route path='/admin/documents' component={AssetsPage} />
        <Route path='/admin/documents/upload/:id' component={UploadAssetPage} />
        <Route path='/admin/documents/:id' component={AssetsPage} />
        <Route path='/devtest' component={DevTestPage} />
        <Route path='/sessionexpired' component={SessionExpiredPage} />
        <Route path='/quill' component={QuillPage} />
        <Route path='/chats/:id' component={ChatList} />
        <Route path='/chat/:id' component={ChatPage} />
        <Route path="*" component={NotFound} />
      </Router>
    </Provider>
  );

};

export default Root;
