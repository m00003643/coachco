import React from 'react';
import { connect } from 'react-redux';
import { AppBar, Button, Input } from 'react-toolbox';
import styles from './app.scss';
import AppStyles from '../common/styles/app.scss';
import { VerifyInvite } from '../utils/api';
import { selectors as brandSelectors } from '../administration/branding-state';

class RegisterPage extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			code: '',
			email: '',
			username: '',
			password: '',
			password_again: '',
			disabled: true,
			message: '',
			codeProvided: false
		}

    this.handleClick = this.handleClick.bind(this);
		
	}

	componentDidMount() {

		if (typeof this.props.location.query.email === 'undefined') return;

		// Route contains email and invite code so set form defaults
		this.setState({
			email: this.props.location.query.email,
			code: this.props.location.query.invite,
			codeProvided: true
		})
	}

	async handleClick() {

		// Make sure passwords match
		if (this.state.password != this.state.password_again) {
			this.setState( { message: 'Passwords do not match'} );
			return;
		}

		// Test username is valid
		const re2 = /^[a-z][a-z0-9_]*$/;
		if (!re2.test(this.state.username) || this.state.username.length < 4 || this.state.username.length > 30) {
			this.setState( { message: 'Username invalid: must be between 4 and 30 charcters long, start with a letter, and only contain lower case, numeric, or _'} );
			return;
		}

		// Test password is valid
		const re = /(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&-])/;
		if (!re.test(this.state.password) || this.state.password < 8 ) {
			this.setState( { message: 'Password invalid: must be at least 8 character long and contain one each of upper, lower, numeric, and spectial (#?!@$%^&-)'} );
			return;
		}

		// Prepare data for api call
		const validation = {
			invitation: this.state.code,
			email: this.state.email,
			username: this.state.username,
			password: this.state.password
		}

		// Make the api call to validate
    try {
			await VerifyInvite(validation).then( data => {
				window.location = '/profile';
			});
    } catch (err) {
			console.log(err);
			this.setState( {message: 'Failed to validate user', active: false} );
    }
		
	}

	handleChange(name, value) {
		this.setState( {...this.state, [name]: value});
		let email = (name == 'email') ? value : this.state.email;
		let code = (name == 'code') ? value : this.state.code;
		let username = (name == 'username') ? value : this.state.username;
		let password = (name == 'password') ? value : this.state.password;
		let password_again = (name == 'password_again') ? value : this.state.password_again;
		this.setState( {disabled: 
			(email != '' || code != '' || username != '' || password != '' || password_again != '') ? false : true} );
	}

	render() {

		const { brand } = this.props;

		return (
			<div>
					<AppBar title={brand.companyName} />
		    	<section className={styles.main_panel} >

						<div style={{textAlign:"center"}}>
							<img src={brand.companyLogo} />
							<br />
							<h3>Welcome to {brand.companyName}</h3>
							<p>This site is intended for existing coaches and clients of {brand.companyName}</p>
							<br /><br />
						</div>

						<div className={styles.register_input_fields}>
							<span className={AppStyles.page_title}>Accept Invitation</span>
							<p>Please provide a valid user name and password as well as the email address you
								used to request an account and the invitation code you received, then
								click &#39;Accept&#39;</p>
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='username' name='username' value={this.state.username} 
								onChange={this.handleChange.bind(this, 'username')} maxLength={20} required />
							<label style={{fontSize: '1rem'}} >must be between 4 and 30 charcters long, start with a letter, and  only contain lower case, numeric, or _ </label>
 						</div>

						<div className={styles.register_input_fields}>
			        <Input type='password' label='password' name='password' value={this.state.password} 
								onChange={this.handleChange.bind(this, 'password')} maxLength={100} required />
							<label style={{fontSize: '1rem'}} >must be at least 8 character long and contain one each of upper, lower, numeric, and spectial (#?!@$%^&-)</label>
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='password' label='password (again)' name='password_again' value={this.state.password_again} 
								onChange={this.handleChange.bind(this, 'password_again')} maxLength={100} required />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='email' name='email' value={this.state.email} 
								onChange={this.handleChange.bind(this, 'email')} maxLength={100} required 
								disabled={this.state.codeProvided} />
						</div>

						<div className={styles.register_input_fields}>
			        <Input type='text' label='code' name='code' value={this.state.code} 
								onChange={this.handleChange.bind(this, 'code')} maxLength={16} required 
								disabled={this.state.codeProvided} />
						</div>

		        <div 
							style={{color:'red', textAlign:'center'}}
							className={styles.register_input_fields}>
							<p>{this.state.message}</p>
						</div>

						<div className={styles.register_input_fields} style={{textAlign:'center'}}>
							<Button label='Accept' raised disabled={this.state.disabled} 
								onClick={this.handleClick} />
						</div>

	  	  	</section >
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {

  return {
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(RegisterPage);
