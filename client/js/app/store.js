import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './root-reducer';

const store = createStore(
  rootReducer,
  compose(
    /**
     * Conditionally add the Redux DevTools extension enhancer
     * if it is installed.
     */
    window.devToolsExtension ? window.devToolsExtension() : f => f
  ));

export default store;