import React from 'react';
import { connect } from 'react-redux';
import { AppBar, Button } from 'react-toolbox';
import styles from './app.scss';
import AppStyles from '../common/styles/app.scss';
import { selectors as brandSelectors } from '../administration/branding-state';

class WelcomePage extends React.Component {

	render() {

		const { brand } = this.props;

		return (
			<div>
					<AppBar title={brand.companyName} />
		    	<section className={styles.main_panel} >

						<div style={{textAlign:"center"}}>
							<img src={brand.companyLogo} />
							<br />
							<h3>Welcome to {brand.companyName}</h3>
							<p>This site is intended for existing coaches and clients of {brand.companyName}</p>
							<br /><br />
						</div>

						<div className={styles.main_options_outline} >
							<p>Already a registered user? Proceed to login page.</p>
							<div className={styles.main_options_button} >
								<Button label='Login' href='/login' raised />
							</div>
						</div>

						<div className={styles.main_options_outline} >
							<p>Already invited? Enter your validation code and setup a personal account.</p>
							<div className={styles.main_options_button} >
								<Button label='Validate' href='/register' raised />
							</div>
						</div>

						<div className={styles.main_options_outline} >
							<p>Curious about {brand.companyName}? Contact us for more information or to request an invitation.</p>
							<div className={styles.main_options_button} >
								<Button label='Request' href='/new' raised />
							</div>
						</div>

	  	  	</section >
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {

  return {
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(WelcomePage);
