import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as assessmentReducer, stateKey as assessmentStateKey } from '../assessments/assessments-state';
import { reducer as userReducer, stateKey as userStateKey } from '../user/user-state';
import { reducer as clientReducer, stateKey as clientStateKey } from '../clients/clients-state';
import { reducer as assetsReducer, stateKey as assetsStateKey } from '../assets/assets-state';
import { reducer as logsReducer, stateKey as logsStateKey } from '../log/log-state';
import { reducer as templateReducer, stateKey as templateStateKey } from '../templates/templates-state';
import { reducer as defaultAssetReducer, stateKey as defaultAssetStateKey } from '../templates/defaultAssets-state';
import { reducer as brandingReducer, stateKey as brandingStateKey } from '../administration/branding-state';
import { reducer as chatReducer, stateKey as chatStateKey } from '../chat/chat-state';

module.exports = combineReducers({
  form: formReducer,
  [assessmentStateKey]: assessmentReducer,
  [userStateKey]: userReducer,
  [clientStateKey]: clientReducer,
  [assetsStateKey]: assetsReducer,
  [logsStateKey]: logsReducer,
  [templateStateKey]: templateReducer,
  [defaultAssetStateKey]: defaultAssetReducer,
  [brandingStateKey]: brandingReducer,
  [chatStateKey]: chatReducer
});
