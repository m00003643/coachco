import React from 'react';
import { connect } from 'react-redux';
import ProfileForm from './ProfileForm.jsx';
import { Panel, AppBar} from 'react-toolbox';
import Page from '../common/Page.jsx';
import { selectors as profileSelectors } from './profile-state';
import AppStyles from '../common/styles/app.scss';

class ProfilePage extends React.Component {

	render() {

		const { profile, settings } = this.props;

		return (
			<Page >
				<span className={AppStyles.page_title}>My Profile</span>
				<ProfileForm profile={profile} settings={settings} />
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
		profile: profileSelectors.getUser(state),
		settings: {
    	"sendEmailNotifications": {
				"scope": "BOTH",
    		"label": "send Email Notifications",
      	"type": "boolean",
      	"value": true
    	},
    	"sendSMSNotifications": {
				"scope": "BOTH",
      	"label": "send SMS Notifications",
        "type": "boolean",
        "value": false
      },
      "sendSMSNumber": {
				"scope": "BOTH",
      	"label": "SMS Number",
        "type": "text",
        "value": "5551234567"
      },
      "allowedFileUploadSizeLimit": {
				"scope": "COACH",
        "label": "Maximum File Size (MB)",
        "options": [
        	0.5,
        	1,
        	2,
        	5,
        	10
        ],
        "type": "list",
        "value": 1
      },
      "chatPageRefreshInterval": {
				"scope": "BOTH",
      	"label": "Chat Page Refresh Interval (seconds)",
        "options": [
        	0,
          1,
          5,
          10,
          30,
          60
        ],
        "type": "list",
        "value": 10
      }
		}

	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(ProfilePage);
