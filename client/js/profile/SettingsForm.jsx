import React from 'react';
import { Switch, Input, Dropdown, Button } from 'react-toolbox';
import { updateUser } from '../utils/api';

class SettingsForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
			settings: this.props.settings,
			hasChanges: false
		};

    this.handleUpdate = this.handleUpdate.bind(this);

  }

	componentWillMount() {

		Object.keys(this.props.settings).forEach(key => {
			let value = this.props.settings[key].value;
			this.setState( { [key]: value} );
		})

	}

	async handleUpdate() {

		// Grab copy of original settings
		let settings = {};
    settings.settings = {...this.state.settings};

		// Update copy with changes made by user
		Object.keys(this.state.settings).forEach(key => {
			settings.settings[key].value = this.state[key];
		});

		console.log(JSON.stringify(settings.settings,null, 4));
    try {
      await updateUser(this.props.user.id, settings)
      .then( data => {
        location.hash = '#settings';
        location.reload();
      });

    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable to update settings' } );
    }
		
	}

  handleChange(field, value) {

		// See if there is any changes from the original settings
		//
		let hasChanges = false;
		Object.keys(this.state.settings).forEach(key => {
			let originalVal = this.state.settings[key].value;
			let currVal = this.state[key];
			if (key == field) { currVal = value; }
			if (currVal != originalVal) { hasChanges = true; }
		});

    this.setState({[field]: value, hasChanges: hasChanges});
  };

	getSettingsBlock(setting, value) {

		let key = setting.key;
		let label = setting.label;
		let type = setting.type;

		if (type == 'boolean') {
			return (
				<Switch
					checked={value}
					label={label}
					onChange={this.handleChange.bind(this, key)}
				/>
			)
		}

		if (type == 'text') {
			return (
				<Input label={label} required={true} 
					value={value} 
					onChange={this.handleChange.bind(this, key)} 
				/>
			)
		}

		if (type == 'list') {

			let options = [];
			setting.options.map(option => {
				options.push({
					value: option,
					label: option
				});
			});

			return (
				<Dropdown
					onChange={this.handleChange.bind(this, key)}
					source={options}
					value={value}
					label={label} 
				/>
			)
		}

	}

	render() {

		const { settings, role } = this.props;

		// Turn the settings prop into an array we can iterate on in React.
		// The array will only contain those settings whose scope matches those
		// of the current user (COACH, CLIENT) or are scoped as 'BOTH'.
		//
		let arrSettings = [];
		Object.keys(settings).forEach(key => {
			let settingScope = settings[key]['scope'].toLowerCase();
			if ( settingScope == role.toLowerCase() || settingScope == 'both') {
				let nextSetting = {};
				nextSetting.key = key;
				Object.keys(settings[key]).forEach(key2 => {
					nextSetting[key2] = settings[key][key2];
				})
				arrSettings.push(nextSetting);
			}
		});

		return (

			<div>

				<div style={{marginTop:'10px'}} >
				{ arrSettings.map((setting, index) => (
					<div key={index} style={{textAlign:'center', width:'300px', margin:'0 auto'}} >
						{this.getSettingsBlock(setting, this.state[setting.key])}
					</div>
				))}
				</div>

				<div style={{textAlign:'center'}} >
					<Button label='Update' onClick={this.handleUpdate} raised={true} 
						primary={true} style={{marginRight:'10px'}} 
						disabled={!this.state.hasChanges} />
				</div>

			</div>

		);
	}

}

// Returns a higer order component that wraps this component
export default SettingsForm;