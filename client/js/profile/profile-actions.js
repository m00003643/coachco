import { createAction } from 'redux-actions';

export const updateProfile = createAction('UPDATE_PROFILE', user => user);
