import React from 'react';
import { getFormValues, isInvalid, reduxForm, SubmissionError, Field as FieldRF } from 'redux-form';
import { updateUser } from '../utils/api';
import { addUser } from '../utils/api';
import { updateProfile } from './profile-actions';
import { Button, Dropdown, Tabs, Tab } from 'react-toolbox';
import Field from '../common/Field.jsx';
import DropdownField from '../common/DropdownField.jsx';
import { isNotEmail } from 'sane-email-validation';
import { normalizePhone } from '../common/Normalize.jsx';
import AppStyles from '../common/styles/app.scss';

const validate = values => {

  const errors = {};

  if (isNotEmail(values.email)) errors.email = 'Email is missing or invalid';

  if (typeof values.phone != 'undefined' && values.phone != null && values.phone != '') {
    const phoneNum = values.phone.replace(/[^0-9]/g, '');
    if (phoneNum.length < 10 || phoneNum.length > 12) errors.phone = 'Phone number format looks incorrect';
  }

  return errors;
}

class ProfileForm extends React.Component {

  constructor(props) {

    super(props);
    this.state = { 
      role: ''
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.renderRole = this.renderRole.bind(this);
    this.handleRoleChange = this.handleRoleChange.bind(this);

  }

  toggleBusy() {
    this.setState({ busy: !this.state.busy });
  }

  async handleSubmit(values) {

    // Make a copy of the values object
    //
    let formData = {...values};

    try {

      // Note: 'new' key is a flag to indicate if we are adding a new client 
      // or updating an existing one.
      //
      if (typeof this.props.profile.new === 'undefined') {
        // Must be updating an existing client or the logged in user
        await updateUser(formData.id, formData).then( data => {
          location.reload();
        });
      } else {
        // Must be adding a new client...
        formData.coachId = this.props.coachId;
        formData.status = 'PENDING';

        // Remove keys the API doesn't recognize
        delete formData.new;

        // Call the API
        await addUser(formData).then( (data) => {
          window.location = '/clients';
        });
      }

    } catch (err) {
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'An unknown error occurred.'
      });
    }
  }

  handleCancel() {
    window.location = '/clients';
  }

  handleRoleChange(value) {
    this.setState({role: value});
  }

  renderRole(field) {
    
    return (
      <div>
        <label>Role</label>
        <Dropdown
          auto
          source={[{value: 'CLIENT', label: 'CLIENT'},{value: 'COACH', label: 'COACH'}]}
          value={field.input.value}
          onChange={this.handleRoleChange}
        />
      </div>
    )
    
  }

  render () {

    const { error, handleSubmit, pristine, submitting } = this.props;
    const canUpdateRole = this.props.canUpdateRole || false;

    return (

      <form method='POST' >
        <div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', paddingRight: '10px', width: '130px'}} >
              <Field name='firstname' type='text' label="First Name" required={true} />     
            </div>
            <div style={{float: 'left', paddingRight: '10px', width: '130px'}} >
              <Field name='lastname' type='text' label="Last Name" required={true} />   
            </div>
            <div style={{float: 'left', width:'70px'}} >
              <DropdownField name='role' type='text' label='Role' required={true} disabled={!canUpdateRole}
                source={
                  [
                    { value: 'CLIENT', label: 'Client' },
                    { value: 'COACH', label: 'Coach' },
                  ]
                }
              />
            </div>
          </div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', paddingRight: '15px', width: '150px'}} >
              <Field name='phone' type='tel' label="Phone Number" required={false} 
                normalize={normalizePhone} />   
            </div>
            <div style={{float: 'left', width: '180px'}} >
              <Field name='email' type='email' label="Email" required={true} />   
            </div>
          </div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', paddingRight: '15px', width: '165px'}} >
              <Field name='company' type='text' label="Company" required={false} />     
            </div>
            <div style={{float: 'left', width: '165px'}} >
              <Field name='title' type='text' label="Title" required={false} />     
            </div>
          </div>

          <div style={{clear: 'both', width: '300px'}} >
            <Field name='address' type='text' label="Address" required={false} />     
          </div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', paddingRight: '15px', width: '200px'}} >
              <Field name='city' type='text' label="City" required={false} />     
            </div>
            <div style={{float: 'left', paddingRight: '15px', width: '75px'}} >
              <Field name='state' type='text' label="State" required={false} />     
            </div>
          </div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', paddingRight: '15px', width: '150px'}} >
              <Field name='postalcode' type='text' label="Postal Code" required={false} />     
            </div>
            <div style={{float: 'left', width: '110px'}} >
              <Field name='country' type='text' label="Country" required={false} />     
            </div>
          </div>

          <div style={{clear: 'both', width: '300px'}} >
            <Field name='website' type='text' label="Website" required={false} />     
          </div>

          <div style={{clear: 'both', width: '300px'}} >
            <label className={AppStyles.notes_header}>Notes</label>
            <Field type='text' name="notes" multiline={true} label='' maxLength={255}
              rows={3} placeholder='Add some notes' />
          </div>

        </div>

        {error && <div className={AppStyles.error_msg} style={{marginTop: '10px'}}>{error}</div>}

        <div className={AppStyles.center_div_content} style={{marginTop: '15px', width: '205px', margin: '0 auto'}} >

          <div style={{float: 'left'}} 
            className={(typeof this.props.profile.new === 'undefined') ? AppStyles.hide_me : AppStyles.show_me} >
            <Button type='button' disabled={false} primary={false} raised={true}
              style={{marginRight: '10px'}} onMouseUp={this.handleCancel} >Cancel
            </Button>
          </div>

          <div style={ (typeof this.props.profile.new === 'undefined') ? {clear: 'both'} : {float:'left'} } >
            <Button type="submit"  disabled={pristine || submitting} primary={true} raised={true}
              onMouseUp={handleSubmit(this.handleSubmit.bind(this))} >
              { (typeof this.props.profile.new === 'undefined') ? 'Update' : 'Invite'}
            </Button>
          </div>

        </div>
      </form>

    );
	}
}

const ProfileFormContainer = reduxForm({
  form: 'profile',
  validate
})(ProfileForm);

export default props => {
  const initialValues = props.profile;

  return <ProfileFormContainer {...props} initialValues={initialValues} />;
};
