import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { updateProfile } from './profile-actions';

export const reducer = handleActions({
  [updateProfile]: (state, action) => {

    return {
      ...state,
      user: {
        ...action.payload
      }
    };
  }
},buildMapFromUserArray(window.INITIAL_STATE.authenticatedUser));

export const stateKey = 'user';

export const selectors = {
  getUser: (state) => {
  	return state[stateKey] || null;
  },
  getProfileTemplate: (state) => {
    // User the currently logged in user profile as reference for our new empty profile.
    let template = {};
    Object.keys(state[stateKey]).map( key => template[key] = '' );
    template.new = true;
    template.role = 'CLIENT';
    return template;
  }
};

function buildMapFromUserArray(user) {
	const ret = {};

	if (!user) return ret;
  ret[user.id] = user;
	return ret;

}
