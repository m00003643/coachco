import React from 'react';
import { connect } from 'react-redux';
import ProfileForm from './ProfileForm.jsx';
import { Panel, AppBar, Tabs, Tab} from 'react-toolbox';
import { selectors as profileSelectors } from './profile-state';
import AppStyles from '../common/styles/app.scss';
import SettingsForm from './SettingsForm.jsx';
import Page from '../common/Page.jsx';

class ProfileWithSettingsPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = { 
			tabIndex: 0
	 };

		// Set the appropriate tab based on hash contained in url
		let hash = this.props.location.hash || '#profile';
		this.state.tabIndex = (hash == '#profile') ? 0 : 1;

  }

  handleTabChange(tabIndex) {
    this.setState({ tabIndex });
  }

	render() {

		const { profile } = this.props;
		let settings = profile.settings;

		return (
			<Page >
				<span className={AppStyles.page_title}>My Profile</span>
				<Tabs index={this.state.tabIndex} onChange={this.handleTabChange.bind(this)}>
					<Tab label={(window.innerWidth < 441) ? '' : 'Detail'} icon='person'>
						<ProfileForm profile={profile} />
					</Tab>
					<Tab label={(window.innerWidth < 441) ? '' : 'Settings'} icon='settings'>
						<SettingsForm user={profile} settings={settings} role={profile.role} />
					</Tab>
				</Tabs>
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
		profile: profileSelectors.getUser(state)
	}

};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(ProfileWithSettingsPage);