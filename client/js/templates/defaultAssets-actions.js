import { createAction } from 'redux-actions';

export const updateDefaultAssets = createAction('UPDATE_DEFAULT_ASSETS', assets => assets);
