import { createAction } from 'redux-actions';

export const updateTemplates = createAction('UPDATE_TEMPLATES', assessmentTemplates => assessmentTemplates);
