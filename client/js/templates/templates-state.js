import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { updateTemplates } from './templates-actions';

export const reducer = handleActions({
  [updateTemplates]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  }
},{ 
  'loaded': false, 
  'templates': [] 
});

export const stateKey = 'assessmentTemplates';

export const selectors = {};

selectors.getAssessmentTemplates = state => state[stateKey];

selectors.getTemplates = state => state[stateKey].templates;

selectors.getTemplate = (state, templateId) => {

  let ret = null;

  state[stateKey].templates.map( template => {
    if (template.id == templateId) {
      ret = template;
      return;
    }
  });
  return ret;
};

