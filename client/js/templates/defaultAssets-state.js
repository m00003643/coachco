import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { updateDefaultAssets } from './defaultAssets-actions';

export const reducer = handleActions({
  [updateDefaultAssets]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  }
},{ 
  'loaded': false, 
  'assets': [] 
});

export const stateKey = 'defaultAssets';

export const selectors = {};

selectors.getDefaultAssetsObj = state => state[stateKey];

selectors.getDefaultAssets = state => state[stateKey].assets;

selectors.getAsset = (state, assetId) => {
  state[stateKey].assets.map( asset => {
    if (asset.id === assetId) return asset;
  });
  return null;
};