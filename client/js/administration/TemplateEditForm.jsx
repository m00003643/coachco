import React from 'react';
import { getFormValues, isInvalid, reduxForm, SubmissionError } from 'redux-form';
import { Button } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import Field from '../common/Field.jsx';
import { updateAssessmentTemplate, addAssessmentTemplate } from '../utils/api';

class TemplateEditForm extends React.Component {

  constructor(props) {

    super(props);

    this.handleCancel = this.handleCancel.bind(this);

  }

  handleCancel() {
    window.location = '/admin/assessment-templates';
  }

  formatTemplateReponseJson(values) {

    let ret = {}
    let template = {};
    let prompts = []
    const coachId = window.INITIAL_STATE.authenticatedUser.id;
    const action = this.props.action || null;

    template['type'] = values.type;

    values.prompts.split('\n').map( prompt => {
      prompts.push( { 'value': 0, 'prompt': prompt} );
    });
    template['prompts'] = prompts;

    template['directions'] = values.directions;
    template['dateAssigned'] = null;
    template['dateCompleted'] = null;
    template['feedback_text'] = values.feedback_text;
    template['feedback_title'] = values.feedback_title;
    template['directions_title'] = values.directions_title;
    template['mentorReviewable'] = 'N';

    console.log(values);
    console.log(template);
    if (action != null) {
      ret['coachId'] = coachId;
    }
    ret['assessmentTemplate'] = template;
    return ret;
  }

  async handleSubmit(values) {

    const id = this.props.assessmentId;
    const action = this.props.action || null;

    const buff = {...values};
    const apiValues = this.formatTemplateReponseJson(buff);

    try {
      if (action == null) {
        await updateAssessmentTemplate(id, apiValues).then(window.location = '/admin/assessment-templates');
      } else {
        await addAssessmentTemplate(apiValues).then(window.location = '/admin/assessment-templates');
      }
    } catch (err) {
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'There was an unknown error while attempting to save the form.'
      });
    }
  }

  render() {

    const { invalid, error, handleSubmit, pristine, submitting } = this.props;
    const action = this.props.action || null;

    return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>

        <div>

          <span className={AppStyles.page_title}>{ (action == 'add') ? 'Create Template' : 'Edit Template' }</span>


            <Field type='text' name="type" label='Assessment Type / Name' />

            <Field type='text' name="directions_title" label='Directions Title' />

            <Field type='text' name="directions" multiline={true} label='Directions' maxLength={400}
              rows={2} />

            <Field type='text' name="feedback_title" label='Feedback Title' />

            <Field type='text' name="feedback_text" label='Feedback Default Text' />
            
            <Field type='text' name="prompts" multiline={true} label='Prompts (oner per line)' maxLength={1000}
              rows={6} />

          {error && <div className={AppStyles.error_msg} style={{marginTop: '10px'}}>{error}</div>}

          <div style={{clear:'both'}} >
            <div className={AppStyles.action_button}>
              <Button label='Cancel' onClick={this.handleCancel} raised={true} primary={true}
                style={{marginRight: '15px'}} />
              <Button type='submit' label={ (action == 'add') ? 'Create' : 'Update' } raised={true} primary={true}
                disabled={submitting || pristine} />
            </div>        
          </div>

        </div>

      </form>
    )
  }
}

const TemplateEditFormContainer = reduxForm({
  form: 'templateEditForm'
})(TemplateEditForm);

export default props => {

  // Translate template.prompts array into a string representation that 
  // multi-line Field component understands
  let prompts = props.template.prompts.reduce( (buff, prompt) => {
    return buff + prompt.prompt + '\n';
  }, '');
  prompts = prompts.slice(0,-1); // Remove trailing newline character
  props.template.prompts = prompts;

  return <TemplateEditFormContainer {...props} initialValues={props.template} />;
};
