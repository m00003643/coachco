import React from 'react';
import { connect } from 'react-redux';
import { Panel, AppBar, Button, Dialog} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import TemplateEditForm from './TemplateEditForm.jsx';
import { selectors as templateSelectors } from '../templates/templates-state';
import { getCoachAssessmentTemplates } from '../utils/api';
import { updateTemplates } from '../templates/templates-actions';

class TemplateEditPage extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      showMessage: false,
      message: ''
    }

    this.handleMsgToggle = this.handleMsgToggle.bind(this);

  }

  handleMsgToggle() {
    this.setState({showMessage: !this.state.showMessage});
  }

  componentDidMount() {

    try {
   		const coachId = window.INITIAL_STATE.authenticatedUser.id;
      getCoachAssessmentTemplates(coachId)
        .then( data => {
          // Dispatch the updateTemplates action...
          this.props.dispatch(
            updateTemplates( {loaded: true, templates: data.result } )
          );
        }
      );
    } catch (err) {
      this.setState( { message: 'Unable to fetch assessment templates' } );
      this.handleMsgToggle();
    }
  }

	render() {

    const { template } = this.props;
    
    let assessmentTemplate = null;
    let assessmentId = null;
    if (template != null) {
      assessmentTemplate = template.assessmentTemplate;
      assessmentId = template.id;
    }

		return (
			<Page>
        {assessmentTemplate && <TemplateEditForm template={assessmentTemplate} assessmentId={assessmentId}/> }

        <div>
          <Dialog 
            active={this.state.showMessage}
            onEscKeyDown={this.handleMsgToggle}
            onOverlayClick={this.handleMsgToggle}
            title='Error' 
            actions={[
              { label: "Ok", onClick: this.handleMsgToggle }
            ]} >

            <div>
              {this.state.message}
            </div>

          </Dialog>
        </div>
	    </Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
    template: templateSelectors.getTemplate(state, ownProps.params.id)
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(TemplateEditPage);