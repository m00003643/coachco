import React from 'react';
import { connect } from 'react-redux';
import { Panel, AppBar, Button, Dialog, Dropdown, Input} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import AssessmentTemplatesList from './AssessmentTemplatesList.jsx';
import { getCoachAssessmentTemplates, addAssessmentTemplate } from '../utils/api';
import { selectors as templateSelectors } from '../templates/templates-state';
import { selectors as userSelectors } from '../user/user-state';
import { updateTemplates } from '../templates/templates-actions';

class AssessmentTemplatesPage extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      clone: false,
      choices: [],
      clone_name: '',
      template_name: '',
      message: ''
    }

    this.handleCreateTemplate = this.handleCreateTemplate.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleCloneClick = this.handleCloneClick.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleTemplateChange = this.handleTemplateChange.bind(this);

  }

  async handleCloneClick() {

    // Test for valid input
    if (this.state.template_name === '' || this.state.clone_name === '') {
      this.setState( { message: 'Missing required input'} );
      return;
    }

    // Get template clone
    let clone = {};
    this.props.assessmentTemplates.templates.map( template => {
      if (template.assessmentTemplate.type === this.state.template_name) {
        clone = {...template};
        return;
      }
    });

    // Prepare clone
    delete clone.id;
    clone.assessmentTemplate.type = this.state.clone_name;

    // Upload clone to assessment templates
    try {
      await addAssessmentTemplate(clone).then(window.location = '/admin/assessment-templates');
    } catch (err) {
      this.setState( { message: 'Unable to create clone'} );
    }

    // Turn off clone dialog
    this.setState({clone: !this.state.clone, message: ''});

  }

  handleToggle() {
    this.setState({clone: !this.state.clone});
  }

  handleNameChange(value) {
    this.setState({clone_name: value});
  }

  handleTemplateChange(value) {
    this.setState({template_name: value});
  }

  componentDidMount() {

    // If the assessement templates for this coach are already in state then nothing to do here
    if (this.props.assessmentTemplates.loaded === true) return;

    const coachId = window.INITIAL_STATE.authenticatedUser.id;
    getCoachAssessmentTemplates(coachId).then(data => {

        // Create choice list for clone operation
        let choices = [];
        data.result.map( template => {
          let name = template.assessmentTemplate.type;
          choices.push( { value: name, label: name} );
        });
        this.setState( { choices : choices });

        // Dispatch the updateTemplates action...
        this.props.dispatch( updateTemplates( {loaded: true, templates: data.result } ));
      },
      err => {
        console.log(err);
        //this.props.dispatch(
        //  displayUserMessage({ type: 'error', msg: 'Error loadin templates...' })
        //)
      }
    );
  }

  handleCreateTemplate() {
    window.location = '/admin/assessment-templates/add';
  }

	render() {

    const { assessmentTemplates } = this.props;
    const templates = assessmentTemplates.templates;

		return (
			<Page >
        <span className={AppStyles.page_title} >
          <Button icon='arrow_back' href='/admin' />
          Assessment Templates
        </span>
        <p>Select a template to edit or else, select &#39;Create&#39; a new one, 
          &#39;Clone&#39; an existing one, or &#39;Delete&#39; one.</p>
        <AssessmentTemplatesList templates={templates} />

        <div style={{clear:'both'}} >
          <div className={AppStyles.action_button}>
            <Button icon='description' label='Create' flat primary 
              onMouseUp={this.handleCreateTemplate} />
            <Button icon='content_copy' label='Clone' flat primary 
              onMouseUp={this.handleToggle} />
          </div>        
        </div>

        <div>
          <Dialog 
            active={this.state.clone}
            onEscKeyDown={this.handleToggle}
            onOverlayClick={this.handleToggle}
            title='Clone Template' 
            actions={[
              { label: "Ok", onClick: this.handleCloneClick },
              { label: "Cancel", onClick: this.handleToggle }
            ]} >

            <div>
              <Dropdown name='choices' type='text' label='Templates' required={true}
                source={this.state.choices} 
                onChange={this.handleTemplateChange} 
                value={this.state.template_name} />
              <Input name="clone_name" type='text' label="Clone Name" required 
                onChange={this.handleNameChange} 
                value={this.state.clone_name} />
            </div>

            <p style={{color:'red'}}>{this.state.message}</p>

          </Dialog>
        </div>
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
    assessmentTemplates: templateSelectors.getAssessmentTemplates(state)
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(AssessmentTemplatesPage);