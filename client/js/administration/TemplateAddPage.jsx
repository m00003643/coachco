import React from 'react';
import Page from '../common/Page.jsx';
import TemplateEditForm from './TemplateEditForm.jsx';
import AppStyles from '../common/styles/app.scss';

class TemplateAddPage extends React.Component {

  constructor(props) {

    super(props);

  }

	componentWillMount() {

		const { clientId, coachId } = this.props;

		this.initialTemplate = {
			type: '',
      prompts: [],
      directions: '',
      feedback_text: '',
      feedback_title: '',
      directions_title: ''
		};

	}

	render() {

		return (
			<Page>
        <TemplateEditForm template={this.initialTemplate} assessmentId={-1} action='add' /> 
	    </Page>
		);
	}

}

export default TemplateAddPage;