import React from 'react';
import { Button, Dialog, Input, Tabs, Tab, Dropdown} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { ResetBrandingEmail } from '../utils/api';
import Dropzone from 'react-dropzone';
import { updateUser } from '../utils/api';
import QuillPage from './QuillPage.jsx';

class BrandingForm extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			active: false,
      activeProfile: false,
      selectedFile: '',
      shared: false,
      showMessage: false,
      message: '',
      fileErrorMsg: '',
			companyLogo: '',
			companyName: '',
      companyTagline: '',
      profileImage: '',
			emailTemplate: '',
      index: 0,
      profileChanged: false,
      messageChanged: false,
      emailBeforeChange: '',
      emailTemplates: [
        { value: 'welcomeEmail', label: 'Welcome Email' },
        { value: 'assessmentNotificationEmail', label: 'Assessment Assigned Email' }
      ],
      curEmailTemplateKey: 'welcomeEmail',
      templateMessage: 'Template used for welcoming new clients'
		}

		// Set the appropriate tab based on hash contained in url
		let hash = this.props.location.hash || '#profile';
		this.state.index = (hash == '#profile') ? 0 :
			(hash == '#logo') ? 1 :
			(hash == '#message1') ? 2 :
			(hash == '#message2') ? 2 : 0;

    switch(hash) {
      case '#message1':
        this.state.emailTemplate = this.props.brand.welcomeEmail.join('');
        this.state.emailBeforeChange = this.props.brand.welcomeEmail;
        this.state.templateMessage = 'Template used for welcoming new clients';
        this.state.curEmailTemplateKey = 'welcomeEmail';
        break;
      case '#message2':
        this.state.emailTemplate = this.props.brand.assessmentNotificationEmail.join('');
        this.state.emailBeforeChange = this.props.brand.assessmentNotificationEmail;
        this.state.templateMessage = 'Template used when notifying clients of an assigned assessment';
        this.state.curEmailTemplateKey = 'assessmentNotificationEmail';
        break;
      default:
        this.state.emailTemplate = this.props.brand.welcomeEmail.join('');
        this.state.emailBeforeChange = this.props.brand.welcomeEmail;
        this.state.templateMessage = 'Template used for welcoming new clients';
        this.state.curEmailTemplateKey = 'welcomeEmail';
        break;
    }

    this.onDrop = this.onDrop.bind(this);
    this.onDropProfile = this.onDropProfile.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleToggleProfile = this.handleToggleProfile.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleYesProfile = this.handleYesProfile.bind(this);
    this.toggleShared = this.toggleShared.bind(this);
    this.handleMsgToggle = this.handleMsgToggle.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleUpdates = this.handleUpdates.bind(this);
    this.handleSetDefaultEmail = this.handleSetDefaultEmail.bind(this);

    this.modules = {
      toolbar: [
        [{ 'header': [1, 2, false] }],
        ['bold', 'italic', 'underline'],
        ['align'],
        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
        ['link', 'image', {color: [] } ]
      ]
    }

    // Quill default formats
    this.formats = [
      'header', 'bold', 'italic', 'underline', 'align', 'list', 'bullet', 'indent', 'link', 'image',
      'color'
    ]
		
	}

  componentDidMount() {

		const emailTemplate = this.props.brand.welcomeEmail.join('');

    this.setState(
      {
        companyLogo: this.props.brand.companyLogo || '',
        companyName: this.props.brand.companyName,
        companyTagline: this.props.brand.companyTagline,
        profileImage: this.props.brand.profileImage || '',
        message: '',
        messageChanged: false
      }
    )

  }

  onDrop(files) {

    const fileWhiteList = ['jpg','tiff','png','bmp'];

    // Check for allowed file type
    let filePrefix = files[0].name.split('.').pop(-1);
    if (fileWhiteList.indexOf(filePrefix) < 0) {
      this.setState( {fileErrorMsg: 'The type of file chosen is not allowed'} );
      return; // not a valid file type
    }

    // Check for allowed file size
    const fileSize = +files[0].size;
    const maxSize = 1000000;
    const strMaxSize = '1 MB';
    if ( fileSize > maxSize ) {
      this.setState( {fileErrorMsg: 'File size exceeds the maximum allowed of ' + strMaxSize} );
      return; // not a valid file type
    }

    this.setState( {fileErrorMsg: '', selectedFile: files[0]} );
    this.handleToggle();
  }

  onDropProfile(files) {

    const fileWhiteList = ['jpg','tiff','png','bmp'];

    // Check for allowed file type
    let filePrefix = files[0].name.split('.').pop(-1);
    if (fileWhiteList.indexOf(filePrefix) < 0) {
      this.setState( {fileErrorMsg: 'The type of file chosen is not allowed'} );
      return; // not a valid file type
    }

    // Check for allowed file size
    const fileSize = +files[0].size;
    const maxSize = 1000000;
    const strMaxSize = '1 MB';
    if ( fileSize > maxSize ) {
      this.setState( {fileErrorMsg: 'File size exceeds the maximum allowed of ' + strMaxSize} );
      return; // not a valid file type
    }

    this.setState( {fileErrorMsg: '', selectedFile: files[0]} );
    this.handleToggleProfile();
  }

  handleTemplateChange(value) {

    let emailTemplate = '';
    let templateMessage = '';
    let emailBeforeChange = '';

    switch(value) {
      case 'welcomeEmail':
        emailTemplate = this.props.brand.welcomeEmail.join('');
        emailBeforeChange = this.props.brand.welcomeEmail;
        templateMessage = 'Template used for welcoming new clients';
        break;
      case 'assessmentNotificationEmail':
        emailTemplate = this.props.brand.assessmentNotificationEmail.join('');
        emailBeforeChange = this.props.brand.assessmentNotificationEmail;
        templateMessage = 'Template used when notifying clients of an assigned assessment';
        break;
    }

    this.setState( {
      curEmailTemplateKey: value,
      emailTemplate: emailTemplate,
      emailBeforeChange: emailBeforeChange,
      templateMessage: templateMessage,
      messageChanged: false
      } );
  }

  handleTabChange(index) {
    this.setState({ index, message: '' });
  }

  handleToggle() {
    this.setState({active: !this.state.active});
  }

  handleToggleProfile() {
    this.setState({activeProfile: !this.state.activeProfile});
  }

  handleMsgToggle() {
    this.setState({showMessage: !this.state.showMessage});
  }

  toggleShared() {
    this.setState({shared: !this.state.shared});
  }

  handleProfileChange(name, value) {
    let change = 
      (value != this.props.brand.companyName) ||
      (value != this.props.brand.companyTagline);
    this.setState( {[name]: value, profileChanged: change} );
  };

  handleEmailChange(value) {
    let change = value != this.state.emailBeforeChange.join('');
    this.setState( {emailTemplate: value, messageChanged: change} );
  };

  async handleSetDefaultEmail() {

    try {
      await ResetBrandingEmail(this.props.user.id, this.state.curEmailTemplateKey)
      .then( data => {

        switch(this.state.curEmailTemplateKey) {
          case 'welcomeEmail':
            location.hash = '#message1';
            break;
          case 'assessmentNotificationEmail':
            location.hash = '#message2';
            break;
          default: 'welcomeEmail';
        }
        
        location.reload(true);
      })
    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable to set default email' } );
    }

  }

  async handleUpdates() {
    let brand = {};
    brand.branding = this.props.brand;
    brand.branding.companyName = this.state.companyName;
    brand.branding.companyTagline = this.state.companyTagline;
    brand.branding.companyLogo = this.state.companyLogo;
    brand.branding.profileImage = this.state.profileImage;

    let change2 = this.state.emailTemplate.replace(/\t/g, '&nbsp;&nbsp;');

    switch(this.state.curEmailTemplateKey) {
      case 'welcomeEmail':
        brand.branding.welcomeEmail = [change2];
        break;
      case 'assessmentNotificationEmail':
        brand.branding.assessmentNotificationEmail = [change2];
        break;
    }
    

    try {
      await updateUser(this.props.user.id, brand)
      .then( data => {
        let hash = (this.state.index == 0) ? '#profile' :
        (this.state.index == 1) ? '#logo' :
        (this.state.index == 2) ? '#message' : '#profile';

        if (hash == '#message') {
          switch(this.state.curEmailTemplateKey) {
            case 'welcomeEmail':
              hash = '#message1';
              break;
            case 'assessmentNotificationEmail':
              hash = '#message2';
              break;
          }
        }

        location.hash = hash;
        location.reload();
      });

    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable to update branding info' } );
    }
    
  }

	handleYes() {

    // Turn off yes/no dialog
    this.setState({active: !this.state.active});

    // Verify we have all needed input
		if (this.state.selectedFile == '') {
      this.setState( { message: 'Required input is missing' } );
      this.handleMsgToggle();
      return;
    }

    // uploadFile will be called when we have received a valid signed request
    const uploadFile = (file, signedRequest, url) => {

      const xhr = new XMLHttpRequest();

      // Create request event handlers
      xhr.onreadystatechange = () => {

        if(xhr.readyState === 4){
          if(xhr.status === 200){
            // The file has been uploaded. Now update the database with asset meta data
            this.setState( {companyLogo: url} );
            this.handleUpdates();

          } else {
            // Handle file upload error
            this.setState( { message: 'Unable to complete upload: failed to send document' } );
            this.handleMsgToggle();
          }
        }
      };

      xhr.open('PUT', signedRequest);
      xhr.send(file);
    } // End uploadFile
    
    // Main function section - Make api call to get signed request url
    const file = this.state.selectedFile;
    const fileNameEncoded = encodeURIComponent(file.name);
    const fileTypeEncoded = encodeURIComponent(file.type);
    const url = `/api/sign-s3/${fileNameEncoded}/${fileTypeEncoded}`;
    const xhr = new XMLHttpRequest();

    // Create Request event handlers
    xhr.onreadystatechange = () => {

      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          // We have the signed request - now upload the file
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          // Handle get signed request error
          this.setState( {message: 'Unable to complete upload: failed to get signed signature'});
          this.handleMsgToggle();
        }
      }
    };

    // Call the api to get the signed request
    xhr.open('GET', url);
    xhr.send();
	}

	handleYesProfile() {

    // Turn off yes/no dialog
    this.setState({activeProfile: !this.state.activeProfile});

    // Verify we have all needed input
		if (this.state.selectedFile == '') {
      this.setState( { message: 'Required input is missing' } );
      this.handleMsgToggle();
      return;
    }

    // uploadFile will be called when we have received a valid signed request
    const uploadFile = (file, signedRequest, url) => {

      const xhr = new XMLHttpRequest();

      // Create request event handlers
      xhr.onreadystatechange = () => {

        if(xhr.readyState === 4){
          if(xhr.status === 200){
            // The file has been uploaded. Now update the database with asset meta data
            this.setState( {profileImage: url} );
            this.handleUpdates();

          } else {
            // Handle file upload error
            this.setState( { message: 'Unable to complete upload: failed to send document' } );
            this.handleMsgToggle();
          }
        }
      };

      xhr.open('PUT', signedRequest);
      xhr.send(file);
    } // End uploadFile
    
    // Main function section - Make api call to get signed request url
    const file = this.state.selectedFile;
    const fileNameEncoded = encodeURIComponent(file.name);
    const fileTypeEncoded = encodeURIComponent(file.type);
    const url = `/api/sign-s3/${fileNameEncoded}/${fileTypeEncoded}`;
    const xhr = new XMLHttpRequest();

    // Create Request event handlers
    xhr.onreadystatechange = () => {

      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          // We have the signed request - now upload the file
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          // Handle get signed request error
          this.setState( {message: 'Unable to complete upload: failed to get signed signature'});
          this.handleMsgToggle();
        }
      }
    };

    // Call the api to get the signed request
    xhr.open('GET', url);
    xhr.send();
	}
	
	render() {

		return (

      <div>
        <span className={AppStyles.page_title} >
          <Button icon='arrow_back' href='/admin' />
          Branding
        </span>

        <Tabs index={this.state.index} onChange={this.handleTabChange.bind(this)}>

          <Tab label='Profile'>
            <p>Enter your company profile information here</p>
            <br />
            <div >
              <Input type='text' label='Company Name' name='companyName' 
                value={this.state.companyName}
                onChange={this.handleProfileChange.bind(this, 'companyName')} maxLength={50} />
            </div>

            <div >
              <Input type='text' label='Company Tagline' name='companyTagline' 
                value={this.state.companyTagline}
                onChange={this.handleProfileChange.bind(this, 'companyTagline')} maxLength={80} />
            </div>

            <p style={{color:'red', textAlign:'center'}}>{this.state.message}</p>

            <div style={{textAlign:'center',marginTop:'10px'}} >
              <Button label='Update' onClick={this.handleUpdates} raised={true} 
                primary={true} disabled={!this.state.profileChanged}/>
            </div>

            <div>
              <br />
              <p style={{textAlign:'center'}}>Click the image below to set profile picture.</p>

              <div style={{marginTop: '10px'}} >
                <Dropzone className={AppStyles.drop_zone} onDrop={this.onDropProfile} multiple={false} 
                  style={{height:'auto'}} >

                  <div>
                    <img src={this.state.profileImage} style={{width:'auto',height:'80px'}} />
                  </div>
                </Dropzone>
              </div>
            </div>

          </Tab>

          <Tab label='Logo'>

            <p>Upload your brand logo here. <br />Use the drop zone below (if present) or click on an existing logo to replace it.</p>

            <div style={{marginTop: '40px'}} >
              <Dropzone className={AppStyles.drop_zone} onDrop={this.onDrop} multiple={false} 
                style={{height:'auto'}} >

                { 
                  ( this.state.companyLogo == '' ) ?
                    <div className={AppStyles.drop_zone_inner} >
                      Select Logo file. Drop an image file here 
                      (or click to select one from your computer). 
                    </div> :
                    <div>
                      <img src={this.state.companyLogo} />
                    </div>
                }
              </Dropzone>
            </div>

            <p style={{color:'red', textAlign:'center'}}>{this.state.message}</p>

          </Tab>

          <Tab label='Message'>

            <Dropdown
              onChange={this.handleTemplateChange.bind(this)}
              source={this.state.emailTemplates}
              value={this.state.curEmailTemplateKey}
              label='Choose a template to edit'
            />
              
            <div>
              <QuillPage emailTemplate={this.state.emailTemplate} 
                handleEmailChange={this.handleEmailChange}
                handleTemplateChange={this.handleTemplateChange} 
                message={this.state.templateMessage}/>
            </div>

            <p style={{color:'red', textAlign:'center'}}>{this.state.message}</p>

            <div style={{textAlign:'center',marginTop:'30px'}} >
              <Button label='Use Default' onClick={this.handleSetDefaultEmail} raised={true} 
                primary={true} style={{marginRight:'10px'}} />
              <Button label='Update' onClick={this.handleUpdates} raised={true} 
                primary={true} disabled={!this.state.messageChanged}/>
            </div>
            
          </Tab>

        </Tabs>

        <div>
          <Dialog 
            active={this.state.active}
            onEscKeyDown={this.handleToggle}
            onOverlayClick={this.handleToggle}
            title='Confirm Upload' 
            actions={[
              { label: "No", onClick: this.handleToggle },
              { label: "Yes", onClick: this.handleYes }
            ]} >

            <div>
              Proceed with uploading logo ({this.state.selectedFile.name})?
            </div>

          </Dialog>
        </div>

        <div>
          <Dialog 
            active={this.state.activeProfile}
            onEscKeyDown={this.handleToggleProfile}
            onOverlayClick={this.handleToggleProfile}
            title='Confirm Upload' 
            actions={[
              { label: "No", onClick: this.handleToggleProfile },
              { label: "Yes", onClick: this.handleYesProfile }
            ]} >

            <div>
              Proceed with uploading profile picture ({this.state.selectedFile.name})?
            </div>

          </Dialog>
        </div>
        
        <div style={{textAlign:'center',color:'red'}} >
          <p>{this.state.fileErrorMsg}</p>
        </div>

        <div>
          <Dialog 
            active={this.state.showMessage}
            onEscKeyDown={this.handleMsgToggle}
            onOverlayClick={this.handleMsgToggle}
            title='Upload Error' 
            actions={[
              { label: "Ok", onClick: this.handleMsgToggle }
            ]} >

            <div>
              {this.state.message}
            </div>

          </Dialog>
        </div>
      </div>
		);
	}

}

export default BrandingForm;

