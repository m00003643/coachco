import React from 'react';
import { Button } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';

class AdminPage extends React.Component {

	render() {

		return (
			<Page >
				<span className={AppStyles.page_title}>Administration Tasks</span>

				<div style={{clear: 'both'}} >
					<ul style={{marginTop: '20px',listStyleType: 'none'}} >
						<li>
							<Button label='Branding' icon='account_circle' 
								href='/admin/branding' raised={true} flat primary />
						</li>
						<li>
							<Button label='Assessment Templates' icon='description' 
								href='/admin/assessment-templates' raised={true} flat primary />
						</li>
						<li>
							<Button label='Shareable Documents' icon='attachment' 
								href='/admin/documents' raised={true} flat primary />
						</li>
					</ul>
				</div>
	    </Page >
		);
	}

}

export default AdminPage;
