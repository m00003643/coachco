import React from 'react';
import { connect } from 'react-redux';
import Page from '../common/Page.jsx';
import { selectors as userSelectors } from '../user/user-state';
import { selectors as brandSelectors } from '../administration/branding-state';
import BrandingForm from './BrandingForm.jsx';

class BrandingPage extends React.Component {

	render() {

		return (
			<Page >
				<BrandingForm user={this.props.user} 
					brand={this.props.brand} location={this.props.location}/>
			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
    user: userSelectors.getUser(state),
    brand: brandSelectors.getBranding(state)
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(BrandingPage);