import React from 'react';
import { Button, List, ListItem, Dialog } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import { deleteAsset } from '../utils/api';
import closest from 'dom-closest';

class AssetsList extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			active: false,
			selectedId: -1
		}

    this.handleDeleteAsset = this.handleDeleteAsset.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleClick = this.handleClick.bind(this);

	}

  handleClick(e) {
    const el = closest(e.target, '.item_click');
    if (el != null) window.open(el.getAttribute('data-payload'), '_blank');
  }

	handleDeleteAsset(e) {
		const assetId = e.target.getAttribute('data-assetid');
    this.setState({active: !this.state.active, selectedId: assetId});
	}

	handleToggle() {
    this.setState({active: !this.state.active, assetId: -1});
	}

	handleYes() {

    this.setState({active: !this.state.active});
		if (this.state.selectedId < 0) return;

    deleteAsset(this.state.selectedId).then(data => {
				window.location = '/admin/documents';
        // Re-route to assets page
      },
      err => {
        console.log(err);
				// Dispatch an error message...
      }
    );
	}

	render() {

    const { assets } = this.props;

    return (
      <div>
				<List selectable ripple>
					{assets.map( (asset) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={asset.id} 
              onClick={this.handleClick} data-payload={asset.assetUrl}>
							<ListItem
								key={asset.id}
								caption={asset.assetName}
								leftIcon='attachment'
								rightActions={[
                  <div key={asset.id} onClick={e => e.stopPropagation()}>
										<Button label='Delete' name={`delAsset${asset.id}`} icon='cancel' 
											onMouseUp={this.handleDeleteAsset} key={asset.id} data-assetid={asset.id}/>
									</div>
								]}
							/>
						</div>
					))}
				</List>

				<div>
					<Dialog 
						active={this.state.active}
						onEscKeyDown={this.handleToggle}
						onOverlayClick={this.handleToggle}
						title='Confirm Delete' 
						actions={[
							{ label: "No", onClick: this.handleToggle },
							{ label: "Yes", onClick: this.handleYes }
						]} >

						<div>
							Are you sure you want to remove this from your list of available documents?
						</div>

					</Dialog>
				</div>
				
      </div>
    )
  }

}

export default AssetsList;