import React from 'react';
import { connect } from 'react-redux';
import { Panel, AppBar, Button} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import AssetsList from './AssetsList.jsx';
import { selectors as defaultAssetSelectors } from '../templates/defaultAssets-state';
import { updateDefaultAssets } from '../templates/defaultAssets-actions';
import { getCoachDefaultAssets } from '../utils/api';
import { selectors as userSelectors } from '../user/user-state';

class AssetsPage extends React.Component {

	constructor(props) {
		super(props);
	}

  componentDidMount() {

    try {
			const coachId = window.INITIAL_STATE.authenticatedUser.id;
      getCoachDefaultAssets(coachId).then( 
        data => {
          // Dispatch the updateDefaultAssets action...
          this.props.dispatch(
            updateDefaultAssets( {loaded: true, assets: data.result } )
          );
        }
      );
    } catch (err) {
      if (err.validationErrors) {
        console.log(err.validationErrors);
        //throw new SubmissionError(err.validationErrors);
      }

      console.log('Some unknow error occurred: ');
			console.log(err);
      //throw new SubmissionError({
      //  _error: err.errorMessage || 'There was an unknown error while attempting to save form.'
      //});
    }
  }
	
	render() {

		const { defaultAssets, coachId } = this.props;
		const assets = defaultAssets.assets;

		return (
			<Page >
        <span className={AppStyles.page_title} >
          <Button icon='arrow_back' href='/admin' />
          Documents
          <AssetsList assets={assets} />
        </span>

        <div style={{clear:'both'}} >
          <div className={AppStyles.action_button}>
            <Button icon='attachment' label='Upload' href={`/admin/documents/upload/${coachId}`} flat primary />
          </div>        
        </div>
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
    defaultAssets: defaultAssetSelectors.getDefaultAssetsObj(state),
		coachId: userSelectors.getUser(state).id
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(AssetsPage);