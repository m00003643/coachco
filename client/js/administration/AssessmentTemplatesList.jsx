import React from 'react';
import { getFormValues, isInvalid, reduxForm, SubmissionError } from 'redux-form';
import { Button, List, ListItem, Dialog } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import { deleteAssessmentTemplate } from '../utils/api';
import closest from 'dom-closest';

class AssessmentTemplateList extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			active: false,
			selectedId: -1
		}

    this.handleDeleteTemplate = this.handleDeleteTemplate.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleClick = this.handleClick.bind(this);

	}

	handleDeleteTemplate(e) {
		const templateId = e.target.getAttribute('data-templateid');
    this.setState({active: !this.state.active, selectedId: templateId});
	}

	handleClick(e) {
    const el = closest(e.target, '.item_click');
		const templateId = el.getAttribute('data-payload');
		window.location = '/admin/assessment-templates/' + templateId;
	}

	handleToggle() {
    this.setState({active: !this.state.active, assetId: -1});
	}

	handleYes() {

    this.setState({active: !this.state.active});
		if (this.state.selectedId < 0) return;

    deleteAssessmentTemplate(this.state.selectedId).then(data => {
				window.location = '/admin/assessment-templates';
        // Re-route to templates page
      },
      err => {
        console.log(err);
				// Dispatch an error message...
      }
    );
	}

  render() {

    const { templates } = this.props;

    return (
      <div>
				<List selectable ripple>
					{templates.map( (template) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={template.id} 
              onClick={this.handleClick} data-payload={template.id}>
							<ListItem
								key={template.id}
								caption={template.assessmentTemplate.type}
								onClick={this.handleClick}
								rightActions={[
									<div key={template.id} onClick={e => e.stopPropagation()} >
										<Button label='Delete' name={`delTemplate${template.id}`} icon='cancel' 
											onMouseUp={this.handleDeleteTemplate} key={template.id} data-templateid={template.id}/>
									</div>
								]}
							/>
						</div>
					))}
				</List>

				<div>
					<Dialog 
						active={this.state.active}
						onEscKeyDown={this.handleToggle}
						onOverlayClick={this.handleToggle}
						title='Confirm Delete' 
						actions={[
							{ label: "No", onClick: this.handleToggle },
							{ label: "Yes", onClick: this.handleYes }
						]} >

						<div>
							Are you sure you want to remove this from your list of available templates?
						</div>

					</Dialog>
				</div>
				
      </div>
    )
  }
}

export default AssessmentTemplateList;