import React from 'react';
import ReactQuill, { Quill } from 'react-quill';
import {Dropdown, Dialog} from 'react-toolbox';
import 'react-quill/dist/quill.core.css';
import 'react-quill/dist/quill.snow.css';

class QuillPage extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      textSize: 'normal',
      showTokens: false,
      cursorPosition: 0
    }

    this.handleTokenChange = this.handleTokenChange.bind(this);
    this.handleTextSizeChange = this.handleTextSizeChange.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.requestToken = this.requestToken.bind(this);

    this.modules = {
      toolbar: '#toolbar'
    }

  }

  handleToggle(event) {
    this.setState({showTokens: !this.state.showTokens});
  }

  handleTextSizeChange(event) {
    this.setState( {textSize: event.target.value} );
  }

  handleTokenChange(event) {
    let editor = this.quill.getEditor();
    console.log(this.state.cursorPosition);
    //if (editor.getSelection() == null) return;
    //const cursorPosition = editor.getSelection().index
    //editor.insertText(cursorPosition, event.target.value);
    editor.insertText(this.state.cursorPosition, event.target.value);
    editor.onChange;
    this.setState({showTokens: !this.state.showTokens});
  }

  handleKeyDown(event) {
    return;
    let x = event.which || event.keyCode;
    let tabCode = '&nbsp;&nbsp;&nbsp;&nbsp;';
    if (x == 9) {
      let editor = this.quill.getEditor();
      if (editor.getSelection() == null) return;
      const cursorPosition = editor.getSelection().index
      editor.insertText(cursorPosition, tabCode);
      event.preventDefault();
    }
  }

  requestToken() {
    let editor = this.quill.getEditor();
    if (editor.getSelection() == null) return;
    const cursorPosition = editor.getSelection().index
    this.setState({cursorPosition: cursorPosition});
    this.handleToggle();
  }

  render() {
    return (
      <div>

        <div>
          <p>{this.props.message}</p>
          <p>Use <strong>Token</strong> to insert brand specific placeholders in the email template.</p>
          <br />
        </div>
      
        <div id="toolbar">

          <span className="ql-formats">
            <select className="ql-size" value={this.state.textSize} 
              onChange={this.handleTextSizeChange} >
              <option value="small"></option>
              <option value="normal"></option>
              <option value="large"></option>
            </select>
          </span>

          <span className="ql-formats">
            <button className="ql-bold"></button>
            <button className="ql-italic"></button>
            <button className="ql-underline"></button>
          </span>

          <span className="ql-formats">
            <button className="ql-list" value="ordered"></button>
            <button className="ql-list" value="bullet"></button>
            <button className="ql-indent" value="-1"></button>
            <button className="ql-indent" value="+1"></button> 
            <select className="ql-align"></select>

          </span>

          <span className="ql-formats">
            <select className="ql-color"></select>
            <select className="ql-background"></select>
          </span>

          <span className="ql-formats">
            <button className="ql-link"></button>
            <button className="ql-image"></button>
          </span>

          <span className="ql-formats">
            <button onClick={this.requestToken} >Token</button>
          </span>

        </div>  

        <ReactQuill 
          theme='snow'
          onChange={this.props.handleEmailChange} 
          onKeyDown={this.handleKeyDown}
          modules={this.modules}
          ref={ quill => this.quill = quill }
          value={this.props.emailTemplate} 
        />

        <div >
          <Dialog 
            active={this.state.showTokens}
            onEscKeyDown={this.handleToggle}
            onOverlayClick={this.handleToggle}
            title='Insert Token' 
          >

            <div>
              <p>Tokens represent values that are inserted
                into your email before it is sent</p><br />
              <select style={{width:'150px'}} value={this.state.token} 
                className="ql-size" onChange={this.handleTokenChange} >
                <option value="">Token...</option>
                <option value="%BRAND-EMAIL-LOGO%">Email Logo</option>
                <option value="%BRAND-COMPANY%">Company Name</option>
                <option value="%COACH-FIRSTNAME%">Coach First Name</option>
                <option value="%COACH-LASTNAME%">Coach Last Name</option>
                <option value="%COACH-EMAIL%">Coach Email</option>
                <option value="%CLIENT-FIRSTNAME%">Client First Name</option>
                <option value="%CLIENT-LASTNAME%">Client Last Name</option>
                <option value="%CLIENT-EMAIL%">Client Email</option>
                <option value="%INVITE-CODE%">Invitation Code</option>
                <option value="%ACCEPT-URL%">Accept URL</option>
                <option value="%BASE-URL%">Base URL</option>
                <option value="%LOGIN-URL%">Login URL</option>
              </select>
            </div>

          </Dialog>
        </div>

      </div>
    );

  }
}

export default QuillPage
