import React from 'react';
import { Button, Input, Dialog, Checkbox } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import Dropzone from 'react-dropzone';
import { addAssetMetaData } from '../utils/api';

class UploadAssetPage extends React.Component {

	constructor(props) {
		super(props);

    this.state = {
      active: false,
      docName: '',
      selectedFile: '',
      shared: false,
      showMessage: false,
      message: '',
      fileErrorMsg: ''
    };

    this.onDrop = this.onDrop.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.toggleShared = this.toggleShared.bind(this);
    this.handleMsgToggle = this.handleMsgToggle.bind(this);
    
	}

  onDrop(files) {

    const fileWhiteList = ['jpg','tiff','png','bmp','pdf','txt','doc','docx','xls','xlsx','key','ppt','pptx'];

    // Check for allowed file type
    let filePrefix = files[0].name.split('.').pop(-1);
    if (fileWhiteList.indexOf(filePrefix) < 0) {
      this.setState( {fileErrorMsg: 'The type of file chosen is not allowed'} );
      return; // not a valid file type
    }

    // Check for allowed file size
    const fileSize = +files[0].size;
    const maxSize = 1000000;
    const strMaxSize = '1 MB';
    if ( fileSize > maxSize ) {
      this.setState( {fileErrorMsg: 'File size exceeds the maximum allowed of ' + strMaxSize} );
      return; // not a valid file type
    }

    this.setState( {fileErrorMsg: ''} );
    this.setState( { selectedFile: files[0] });
    this.handleToggle();
  }

  handleToggle() {
    this.setState({active: !this.state.active});
  }

  handleMsgToggle() {
    this.setState({showMessage: !this.state.showMessage});
  }

  toggleShared() {
    this.setState({shared: !this.state.shared});
  }

  handleChange(name, value) {
    this.setState( {docName: value} );
  };

	handleYes() {

    // Turn off yes/no dialog
    this.setState({active: !this.state.active});

    // Verify we have all needed input
		if (this.state.selectedFile == '' || this.state.docName == '') {
      this.setState( { message: 'Required input is missing' } );
      this.handleMsgToggle();
      return;
    }

    // uploadFile will be called when we have received a valid signed request
    const uploadFile = (file, signedRequest, url) => {

      const xhr = new XMLHttpRequest();

      // Create request event handlers
      xhr.onreadystatechange = () => {

        if(xhr.readyState === 4){
          if(xhr.status === 200){
            // The file has been uploaded. Now update the database with asset meta data
            let apiData = {};
            apiData['userId'] = this.props.params.id;
            apiData['assetType'] = (this.state.shared) ? 'A' : 'I';
            apiData['assetName'] = this.state.docName;
            apiData['assetUrl'] = url;

            try {
              addAssetMetaData(apiData)
                .then( () => window.location = '/admin/documents');
            } catch (err) {
              this.setState( { message: 'Unable to complete upload: failed to save document details' } );
              this.handleMsgToggle();
            }

          } else {
            // Handle file upload error
            //console.log(xhr);
            this.setState( { message: 'Unable to complete upload: failed to send document' } );
            this.handleMsgToggle();
          }
        }
      };

      xhr.open('PUT', signedRequest);
      xhr.send(file);
    } // End uploadFile
    
    // Main function section - Make api call to get signed request url
    const file = this.state.selectedFile;
    const fileNameEncoded = encodeURIComponent(file.name);
    const fileTypeEncoded = encodeURIComponent(file.type);
    const url = `/api/sign-s3/${fileNameEncoded}/${fileTypeEncoded}`;
    const xhr = new XMLHttpRequest();

    // Create Request event handlers
    xhr.onreadystatechange = () => {

      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          // We have the signed request - now upload the file
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          // Handle get signed request error
          this.setState( {message: 'Unable to complete upload: failed to get signed signature'});
          this.handleMsgToggle();
        }
      }
    };

    // Call the api to get the signed request
    xhr.open('GET', url);
    xhr.send();
	}


	render() {

		return (
			<Page >
        <span className={AppStyles.page_title} >
          <Button icon='arrow_back' href='/admin' />Upload a Document</span>

        <div style={{width: '300px', margin: '0 auto'}} >
          <p>Enter a document name, indicate if it should be shared automatically
            with new clients, and then pick the document file to start upload</p>
          <Input name="doc_name" label="Document Name" required={true} 
            value={this.state.docName} onChange={this.handleChange.bind(this, 'docName')} />
        </div>

        <div style={{textAlign:'center'}} >
          <Checkbox label='Shared Automatically' 
            checked={this.state.shared} onChange={this.toggleShared} />
        </div>

        <div>
          <Dropzone className={AppStyles.drop_zone} onDrop={this.onDrop} multiple={false} >
            <div className={AppStyles.drop_zone_inner} >
              Drop a document file here for upload 
              (or click to select one from your computer). 
              Only one allowed.
            </div>
          </Dropzone>
        </div>
        
        <div>
          <Dialog 
            active={this.state.active}
            onEscKeyDown={this.handleToggle}
            onOverlayClick={this.handleToggle}
            title='Confirm Upload' 
            actions={[
              { label: "No", onClick: this.handleToggle },
              { label: "Yes", onClick: this.handleYes }
            ]} >

            <div>
              Proceed with uploading {this.state.docName} ({this.state.selectedFile.name})?
            </div>

          </Dialog>
        </div>
        <div style={{textAlign:'center',color:'red'}} >
          <p>{this.state.fileErrorMsg}</p>
        </div>

        <div>
          <Dialog 
            active={this.state.showMessage}
            onEscKeyDown={this.handleMsgToggle}
            onOverlayClick={this.handleMsgToggle}
            title='Upload Error' 
            actions={[
              { label: "Ok", onClick: this.handleMsgToggle }
            ]} >

            <div>
              {this.state.message}
            </div>

          </Dialog>
        </div>

	    </Page >
		);
	}

}

export default UploadAssetPage