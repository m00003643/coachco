import { handleActions } from 'redux-actions';
import { updateBranding } from './branding-actions';

export const reducer = handleActions({
  // action.payload == branding info
  [updateBranding]: (state, action) => action.payload
}, window.INITIAL_STATE.branding);

export const stateKey = 'branding';

export const selectors = {
  getBranding: state => state[stateKey]
};
