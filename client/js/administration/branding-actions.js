import { createAction } from 'redux-actions';

export const updateBranding = createAction('UPDATE_BRANDING', branding => branding);