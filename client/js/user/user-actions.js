import { createAction } from 'redux-actions';

export const updateUser = createAction('UPDATE_USER', user => user);
