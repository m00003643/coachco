import { handleActions } from 'redux-actions';
import { updateUser } from './user-actions';

export const reducer = handleActions({
  // action.payload == the user
  [updateUser]: (state, action) => action.payload
}, window.INITIAL_STATE.authenticatedUser);

export const stateKey = 'user';

export const selectors = {
  getUser: state => state[stateKey]
};
