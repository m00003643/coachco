import React from 'react';
import { Button } from 'react-toolbox';
import { List, Dialog, ListItem, Checkbox } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import ClientAssignAssessment from './ClientAssignAssessment.jsx';
import { assignAssessment, deleteAssignedAssesssment, updateAssessment }  from '../utils/api';
import { reduxForm, SubmissionError } from 'redux-form';
import closest from 'dom-closest';

class ClientAssessments extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      checkbox: false,
      showAssignDialog: false,
			showVerify: false,
			selectedId: -1,
      message: '',
      mentor: {}
    };

    this.handleAssignToggle = this.handleAssignToggle.bind(this);
    this.assignAssessment = this.assignAssessment.bind(this);
    this.handleDeleteAssessment = this.handleDeleteAssessment.bind(this);
    this.handleDeleteToggle = this.handleDeleteToggle.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleClick = this.handleClick.bind(this);

  }

  handleClick(e) {
    const el = closest(e.target, '.item_click');
    if (el != null) window.open(el.getAttribute('data-payload'), '_blank');
  }

	handleDeleteAssessment(e) {
		const assignedId = e.target.getAttribute('data-assigned_id');
    this.setState({showVerify: !this.state.showVerify, selectedId: assignedId});
	}

	handleDeleteToggle() {
    this.setState({showVerify: !this.state.showVerify, assetId: -1, message: ''});
	}

	handleYes() {

    this.setState({showVerify: !this.state.showVerify, message: ''});
		if (this.state.selectedId < 0) return;

    try {
      deleteAssignedAssesssment(this.state.selectedId)
        .then(data => {location.reload()})
    }
    catch(err) {
      console.log(err);
      this.setState( {message: 'Failed to remove assigned assessment'} );
    }

	}

  handleMentorChange(id, fldName, value) {

    // Update mentor reviewable checkbox in state so component will render properly
    let mentor = {...this.state.mentor};
    mentor[fldName] = { state: value, [id]: id};
    this.setState( { mentor: mentor } );

    // Make a copy (a) of the assessment that is being updated
    let a = {};
    this.props.assessments.map( assessment => {
      if ( assessment.id === id ) {
        a = {...assessment};
        return
      }
    });

    // Set the assessment copy data (mentorReviewable only)
    a.assessment.mentorReviewable = (value) ? 'Y' : 'N';

    // Call the api to do the update
    try {
      updateAssessment(a.id, a)
        .then(data => {location.reload()})
    }
    catch(err) {
      console.log(err);
      this.setState( {message: 'Failed to update mentor info'} );
    }

  }

  assignAssessment(template) {

    // Build the assessment payload for the assignAssessment API call.
    let assessment = {};
    const thisDay = new Date();
    assessment.assessment = template.assessmentTemplate;
    assessment.userId = this.props.clientId;
    assessment.assessment.dateAssigned = thisDay.getMonth()+1 + '/' + thisDay.getDate() + '/' + thisDay.getFullYear();

    // Call the API
    //
    try {
      assignAssessment(assessment)
        .then(data => {location.reload()})
    }
    catch(err) {
      console.log(err);
      this.setState( {message: 'Failed to assign the assessment'} );
    }

    this.handleAssignToggle();
  }

  handleAssignToggle() {
    this.setState({showAssignDialog: !this.state.showAssignDialog});
  }

  render() {

    const { assessments, coachId, clientId, initialValues, sharedAssessments } = this.props;

    // Get just this client's shared assessments
    let thisClientsShared = [];
    sharedAssessments.map( assessment => {
      if (assessment.coachId == clientId) thisClientsShared.push(assessment);
    });

    return (
      <div>
        <label>Check any assessments you wish to be viewable by your mentor</label>

        <List selectable ripple>
          {assessments.map( (assessment, index) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={assessment.id} 
              onClick={this.handleClick} data-payload={`/clients/${assessment.userId}/assessments/${index}`}>
              <ListItem
                key={assessment.id}
                caption={assessment.assessment.type}
                legend={(assessment.assessment.dateCompleted != null) ? 'Completed on ' + assessment.assessment.dateCompleted : 'Assigned on ' + assessment.assessment.dateAssigned}
                leftActions={[
                  <div key={assessment.id} onClick={e => e.stopPropagation()}>
                    <Checkbox key={assessment.id} 
                      checked={ (typeof this.state.mentor[`cb${index}`] != 'undefined') ? this.state.mentor[`cb${index}`].state : (assessment.assessment.mentorReviewable === 'Y') ? true : false  }
                      onChange={this.handleMentorChange.bind(this, assessment.id,`cb${index}`)} />
                  </div>
                ]}
                rightActions={[
                  <div key={assessment.id} onClick={e => e.stopPropagation()}>
                    <Button label='Delete' name={`delAsset${assessment.id}`} icon='cancel' 
                    onMouseUp={this.handleDeleteAssessment} key={assessment.id} data-assigned_id={assessment.id}/>
                  </div>
                ]}
              />
            </div>
          ))}
        </List>

        <List selectable ripple>
          {thisClientsShared.map( (assessment, index) => (
            <ListItem
              key={index}
              leftIcon='person'
              caption={
                assessment.assessment.type
              }
              legend={assessment.clientname + ' shared by ' + assessment.coachname}
              to={`/assessments/shared/${clientId}#${assessment.id}`}
              onClick={this.handleClick}
            />
          ))}
        </List>

        <div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

        <div className={AppStyles.action_button} >
          <Button icon='note_add' label='Assign Assessment' flat primary 
            onClick={this.handleAssignToggle.bind(this)} />
        </div>        

        <Dialog 
          active={this.state.showAssignDialog}
          onEscKeyDown={this.handleAssignToggle}
          onOverlayClick={this.handleAssignToggle}
          title='Assign Assessment' 
        >
          <ClientAssignAssessment onCancel={this.handleAssignToggle}
            onChange={this.assignAssessment} coachId={coachId} />
        </Dialog>

        <Dialog 
          active={this.state.showVerify}
          onEscKeyDown={this.handleDeleteToggle}
          onOverlayClick={this.handleDeleteToggle}
          title='Confirm Delete' 
          actions={[
            { label: "No", onClick: this.handleDeleteToggle },
            { label: "Yes", onClick: this.handleYes }
          ]} 
        >
          <p>Are you sure you want to remove this assigned assessment?</p>
        </Dialog>
      </div>
    )
  }
}

export default ClientAssessments;