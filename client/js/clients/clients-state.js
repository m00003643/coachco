import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';

export const reducer = handleActions({}, buildMapFromClientArray(window.INITIAL_STATE.clients));

export const stateKey = 'clients';

export const selectors = {
  getClients: createSelector(
    state => state[stateKey],
    clientsMap => Object.values(clientsMap)
  ),
  getClient: (state, id) => {
  	return state[stateKey][id] || null
  },
  getAssets: (state, id) => {
    return state[stateKey][id].assets || null
  },
  getAssessments: (state, id) => {
    return state[stateKey][id].assessments || null
  },
  getClientAssessment: (state, id, id2) => {
    return state[stateKey][id].assessments[id2] || null
  }
};

function buildMapFromClientArray(clients) {
	const ret = {};

	if (!clients) return ret;

	clients.forEach(function(client) {
    // we want `log-state.js` to be the sole source of truth
    // on logs, so we'll strip them out of our state slice
    client.phone = (client.phone == null) ? '' : client.phone;
    const { log, ...otherClientProps } = client;

    ret[client.id] = otherClientProps;
  });

	return ret;
}
