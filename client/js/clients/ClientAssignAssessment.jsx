import React from 'react';
import { connect } from 'react-redux';
import { getCoachAssessmentTemplates } from '../utils/api';
import { selectors as templateSelectors } from '../templates/templates-state';
import { updateTemplates } from '../templates/templates-actions';
import { Button, Dropdown, ProgressBar } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';

class ClientAssignAssessment extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      chosenTemplate: null
    }

    this.handleAssignment = this.handleAssignment.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.extractTemplate = this.extractTemplate.bind(this);
    
  }

  componentDidMount() {

    // If the assessement templates for this coach are already in state then nothing to do here
    if (this.props.assessmentTemplates.loaded === true) return;

    try {
      getCoachAssessmentTemplates(this.props.coachId).then( 
        data => {
          // Dispatch the updateTemplates action...
          this.props.dispatch(
            updateTemplates( {loaded: true, templates: data.result } )
          );
        }
      );
    } catch (err) {
      if (err.validationErrors) {
        console.log(err.validationErrors);
        //throw new SubmissionError(err.validationErrors);
      }

      console.log('Some unknow error occurred: ' + err.errorMessage);
      //throw new SubmissionError({
      //  _error: err.errorMessage || 'There was an unknown error while attempting to save form.'
      //});
    }
  }

  handleChange(val) {
    this.setState( { chosenTemplate : val} );
  }

  extractTemplate() {

    let template = null;

    this.props.assessmentTemplates.templates.map( record => {
      if (record.id === this.state.chosenTemplate) {
        template = record;
        return;
      }
    });

    return template;

  }

  handleAssignment() {

    const template = this.extractTemplate();
    this.props.onChange(template);
  }

  render() {

    const { onCancel, coachId, assessmentTemplates } = this.props;
    const loaded = assessmentTemplates.loaded;

    let templates = [];
    assessmentTemplates.templates.map( obj => {
      templates.push({'value': obj.id, 'label': obj.assessmentTemplate.type});
    });

    return (

      <div>

        <div className={(loaded == true) ? AppStyles.hide_me : AppStyles.show_me} 
          style={{textAlign: 'center'}} >
          <ProgressBar type="circular" mode="indeterminate" multicolor={true} />          
        </div>

        <div>
          <Dropdown
            onChange={this.handleChange}
            source={templates}
            value={this.state.chosenTemplate}
            label={ (loaded == true) ? 'Template...' : 'Loading Templates...' } 
            disabled={ (loaded == true) ? false : true }/>
        </div>

        <div style={{clear: 'both'}} >
          <div style={{float: 'right'}} >
            <Button label='Cancel' onClick={onCancel} raised={true} 
              style={{marginRight: '15px'}} />
            <Button label={`Assign`} onClick={this.handleAssignment} raised={true} 
              disabled={ (this.state.chosenTemplate === null) ? true : false }/>
          </div>
        </div>

      </div>

    );
  };
};

const mapStateToProps = state => {

  return {
    assessmentTemplates: templateSelectors.getAssessmentTemplates(state)
  };
};

export default connect(mapStateToProps)(ClientAssignAssessment);
