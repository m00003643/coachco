import React from 'react';
import { connect } from 'react-redux';
import { Panel, Button, Tabs, Tab } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as clientSelectors } from './clients-state';
import { selectors as logSelectors } from '../log/log-state';
import ProfileForm from '../profile/ProfileForm.jsx';
import ClientAssessments from './ClientAssessments.jsx';
import ClientAssets from './ClientAssets.jsx';
import ChatPageClient from '../chat/ChatPageClient.jsx';
import LogList from '../log/LogList.jsx';
import { getSharedAssessments } from '../utils/api';

class ClientPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = { 
			index: 0,
			sharedAssessments: [],
			message: ''
	 };

		// Set the appropriate tab based on hash contained in url
		let hash = this.props.location.hash || null;
		this.state.index = (hash == '#details') ? 0 :
			(hash == '#assessments') ? 1 :
			(hash == '#documents') ? 2 :
			(hash == '#logs') ? 3 : 
			(hash == '#chat') ? 4 : 0;
  }

	componentDidMount() {

		let showInactive = (this.props.location.hash && this.props.location.hash == '1') ? true : false;
		this.setState( { showInactive: showInactive } );	

    try {
      getSharedAssessments(this.props.client.coachId).then( 
        data => {
					if (data.result && data.result.length > 0)
						this.setState( {
							sharedAssessments: data.result,
							message: ''
						} );
        }
      );
    } catch (err) {
			this.setState( {message: 'Error fetching shared assessments'} );
    }

	}

  handleTabChange(index) {
		let hash = (index == 0) ? '#details' :
			(index == 1) ? '#assessments' :
			(index == 2) ? '#documents' :
			(index == 3) ? '#logs' : 
			(index == 4) ? '#chat' : '#details';
		
    this.setState({ index });
		window.location = window.location.pathname + hash;
  }

	render() {

		const { client, assets, assessments, log, clientId } = this.props;

		return (
			<Page >
					
				<span className={AppStyles.page_title} >
					<Button icon='arrow_back' href='/clients' />
					{client.firstname} {client.lastname}
				</span>

				<Tabs index={this.state.index} onChange={this.handleTabChange.bind(this)}>
					<Tab label={(window.innerWidth < 441) ? '' : 'Detail'} icon='person'>
						<ProfileForm profile={client} canUpdateRole={true} /></Tab>
					<Tab label={(window.innerWidth < 441) ? '' : 'Assessments'} icon='description'>
						<ClientAssessments assessments={assessments} coachId={client.coachId} clientId={client.id} 
							sharedAssessments={this.state.sharedAssessments} /></Tab>
					<Tab label={(window.innerWidth < 441) ? '' : 'Documents'} icon='attachment'>
						<ClientAssets assets={assets} coachId={client.coachId} clientId={client.id} /></Tab>
					<Tab label={(window.innerWidth < 441) ? '' : 'Log'} icon='watch'>
						<LogList clientId={clientId} clientLog={log} call_origin='client_logs' /></Tab>
					<Tab label={(window.innerWidth < 441) ? '' : 'Chat'} icon='chat'>
						<ChatPageClient user={client} /></Tab>
				</Tabs>

				<div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

  const id = +ownProps.params.id;

	return {
		client: clientSelectors.getClient(state, id),
		assets: clientSelectors.getAssets(state, id),
		assessments: clientSelectors.getAssessments(state, id),
		log: logSelectors.getLogByClient(state, id),
		clientId: id
	};
};

export default connect(mapStateToProps)(ClientPage);