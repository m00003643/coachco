import React from 'react';
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox, 
	Button, Dialog, Avatar, FontIcon } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import ClientAssignAsset from './ClientAssignAsset.jsx';
import closest from 'dom-closest';
import { updateUser, deleteUser } from '../utils/api';
import moment from 'moment';

class ClientsList extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			active: false,
			prompt: '',
			actions: [],
			userStatus: null,
			clientId: null,
			message: ''
		}

    this.handleStatusChange = this.handleStatusChange.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleStatusAction = this.handleStatusAction.bind(this);
		
	}

	getClientIcon(client) {

		const role = client.role;
		const status = client.status;

		const icon = 
			( role === 'COACH' && status === 'PENDING' ) ? 'people_outline' :
			( role === 'COACH' && status === 'INACTIVE' ) ? 'people_outline' :
			( role === 'COACH' ) ? 'people' :
			( role === 'CLIENT' && status === 'PENDING' ) ? 'person_outline' :
			( role === 'CLIENT' && status === 'INACTIVE' ) ? 'person_outline' :
			( role === 'CLIENT' ) ? 'person' :
			'person';

		return icon;

	}

	getClientIconStyle(client) {

		// Determine if the date this client was invited (created) is old (>29 days) or not
		const rightNow = moment(new Date());
		const dateCreated = moment(client.created);
		const old = ( rightNow.diff(dateCreated, 'days') > 29 ) ? true : false; 

		// Calculate the style based on client status and days since invited.
		const style = 
			( client.status === 'PENDING' && !old ) ? { fontSize: '18px', color:'green', float:'left' } :
			( client.status === 'PENDING' && old ) ? { fontSize: '18px', color:'red', float:'left' } :
			{ fontSize:'18px',float:'left' };

		return style;
		
	}

	handleToggle() {
    this.setState({active: !this.state.active});
	}

	async handleStatusAction() {

		if (this.state.userStatus === 'REQUEST') return;

		// Is the invitation being revoked?
		if (this.state.userStatus === 'REVOKE') {
			try {
				await deleteUser(this.state.clientId).then( data => {
					location.reload();
				});
			} catch (err) {
				console.log(err);
				this.setState( {message: 'Failed to revoke invitation', active: false} );
			}
			return;
		}

		// We must be setting user status to ACTIVE or INACTIVE
    try {
			const user = { status: this.state.userStatus };
			await updateUser(this.state.clientId, user).then( data => {
				location.reload();
			});
    } catch (err) {
			console.log(err);
			this.setState( {message: 'Failed to change user status', active: false} );
    }
		
	}

	handleStatusChange(e) {

		// Get id of user that was clicked on
		const clientId = e.target.getAttribute('data-payload');

		// Get user record from props
		let user = {};
		this.props.clients.map( client => {
			if (client.id == clientId) {
				user = client;
				return;
			}
		});

		// Set action dialog properties
		const prompt = 
			( user.status === 'ACTIVE' ) ? 'Set this user to INACTIVE status?' :
			( user.status === 'INACTIVE' ) ? 'Set this user to ACTIVE status?' :
			( user.status === 'REQUEST' ) ? 'Send an invite to this user?' :
			( user.status === 'PENDING' ) ? 'Revoke this invitation?' :
			null;

		if (prompt == null) return;

		/*
		let actions = 
			( user.status === 'PENDING' ) ?
			  [
    			{ label: "Ok", onClick: this.handleToggle },
  			] :
			  [
    			{ label: "Cancel", onClick: this.handleToggle },
    			{ label: "Ok", onClick: this.handleStatusAction }
  			];
		*/

		let actions = [
    			{ label: "Cancel", onClick: this.handleToggle },
    			{ label: "Ok", onClick: this.handleStatusAction }
  			];

		const newStatus = 
			( user.status === 'ACTIVE' ) ? 'INACTIVE' :
			( user.status === 'INACTIVE' ) ? 'ACTIVE' :
			( user.status === 'REQUEST' ) ? 'REQUEST' : 
			( user.status === 'PENDING' ) ? 'REVOKE' : 'UNKNOWN';

		// Launch the dialog
		this.setState( {prompt: prompt, actions: actions, userStatus: newStatus, clientId: clientId} );
		this.handleToggle();

	}

	handleClick(e) {
    const el = closest(e.target, '.item_click');
		const clientId = el.getAttribute('data-payload');
		window.location = '/clients/' + clientId + '#details';
	}

	render() {

		const { clients, showInactive } = this.props;

		let filteredClients = [];

		if (!showInactive) {
			clients.map( client => {
				if (client.status != 'INACTIVE' ) filteredClients.push(client);
			});
		} else {
			filteredClients = clients;
		}

		return (

			<div>

				<List selectable ripple>
					{filteredClients.map(client => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={client.id} 
              onClick={this.handleClick} data-payload={client.id}>
							<ListItem
								key={client.id}
								caption={client.firstname + ' ' + client.lastname}
								legend={client.email + ' | ' + client.phone }
								onClick={this.handleClick}
								rightActions={[
									<div key={client.id} onClick={e => e.stopPropagation()} >
										<div style={{float:'left'}}
											className={(this.props.seenMap[client.id] == true || typeof this.props.seenMap[client.id] === 'undefined') ? AppStyles.hide_me : AppStyles.show_me}>
											<FontIcon value='chat' style={{paddingTop:'10px',fontSize:'20px'}}/>
										</div>
										<Button icon={this.getClientIcon(client)}
											onMouseUp={this.handleStatusChange} key={client.id} 
											data-payload={client.id}
											style={this.getClientIconStyle(client)} />
									</div>
								]}
							/>
						</div>
					))}
				</List>

        <div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

				<div style={{clear:'both'}} >
					<div className={AppStyles.action_button}>
						<Button icon='person' label='Send Invitation' flat primary href='/clients/add' />
					</div>        
				</div>

				<div>
					<Dialog 
						active={this.state.active}
						onEscKeyDown={this.handleToggle}
						onOverlayClick={this.handleToggle}
						title='User Status' 
						actions={this.state.actions} >

						<div>
							{this.state.prompt}
						</div>

					</Dialog>
				</div>

			</div>
		);
	}

}

export default ClientsList;