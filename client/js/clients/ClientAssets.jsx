import React from 'react';
import { List, Dialog, ListItem, Button } from 'react-toolbox';
import ClientAssignAsset from './ClientAssignAsset.jsx';
import { assignAsset, deleteAssignedAsset, getClientSharedAsset } from '../utils/api';
import AppStyles from '../common/styles/app.scss';
import closest from 'dom-closest';

class ClientAssets extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      active: false,
			showVerify: false,
			selectedId: -1,
      message: '',
      clientShares: []
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.assignAsset = this.assignAsset.bind(this);
    this.handleDeleteAsset = this.handleDeleteAsset.bind(this);
    this.handleVerify = this.handleVerify.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleClick = this.handleClick.bind(this);

  }

  componentWillMount() {

    getClientSharedAsset(this.props.clientId)
      .then(data => {
        let shares = [];
        data.result.map(shared => {
          if (shared.assetType == 'C') {
            shares.push({
              name: shared.assetName,
              url: shared.assetUrl
            })
          }
        })
        this.setState( {clientShares: shares} );
      },
      err => {
        console.log(err);
        this.setState( {message: 'Failed to fetch shared assets'} );
      }
    );

  }

	handleDeleteAsset(e) {
		const assignedId = e.target.getAttribute('data-assigned_id');
    this.setState({showVerify: !this.state.showVerify, selectedId: assignedId});
	}

	handleVerify() {
    this.setState({showVerify: !this.state.showVerify, assetId: -1, message: ''});
	}

	handleYes() {

    this.setState({showVerify: !this.state.showVerify, message: ''});
		if (this.state.selectedId < 0) return;

    deleteAssignedAsset(this.state.selectedId)
      .then(data => {
        window.location.reload();
      },
      err => {
        console.log(err);
        this.setState( {message: 'Failed to remove assigned document'} );
      }
    );
	}

  assignAsset(asset) {

    try {
      assignAsset(asset).then( 
        data => {
          window.location.reload();
        }
      );
    } catch (err) {
      console.log(err);
      this.setState( {message: 'Failed to assign document'} );
    }

    this.handleToggle();
  }

  handleToggle() {
    this.setState({active: !this.state.active, message: ''});
  }

  handleClick(e) {
    const el = closest(e.target, '.item_click');
    if (el != null) window.open(el.getAttribute('data-payload'), '_blank');
  }

  getFileType(fname) {
    let fileExt = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();

    let picType = ['jpg','png','bmp','tiff'].includes(fileExt);
    let pdfType = ['pdf'].includes(fileExt);

    return ( picType ? 'photo' : pdfType ? 'picture_as_pdf' : 'attachment' );

  }
  
  render() {

    const { assets, coachId, clientId } = this.props;

    // Build a map of currently assigned assets(their assetId). This will be used to
    // fiter the list of choices presented when assigning a new asset to a client.
    //
    let alreadyAssignedAssetIds = [];
    assets.map( asset => { alreadyAssignedAssetIds.push(asset.assetId)});

    return (
      <div>

        <List selectable ripple>
          {assets.map( (asset, index) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={asset.assetId} 
              onClick={this.handleClick} data-payload={asset.assetUrl}>
              <ListItem
                key={asset.assetId}
                caption={asset.assetName}
                leftIcon={ this.getFileType(asset.assetUrl) }
                rightActions={[
                  <div key={asset.assetId} onClick={e => e.stopPropagation()}>
                    <Button label='Delete' name={`delAsset${asset.assetId}`} icon='cancel' 
                    onMouseUp={this.handleDeleteAsset} key={asset.assetId} data-assigned_id={asset.id}/>
                  </div>
                ]}
            />
           </div>
          ))}
        </List>

        <div style={{textAlign:'center',fontSize:'11px'}} >
          <label>Shared by client</label>
        </div>

        <List selectable ripple>
          {this.state.clientShares.map( (share, index) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={index} 
              onClick={this.handleClick} data-payload={share.url}>
              <ListItem
                key={index}
                caption={share.name}
                leftIcon={ this.getFileType(share.url) }
                rightIcon='share'
            />
           </div>
          ))}
        </List>

        <div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

        <div style={{clear:'both'}} >
          <div className={AppStyles.action_button}>
            <Button icon='add' label='Assign Document' flat primary 
              onClick={this.handleToggle.bind(this)} />
          </div>        
        </div>

        <div>
          <Dialog 
            active={this.state.active}
            onEscKeyDown={this.handleToggle}
            onOverlayClick={this.handleToggle}
            title='Assign Document' >

            <ClientAssignAsset 
              onCancel={this.handleToggle}
              onChange={this.assignAsset}
              coachId={coachId}
              clientId={clientId}
              alreadyAssignedAssetIds={alreadyAssignedAssetIds}
            />

          </Dialog>
        </div>

				<div>
					<Dialog 
						active={this.state.showVerify}
						onEscKeyDown={this.handleVerify}
						onOverlayClick={this.handleVerify}
						title='Confirm Delete' 
						actions={[
							{ label: "No", onClick: this.handleVerify },
							{ label: "Yes", onClick: this.handleYes }
						]} >

						<div>
							Are you sure you want to remove this from your list of available documents?
						</div>

					</Dialog>
				</div>

      </div>
    );
  };
};

export default ClientAssets;