import React from 'react';
import { reduxForm, SubmissionError } from 'redux-form';
import { Button } from 'react-toolbox';
import Field from '../common/Field.jsx';
import SliderField from '../common/SliderField.jsx';
import AppStyles from '../common/styles/app.scss';

class AssessmentForm extends React.Component {

  async handleSubmit(data) {
    console.log(values);
    // Do nothing
  }

	render() {

    const { invalid, error, handleSubmit, pristine, submitting } = this.props;
		const assessment = this.props.assessment.assessment;
    const clientId = this.props.assessment.userId;
    const questions = assessment.prompts;

		return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <div>
          
          <span className={AppStyles.page_title}>
            <Button icon='arrow_back' href={`/clients/${clientId}/#assessments`} />
            {assessment.type}
          </span>

          <p className={AppStyles.notes_header}>{assessment.directions_title}:</p>
          <label className={AppStyles.notes_content}>{assessment.directions}</label>
          <br /><br />

          {
              questions.map( (question, index) => (
                <div key={index}>
                  <p>{question.prompt}</p>
                  <SliderField name={`prompt-${index}`}
                    min={0} max={10} step={1} pinned={true} snaps={true} disabled={true} />
                </div>
              ))
          }

          <p className={AppStyles.notes_header}>{assessment.feedback_title}:</p>
          <label className={AppStyles.notes_content}>{assessment.feedback_text}</label>
          <Field type='text' name="feedback" multiline={true} label='' maxLength={200}
            rows={2} placeholder='Your input is welcome' disabled={true} />

        </div>

      </form>
		);
	}

}

const AssessmentFormContainer = reduxForm({
  form: 'client_assessment'
})(AssessmentForm);

export default props => {
  const initialValues = props.assessment.assessment.prompts.reduce((vals, prompt, i) => {
    vals[`prompt-${i}`] = prompt.value;
    return vals;
  }, {});

  return <AssessmentFormContainer {...props} initialValues={initialValues} />;
};
