import React from 'react';
import { connect } from 'react-redux';
import ProfileForm from '../profile/ProfileForm.jsx';
import { Panel, AppBar} from 'react-toolbox';
import Page from '../common/Page.jsx';
import { selectors as profileSelectors } from '../profile/profile-state';
import { selectors as userSelectors } from '../user/user-state';
import AppStyles from '../common/styles/app.scss';

class AddClientPage extends React.Component {

	render() {

		const { profile, coach } = this.props;
		const coachId = coach.id;

		return (
			<Page >
				<span className={AppStyles.page_title}>Create Invitation</span>
				<ProfileForm profile={profile} coachId={coachId} canUpdateRole={true}/>
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
		profile: profileSelectors.getProfileTemplate(state),
		coach: userSelectors.getUser(state)
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(AddClientPage);