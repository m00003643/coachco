import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as clientSelectors } from './clients-state';
import ClientAssessment from './ClientAssessment.jsx';

class ClientAssessmentPage extends React.Component {

	render() {
		
		const { params } = this.props;

		return (
			<Page >
				<ClientAssessment assessment={this.props.assessment} />
			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

  const clientId = ownProps.params.id;
	const assessmentId = ownProps.params.id2;

	return {
		assessment: clientSelectors.getClientAssessment(state, clientId, assessmentId)
	};
};

export default connect(mapStateToProps)(ClientAssessmentPage);