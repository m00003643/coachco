import React from 'react';
import { connect } from 'react-redux';
import { getCoachDefaultAssets } from '../utils/api';
import { selectors as defaultAssetSelectors } from '../templates/defaultAssets-state';
import { updateDefaultAssets } from '../templates/defaultAssets-actions';
import { Button, Dropdown, ProgressBar } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import { lodash as _ } from 'lodash';

class ClientAssignAsset extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      chosenAsset: null
    }

    this.handleAssignment = this.handleAssignment.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
  }

  componentDidMount() {

    //If the default assets for this coach are already in state then nothing to do here
    if (this.props.defaultAssets.loaded === true) return;

    try {
      getCoachDefaultAssets(this.props.coachId).then( 
        data => {

          // Strip out L type (Logo) assets
          let assets = [];
          data.result.map( asset => {
            if (asset.assetType != 'L') assets.push(asset);
          });

          // Dispatch the updateDefaultAssets action...
          this.props.dispatch(
            updateDefaultAssets( {loaded: true, assets: assets } )
          );
        }
      );
    } catch (err) {
      if (err.validationErrors) {
        console.log(err.validationErrors);
        //throw new SubmissionError(err.validationErrors);
      }

      console.log('Some unknow error occurred: ' + err.errorMessage);
      //throw new SubmissionError({
      //  _error: err.errorMessage || 'There was an unknown error while attempting to save form.'
      //});
    }
  }

  handleChange(val) {
    this.setState( { chosenAsset : val} );
  }

  handleAssignment() {
    this.props.onChange( {assetId: this.state.chosenAsset, userId: this.props.clientId} );
  }

  render() {

    const { onCancel, coachId, defaultAssets, alreadyAssignedAssetIds } = this.props;
    const loaded = defaultAssets.loaded;

    let assets = [];
    defaultAssets.assets.map( obj => {
      if ( !alreadyAssignedAssetIds.find(x => x === obj.id) ) {
        // Just the assets that haven't already been assigned, please.
        assets.push({'value': obj.id, 'label': obj.assetName});
      }
    });

    return (

      <div>

        <div className={(loaded == true) ? AppStyles.hide_me : AppStyles.show_me} 
          style={{textAlign: 'center'}} >
          <ProgressBar type="circular" mode="indeterminate" multicolor={true} />          
        </div>

        <div>
          <Dropdown
            onChange={this.handleChange}
            source={assets}
            value={this.state.chosenAsset}
            label={ (loaded == true) ? 'Document...' : 'Loading Documents...' } 
            disabled={ (loaded == true) ? false : true }/>
        </div>

        <div style={{clear: 'both'}} >
          <div style={{float: 'right'}} >
            <Button label='Cancel' onClick={onCancel} raised={true} 
              style={{marginRight: '15px'}} />
            <Button label={`Assign`} onClick={this.handleAssignment} raised={true} 
              disabled={ (this.state.chosenAsset === null) ? true : false }/>
          </div>
        </div>

      </div>

    );
  };
};

const mapStateToProps = state => {

  return {
    defaultAssets: defaultAssetSelectors.getDefaultAssetsObj(state)
  };
};

export default connect(mapStateToProps)(ClientAssignAsset);
