import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import SharedAssessment from './SharedAssessment.jsx';
import { getAssessment }  from '../utils/api';

class SharedAssessmentPage extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      assessment: null,
			message: ''
    }
  }

	componentDidMount() {

		let assessmentId = this.props.location.hash.replace('#','');

    try {
      getAssessment(assessmentId).then( 
        data => {
					this.setState( 
						{
							assessment: data.result,
							message: ''
						} 
					);
        }
      );
    } catch (err) {
			this.setState( {message: 'Error fetching shared assessments'} );
    }
		
	}


	render() {
		
		return (
			<Page >
				{ this.state.assessment && 
					<SharedAssessment 
						assessment={this.state.assessment} 
						sharingCoach={this.props.params.id}
					/> 
				}
				<div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>
			</Page>
		);
	}

}

export default SharedAssessmentPage;