import React from 'react';
import { connect } from 'react-redux';
import { Panel, Button, Dialog, Chip, Avatar, 
	Checkbox, List, ListItem } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as clientSelectors } from './clients-state';
import { selectors as userSelectors } from '../profile/profile-state';
import ClientsList from './ClientsList.jsx';
import { getSharedAssessments, GetCoachReadStatus } from '../utils/api';

class ClientsPage extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			active: false,
			showInactive: false
		}
    this.handleLegendClick = this.handleLegendClick.bind(this);
    this.handleChangeInactive = this.handleChangeInactive.bind(this);
	}

	componentDidMount() {

    try {
			GetCoachReadStatus(this.props.user.id)
			.then( data => {
				let seenMap = {};
				data.result.map( rec => {
					seenMap[rec.userId] = rec.seenByCoach;
				})
				this.setState( {seenMap: seenMap});
			})
    } catch (err) {
      console.log(err);
    }

	}

	handleLegendClick() {
    this.setState({active: !this.state.active});
	}

	handleChangeInactive() {
    this.setState({showInactive: !this.state.showInactive});
	}

	render() {
		const { clients } = this.props;

		return (
			<Page >
					<span className={AppStyles.page_title}>My Clients</span>
					<div style={{textAlign:'left'}} >
						<div style={{float:'left', paddingTop:'10px', paddingLeft:'17px'}} >
							<Checkbox label='Show Inactive' checked={this.state.showInactive} 
								onChange={this.handleChangeInactive}/>
						</div>
						<div style={{float:'right'}} >
							<Button label='Status Legend' icon='person' onMouseUp={this.handleLegendClick}/>
						</div>
					</div>
					<div style={{clear:'both'}} >					  
						{ this.state.seenMap && <ClientsList clients={clients} showInactive={this.state.showInactive} 
							seenMap={this.state.seenMap} /> }
					</div>

					<div>
						<Dialog 
							active={this.state.active}
							onEscKeyDown={this.handleLegendClick}
							onOverlayClick={this.handleLegendClick}
							title='User Status' 
							actions={[
								{ label: "Ok", onClick: this.handleLegendClick }
							]} >

							<div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'black'}} icon='person'/>Active Client</Chip>
								</div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'black'}} icon='person_outline'/>Inactive Client</Chip>
								</div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'black'}} icon='people'/>Active Coach</Chip>
								</div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'black'}} icon='people_outline'/>Inactive Coach</Chip>
								</div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'green'}} icon='person_outline'/>Recent Invitee</Chip>
								</div>
								<div>
									<Chip><Avatar style={{backgroundColor:'white', color:'red'}} icon='person_outline'/>Invited over 30 days ago</Chip>
								</div>
							</div>

						</Dialog>
					</div>
				
			</Page>
		);
	}

}

const mapStateToProps = function(state) {
	return {
		clients: clientSelectors.getClients(state),
		user: userSelectors.getUser(state)
	};
};

export default connect(mapStateToProps)(ClientsPage);