import React from 'react';
import { Field as ReduxFormField } from 'redux-form';
import { Input as ReactToolboxInput } from 'react-toolbox';

const Input = ({ input, type, label, required, maxLength, multiline,
   autoFocus, className, theme, disabled = false, meta: { touched, error } }) => {
  return (
    <ReactToolboxInput {...input}
     type={type} label={label} required={required}
     maxLength={maxLength} multiline={multiline}
     autoFocus={autoFocus} disabled={disabled}
     className={className} theme={theme}
     error={(touched && error) ? error : null} floating />
  );
};

const Field = (props) => {
  return (
    <ReduxFormField {...props} component={Input} />
  );
};

export default Field;