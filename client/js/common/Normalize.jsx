export const normalizePhone = value => {
  return value.replace(/[^\d]+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');  
}

export const makeLowerCase = value => {
  return value.toLowerCase();
}

export const roundLogHours = value => {
  return (Math.round(value * 4) / 4).toFixed(2)
}

