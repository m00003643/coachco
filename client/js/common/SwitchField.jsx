import React from 'react';
import { Field } from 'redux-form';
import { Switch as ReactToolboxSwitch } from 'react-toolbox';
import Style from './styles/SwitchField.scss';

const Switch = props => {

  const { input, label } = props;

  // Switch uses 'checked' rather than 'value' 
  let { value, ...otherInputProps } = input;

  return <ReactToolboxSwitch {...otherInputProps}  label={label} checked={value} 
    theme={Style} />;
};

const SwitchField = props => {
  return (
    <Field {...props} component={Switch} />
  );
};

export default SwitchField;