import React from 'react';
import { Field as ReduxFormField } from 'redux-form';
import { TimePicker as ReactToolboxTimePicker } from 'react-toolbox';

// Note: TimePickerField uses custom 'timeformat' prop rather than standard 'format' prop.
// redux-form also has a prop called 'format'
//
const TimePicker = ({ input, timeformat, active = false, meta, ...otherProps }) => (
 <ReactToolboxTimePicker {...input} {...otherProps} active={active} format={timeformat} onBlur={null} />
);

export default props => <ReduxFormField {...props} component={TimePicker} />;
