import React from 'react';
import { Field as ReduxFormField } from 'redux-form';
import { DatePicker as ReactToolboxDatePicker } from 'react-toolbox';

const DatePicker = ({ input, active = false, autoOk = false, cancelLabel = 'Cancel', className,
  inputClassName, inputFormat, label, maxDate, minDate, 
   readonly = false, sundayFirstDayOfWeek = true }) => {

  let { value } = input;

  // Somewhere in the dance between redux-form and react-toolbox the datepicker value is
  // getting converted to a short date string when a user hits cancel on the picker's dialog.
  // Force the value back into a full date object (we don't care about the time)
  value = new Date(value);

  return (
    <ReactToolboxDatePicker {...input}
     active={active} autoOk={autoOk} cancelLabel={cancelLabel} className={className}
     inputClassName={inputClassName} inputFormat={inputFormat} label={label}
     maxDate={maxDate} minDate={minDate} readonly={readonly} sundayFirstDayOfWeek={sundayFirstDayOfWeek}
     value={value} />
  );
};

const DatePickerField = (props) => {
  return (
    <ReduxFormField {...props} component={DatePicker} />
  );
};

export default DatePickerField;
