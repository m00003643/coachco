import React from 'react';
import { connect } from 'react-redux';
import { AppBar, Layout, Panel, Drawer, Navigation, NavDrawer,
    List, ListSubHeader, ListDivider, ListItem, Link } from 'react-toolbox';
import { selectors as assessmentSelectors } from '../assessments/assessments-state';
import { selectors as userSelectors } from '../user/user-state';
import { selectors as clientSelectors } from '../clients/clients-state';
import { selectors as assetsSelectors } from '../assets/assets-state';
import { selectors as brandSelectors } from '../administration/branding-state';
import ListItemLink from './ListItemLink.jsx';
import AppStyles from './styles/app.scss';
import AppBarTheme from './themes/app-bar-theme.scss';
import { deauthenticateUser, GetClientReadStatus, GetCoachReadStatus } from '../utils/api';
import closest from 'dom-closest';

class Page extends React.Component {

  constructor(props) {
    super(props);

    const chatRefreshInterval = this.props.user.settings.chatPageRefreshInterval.value * 1000;

    this.state = {
      active: false,
      checkChatInterval: chatRefreshInterval,
      intervalListener: 0,
      unseenCoachChat: false,
      unseenClientsChat: false
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChatClick = this.handleChatClick.bind(this);
  }

  componentDidMount() {

    // Set the frequency to update the chat window in case the coach/client chat
    // has changed. Note: 0 means don't check

    if (this.state.checkChatInterval != 0) {
      let id = setInterval(function() { 
        this.CheckForChatActivity(); 
      }.bind(this), this.state.checkChatInterval);
      this.setState( {intervalListener: id});
    }

    // Don't wait for the first check chat interval to occur
    this.CheckForChatActivity();
  }

  componentWillUnmount() {
    window.clearInterval(this.state.intervalListener);
  }

  CheckReadStatus(data, key) {

    let ret = false;
    data.map(rec => {
      if (rec[key] == false) ret = true;
    });
    return ret;

  }

  CheckForChatActivity() {

    try {

      // Has my coach been chatting with me?
      //
      GetClientReadStatus(this.props.user.id)
      .then( data => {
        if (data.result == null)
          return false;
        else
          return this.CheckReadStatus(data.result, 'seenByClient');
      })
      .then ( coachActivity => {
        this.setState({unseenCoachChat: coachActivity});

        // If I'm a coach, check for client chat activity
        if (this.props.user.role == 'COACH') {
          GetCoachReadStatus(this.props.user.id)
          .then( data => {
            let clientsActivity = false;
            if (data == null)
              clientsActivity = false;
            else
              clientsActivity = this.CheckReadStatus(data.result, 'seenByCoach');

            if ((clientsActivity || coachActivity) && window.location.hash == '#chat')
              window.location.reload();

            this.setState({unseenClientsChat: clientsActivity});
          })
        }
      })
    } catch (err) {
      console.log(err);
    }

  }

  async handleLogout() {

    try {
      await deauthenticateUser();
      window.location = '/login';
    } catch (err) {
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'There was an unknown error while attempting to log you out.'
      });
    }
  }

  handleClick(e) {
    const el = closest(e.target, '.item_click');
    if (el === null || (el.getAttribute('data-count') != null && el.getAttribute('data-count') < 1)) return;
    if (el != null) window.location = el.getAttribute('data-payload');
  }

  handleToggle() {
    this.setState({ active: !this.state.active });
  };

  handleChatClick() {

    if (this.props.user.role == 'COACH') {
      window.location = '/clients';
    } else {
      window.location = '/chat';
    }
  }

  render () {

    const { assessments, user, clients, assets, pageMargin, brand } = this.props;

    if (this.props.user == null) {
      window.location = '/sessionexpired';
    }

    const caption = (user && user.firstname) ?
        `Welcome, ${user.firstname}!` :
        'What would you like to do?';

    const st = (user && user.role == 'CLIENT') ? AppStyles.hide_me : AppStyles.show_me;
    const roleStyle = `${st} ${AppStyles.custom_list_item}`;

    const stClient = (user && user.role == 'COACH') ? AppStyles.hide_me : AppStyles.show_me;
    const roleStyleClient = `${stClient} ${AppStyles.custom_list_item}`;

    const stHasCoach = (user && user.coachId == null) ? AppStyles.hide_me : AppStyles.show_me;
    const roleStyleHasCoach = `${stHasCoach} ${AppStyles.custom_list_item}`;

    return (
      <Layout>

        <NavDrawer permanentAt='sm' active={this.state.active} 
            onOverlayClick={this.handleToggle}>


          <List selectable ripple>

            <div style={{overflow:'auto'}}>
              <div style={{float:'left',width:'50%',paddingLeft:'20px'}}>
                { 
                  typeof this.props.brand.companyLogo != 'undefined' &&
                  <img src={this.props.brand.companyLogo} 
                    style={{width:'auto', height:'50px'}}/>
                }
              </div>
              <div style={{float:'left',width:'50%',textAlign:'right', paddingRight:'5px'}}>
                { 
                  typeof this.props.brand.profileImage != 'undefined' &&
                  <img src={this.props.brand.profileImage} 
                    style={{width:'auto', height:'50px'}}/>
                }
              </div>
            </div>

            <ListSubHeader caption={caption} />
            <div data-payload={`/clients`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='My Clients' legend='View your clients' 
                leftIcon='people' badge={clients.length} 
                className={roleStyle} />
            </div>
            <div data-payload={`/assessments`} data-count={assessments.length} 
              onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='My Assessments' legend='View/complete your assessments'
                leftIcon='description' badge={assessments.length} disabled={ (assessments.length == 0) ? true : false }
                className={AppStyles.custom_list_item} />
            </div>
            <div data-payload={`/documents`} data-count={assets.length} 
              onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='My Documents' legend='View your documents' 
                leftIcon='folder' badge={assets.length}  disabled={ (assets.length == 0) ? true : false } 
                className={AppStyles.custom_list_item}/>
            </div>
            <div data-payload={`/chat/${user.id}#chat`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='Chat with Coach' legend='View chat activity' 
                leftIcon='chat' flag={this.state.unseenCoachChat ? 'New!' : ''} 
                className={roleStyleHasCoach}
              />
            </div>
            <div data-payload={`/logs`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='My Log' legend='View your log' leftIcon='watch' 
                className={roleStyle}/>
            </div>
            <div data-payload={`/profile`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='My Profile' legend='Update your personal information' leftIcon='person pin' 
                className={AppStyles.custom_list_item}/>
            </div>
            <div data-payload={`/admin`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='Administration' legend='Assessment templates and more' leftIcon='security' 
                className={roleStyle}/>
            </div>
            <div data-payload={`/devtest`} onClick={this.handleClick} className={'item_click'}>
              <ListItemLink caption='Dev Test' legend='Dev Test' leftIcon='build pin' 
                className={AppStyles.custom_list_item}/>
            </div>
            <ListDivider />
            <ListItem caption='Logout' leftIcon='arrow_back' 
              onClick={this.handleLogout} />
          </List>

        </NavDrawer>

        <Panel scrollY>

          <AppBar 
            leftIcon='menu' 
            rightIcon={ (this.state.unseenClientsChat) ? 'chat' : null} 
            onLeftIconClick={this.handleToggle}
            onRightIconClick={this.handleChatClick}
            theme={AppBarTheme} 
            className={AppStyles.center_title}
          >
            <h1 style={{flexGrow:'1', fontSize:'1.8rem', fontWeight:'bold'}}>
              {brand.companyName}
              <span> - </span>
              <span style={{fontSize:'1.4rem', fontStyle:'italic'}}>"{brand.companyTagline}"</span>
            </h1>
          </AppBar>

          <div className={AppStyles.center_panel} style={{ flex: 1, padding: '1.8rem' }}>
            <section 
              className={
                ( window.innerWidth < 441 ) ? AppStyles.small_width_format :
                ( window.innerWidth < 1500 ) ? AppStyles.medium_width_format :
                AppStyles.large_width_format
              }>
              {this.props.children}
            </section>
          </div>

        </Panel>

      </Layout>

    );
  }
};


const mapStateToProps = (state, ownProps) => {

  let pageMargin = (typeof ownProps.pageMargin != 'undefined') ? ownProps.pageMargin : '100';

  return {
    assessments: assessmentSelectors.getAssessments(state),
    user: userSelectors.getUser(state),
    clients: clientSelectors.getClients(state),
    assets: assetsSelectors.getAssets(state),
    pageMargin: pageMargin,
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(Page);