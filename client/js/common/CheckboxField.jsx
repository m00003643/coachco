import React from 'react';
import { Field } from 'redux-form';
import { Checkbox as ReactToolboxCheckbox } from 'react-toolbox';
import Style from './styles/CheckboxField.scss';

const Checkbox = props => {

  const { input, label } = props;

  // Checkbox uses 'checked' rather than 'value' 
  let { value, ...otherInputProps } = input;

  return <ReactToolboxCheckbox {...otherInputProps}  label={label} checked={value} 
    theme={Style} />;
};

const CheckboxField = props => {
  return (
    <Field {...props} component={Checkbox} />
  );
};

export default CheckboxField;