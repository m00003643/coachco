import React from 'react';
import { browserHistory } from 'react-router';
import { ListItem, Avatar } from 'react-toolbox';

import styles from './styles/ListItemLink.scss';

export default class ListItemLink extends React.Component {
  handleClick(e) {

    // If no 'to' prop is provided then return we aren't using the 'link' functionallity.
    // Just return.
    if (typeof this.props.to === 'undefined') return;

    // if it looks like a user is trying to open the link in a new tab, then don't intercept it
    if (e.ctrlKey || e.shiftKey || e.metaKey || (e.button && e.button == 1)) {
      return;
    }

    e.preventDefault();

    if (target) { 
      window.open(this.props.to, target);
      return;
    }

    browserHistory.push(this.props.to);
  }

  renderItemContent() {
    const { caption, badge, legend, flag } = this.props;

    return (
      <span className={styles.content}>
        {caption &&
          <span className={styles.caption}>
            {caption}
            {typeof badge === 'number' &&
              <span className={styles.badge}>{badge}</span>
            }
            {flag &&
              <span className={styles.flag}>{flag}</span>
            }
          </span>
        }
        {legend && <span className={styles.legend}>{legend}</span>}
      </span>
    );
  }

  render() {
    // we handle the caption ourselves in order to also add a badge if necessary.
    // note that `itemContent` also overrides the legend (which we don't currently use)
    return <ListItem {...this.props} caption={undefined} onClick={this.handleClick.bind(this)}
        itemContent={this.renderItemContent()} />;
  }
};