import React from 'react';
import { Field } from 'redux-form';
import { ListCheckbox as ReactToolboxListCheckbox} from 'react-toolbox';
import Style from './styles/SwitchField.scss';

const ListCheckbox = props => {

  const { input, label } = props;

  // ListCheckbox uses 'checked' rather than 'value' 
  let { value, ...otherInputProps } = input;

  return <ReactToolboxListCheckbox {...otherInputProps}  label={label} checked={value} 
    theme={Style} />;
};

const ListCheckboxField = props => {
  return (
    <Field {...props} component={ListCheckbox} />
  );
};

export default ListCheckboxField;