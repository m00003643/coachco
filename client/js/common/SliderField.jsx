import React from 'react';
import { Field } from 'redux-form';
import { Slider as ReactToolboxSlider } from 'react-toolbox';
import Style from './styles/SliderField.scss';

const Slider = props => {
  const { input, ...sliderProps } = props;

  // the slider is very particular about receiving numeric types for `value`...
  let { value, ...otherInputProps } = input;
  value = value ? +value : 0;

  return <ReactToolboxSlider {...otherInputProps} {...sliderProps} value={value} 
    theme={Style} />;
};

const SliderField = props => {
  return (
    <Field {...props} component={Slider} />
  );
};

export default SliderField;
