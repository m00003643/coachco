import React from 'react';
import { Field as ReduxFormField } from 'redux-form';
import { Combobox as ReactWidgetsCombobox } from 'react-widgets';

const Combobox = ({ value, data, valueField, textField, itemComponent, disabled = false,
  readOnly = false, groupBy, groupComponent, suggest = false, filter = false,
  caseSensitive = false, minLength = 1, defaultOpen = false, busy = false,
  duration = 250, isRtl = false }) => {
  return (
    <ReactWidgetsCombobox 
      value={value} data={data} valueField={valueField} textField={textField} 
      itemComponent={itemComponent} disabled={disabled} caseSensitive={caseSensitive}
      minLength={minLength} defaultOpen={defaultOpen} busy={busy} duration={duration} isRtl={isRtl} />
  );
};

const ComboboxField = (props) => {
  return (
    <ReduxFormField {...props} component={Combobox} />
  );
};

export default ComboboxField;