import React from 'react';
import { Field as ReduxFormField } from 'redux-form';
import { Dropdown as ReactToolboxDropdown } from 'react-toolbox';

const Dropdown = ({ input, allowBlank = true, auto = true, className, disabled = false, error,
  label, source, template, required = false }) => {

  let { value } = input;
    
  return (
    <ReactToolboxDropdown {...input}
     allowBlank={allowBlank} auto={auto} className={className} disabled={disabled} error={error}
     label={label} source={source} template={template} value={value} required={required} />
  );
};

const DropdownField = (props) => {
  return (
    <ReduxFormField {...props} component={Dropdown} />
  );
};

export default DropdownField;
