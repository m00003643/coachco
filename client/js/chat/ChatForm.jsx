import React from 'react';
import { Button, Dialog, Input, Tabs, Tab} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import TextareaAutosize from 'react-autosize-textarea';
import moment from 'moment';
import { AddClientChat, UpdateClientChat, SetClientReadStatus, SetCoachReadStatus } from '../utils/api';
import Dropzone from 'react-dropzone';

class ChatForm extends React.Component {

	constructor(props) {
		super(props);

    this.state = {
      chatMessage: '',
      messageChanged: false,
      chatThread: this.props.chat,
      showDropZone: false,
      fileErrorMsg: '',
      selectedFile: '',
      commentLink: ''
    }

    this.handleChatChange = this.handleChatChange.bind(this);
    this.handleAddChat = this.handleAddChat.bind(this);
    this.postChat = this.postChat.bind(this);
    this.toggleDropZone = this.toggleDropZone.bind(this);
    this.uploadAttachment = this.uploadAttachment.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.removeAttachment = this.removeAttachment.bind(this);

  }

  handleChatChange(e) {
    const changed = e.target.value != '';
    this.setState( {chatMessage: e.target.value, messageChanged: changed} );
  }

  handleAddChat() {

    if (this.state.selectedFile) {
      this.uploadAttachment()
    } else {
      this.postChat()
    }

  }

  postChat() {

    let message = this.state.chatMessage;
    let thisMoment = moment();

    let chat = {
      commentLink: this.state.commentLink,
      commentWhen: thisMoment,
      commentMessage: message,
      commentUserType: this.props.speaker
    }

    if (this.props.speaker == 'COACH') {
      chat.coachLastSeen = thisMoment;
    } else {
      chat.clientLastSeen = thisMoment;
    }

    let chatThread = this.state.chatThread;
    chatThread.splice(0, 0, chat);

    let conversation = {
      userId: this.props.user.id,
      coachId: this.props.user.coachId,
      conversation: {
        messages: chatThread
      }
    }

    try {

      if (this.props.conversationId == -1) {
        AddClientChat(conversation)
        .then( data => {
          let id = data.result.id;
          if (this.props.speaker == 'COACH') {
            SetClientReadStatus(id, false);
          } else {
            SetCoachReadStatus(id, false);
          }
        })
      } else {
        UpdateClientChat(this.props.conversationId, conversation)
        .then( data => {
          let id = this.props.conversationId;
          if (this.props.speaker == 'COACH') {
            SetClientReadStatus(id, false);
          } else {
            SetCoachReadStatus(id, false);
          }
        })
      }

    this.setState({
      chatThread: chatThread, 
      chatMessage: '', 
      messageChanged: false,
      selectedFile: '',
      commentLink: ''} );

    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable to save chat' } );
    }

  }

  formatDate(d) {

    return moment(d).format("MMM DD, h:mma");
  }

  toggleDropZone() {
    this.setState( {showDropZone: !this.state.showDropZone} );
  }

  removeAttachment() {
    this.setState( {selectedFile: '', commentLink: ''} );
  }

  onDrop(files) {

    const fileWhiteList = ['jpg','tiff','png','bmp'];

    // Check for allowed file type
    let filePrefix = files[0].name.split('.').pop(-1);
    if (fileWhiteList.indexOf(filePrefix) < 0) {
      this.setState( {fileErrorMsg: 'The type of file chosen is not allowed'} );
      return; // not a valid file type
    }

    // Check for allowed file size
    const fileSize = +files[0].size;
    const maxSize = 1000000;
    const strMaxSize = '1 MB';
    if ( fileSize > maxSize ) {
      this.setState( {fileErrorMsg: 'File size exceeds the maximum allowed of ' + strMaxSize} );
    } else {
      this.setState( {fileErrorMsg: '', selectedFile: files[0], commentLink: '', showDropZone: !this.state.showDropZone} );
    }
  }

	uploadAttachment() {

    // Turn off yes/no dialog
    this.setState({active: !this.state.active});

    // Verify we have all needed input
		if (this.state.selectedFile == '') {
      this.setState( { fileErrorMsg: 'Required input is missing' } );
      this.handleMsgToggle();
      return;
    }

    // uploadFile will be called when we have received a valid signed request
    const uploadFile = (file, signedRequest, url) => {

      const xhr = new XMLHttpRequest();

      // Create request event handlers
      xhr.onreadystatechange = () => {

        if(xhr.readyState === 4){
          if(xhr.status === 200){
            // The file has been uploaded. Now update the database with asset meta data
            this.setState( {commentLink: url} );
            this.postChat();

          } else {
            // Handle file upload error
            this.setState( { fileErrorMsg: 'Unable to complete upload: failed to send document' } );
          }
        }
      };

      xhr.open('PUT', signedRequest);
      xhr.send(file);
    } // End uploadFile
    
    // Main function section - Make api call to get signed request url
    const file = this.state.selectedFile;
    const fileNameEncoded = encodeURIComponent(file.name);
    const fileTypeEncoded = encodeURIComponent(file.type);
    const url = `/api/sign-s3/${fileNameEncoded}/${fileTypeEncoded}`;
    const xhr = new XMLHttpRequest();

    // Create Request event handlers
    xhr.onreadystatechange = () => {

      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          // We have the signed request - now upload the file
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          // Handle get signed request error
          this.setState( {fileErrorMsg: 'Unable to complete upload: failed to get signed signature'});
        }
      }
    };

    // Call the api to get the signed request
    xhr.open('GET', url);
    xhr.send();
	}

	render() {

    const chat = this.state.chatThread;

		return (
      <div>

        <div>
          <label style={{color:'grey',float:'left',paddingTop:'12px'}} >Say something...</label>
          {!this.state.showDropZone && <Button style={{float:'right',fontSize:'12px'}} icon='attachment' 
            label='Add Attachment' onClick={this.toggleDropZone}
            disabled={(this.state.selectedFile) ? true : false}/>}
          {this.state.showDropZone && <Button style={{float:'right',fontSize:'12px'}} icon='cancel' 
            label='Cancel Attachment' onClick={this.toggleDropZone}/>}
        </div>

        <TextareaAutosize style={{width:'100%', fontSize:'14px'}} value={this.state.chatMessage} 
        onChange={this.handleChatChange} /> 

        <div className={(this.state.selectedFile) ? AppStyles.show_me : AppStyles.hide_me}>
          <label style={{fontSize:'13px', color:'grey'}}>Attachment: {this.state.selectedFile.name}</label>
          <Button label='Remove' onClick={this.removeAttachment} raised={false} 
            primary={true} style={{fontSize:'13px', marginTop:'-7px'}}/>
        </div>

        <div className={(this.state.showDropZone) ? AppStyles.show_me : AppStyles.hide_me}>
          <Dropzone className={AppStyles.drop_zone} 
            onDrop={this.onDrop} onFileDialogCancel={this.toggleDropZone} multiple={false} >
            <div className={AppStyles.drop_zone_inner} >
              Drop a file here (or click to select one from your computer). 
              Only one allowed.
            </div>
          </Dropzone>
        </div>

        <div style={{textAlign:'center',color:'red'}} >
          <p>{this.state.fileErrorMsg}</p>
        </div>

        <div style={{textAlign:'right',marginTop:'5px', marginBottom:'5px'}} >
          <Button label='Add to chat' onClick={this.handleAddChat} raised={false} 
            primary={true} disabled={!this.state.messageChanged}
            style={{fontSize:'12px', marginTop:'-7px'}}/>
        </div>

        {chat.map( (chat, index) => (
          <div style={{clear:'both'}} key={index}>
            <div style={(chat.commentUserType == 'CLIENT') ? {float:'left'} : {float:'right'} } >
              <div className={(chat.commentUserType == 'CLIENT') ? AppStyles.client_chat_block : AppStyles.coach_chat_block } >
                <div style={ ( chat.commentUserType == 'CLIENT') ? {float:'left', fontSize:'11px', color:'grey'} : {float:'right', fontSize: '11px', color:'grey'} } >
                  {(chat.commentUserType == 'CLIENT') ? 
                    this.props.clientName : this.props.coachName} {this.formatDate(chat.commentWhen)} 
                </div>
                <div style={{clear:'both'}} ></div>
                <div style={ ( chat.commentUserType == 'CLIENT') ? {float:'left', fontSize:'14px'} : {float:'right', fontSize:'14px'} }>
                  {chat.commentMessage} {(chat.commentLink) && 
                    <Button icon='attachment' href={chat.commentLink} target='_blank'/>}
                </div>
              </div>
            </div>
          </div>
        ))}

      </div>
		);
	}

}

export default ChatForm;