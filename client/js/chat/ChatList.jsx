import React from 'react';
import Page from '../common/Page.jsx';
import { List, ListItem} from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import closest from 'dom-closest';

class ChatList extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      chatlist: []
    }

  }

  componentWillMount() {

    // Fetch list of client chats for coach (params - id)
    let chatlist = [];
    chatlist.push({client: 'Tom Smith', id: 1});
    chatlist.push({client: 'Susan Anthony', id:2});
    chatlist.push({client: 'Bridget Jones', id:3});

    this.setState( { chatlist: chatlist } );

    this.handleClick = this.handleClick.bind(this);
    
  }

	handleClick(e) {
    console.log('Hey');
    const el = closest(e.target, '.item_click');
		const clientId = el.getAttribute('data-payload');
		window.location = '/chat/' + clientId;
	}

	render() {

    let chatlist = this.state.chatlist;
    console.log(chatlist);

		return (
			<Page >

        <span className={AppStyles.page_title} >
          Chats
        </span>

				<List selectable ripple>
					{chatlist.map( chat => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={chat.id} 
              onClick={this.handleClick} data-payload={chat.id}>
							<ListItem
								key={chat.id}
								caption={chat.client}
								onClick={this.handleClick}
                rightIcon='chat'
							/>
						</div>
					))}
				</List>

			</Page>
		);
	}

}

export default ChatList;