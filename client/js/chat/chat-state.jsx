import { handleActions } from 'redux-actions';
import { updateChatTimeStamp } from './chat-actions';

export const reducer = handleActions({
  // action.payload == branding info
  [updateChatTimeStamp]: (state, action) => action.payload
}, null);

export const stateKey = 'chatTimeStamp';

export const selectors = {
  getChatTimeStamp: state => state[stateKey]
};