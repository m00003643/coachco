import React from 'react';
import Page from '../common/Page.jsx';
import ChatForm from './ChatForm.jsx';
import { selectors as userSelectors } from '../profile/profile-state';
import { GetClientChat, GetUser, UpdateClientChat } from '../utils/api';

class ChatPageClient extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      chat: [],
      clientName: '',
      coachName: '',
      conversationId: -99,
    }

  }

  componentDidMount() {

    try {
      GetClientChat(this.props.user.id)
      .then( data => {
        let chat = data.result.conversation.messages;
        let clientName = this.props.user.firstname + ' ' + this.props.user.lastname;
        let id = data.result.id;

        this.setState( { 
          chat: chat, 
          clientName: clientName,
          conversationId: id 
        } );
        return this.props.user.coachId
      })
      .then( coachId => {
        // We need the user's coach's name
        GetUser(coachId)
        .then( (data) => {
          const coachName = data.result.firstname + ' ' + data.result.lastname;
          this.setState( {coachName: coachName} );
          return true;
        })
      })
      .then( data => {
        // Update lastSeen for this chat
        let conversation = {
          userId: this.props.user.id,
          coachId: this.props.user.coachId,
          conversation: {
            messages: this.state.chat,
            seenByCoach: true
          }
        }
        if (this.state.conversationId != -1) {
          UpdateClientChat(this.state.conversationId, conversation)
        }
      })
    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable retrieve chat' } );
    }

  }

	render() {

		return (
      <div>
        {(this.state.chat.length > 0 || this.state.conversationId == -1) && 
          <ChatForm chat={this.state.chat} clientName={this.state.clientName} 
            coachName={this.state.coachName} role={this.props.user.role} 
            conversationId={this.state.conversationId} speaker='COACH'
            user={this.props.user} />
        }
      </div>
		);
	}

}

export default ChatPageClient;
