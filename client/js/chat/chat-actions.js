import { createAction } from 'redux-actions';

export const updateChatTimeStamp = createAction('UPDATE_CHAT_TIMESTAMP', chat => chat);