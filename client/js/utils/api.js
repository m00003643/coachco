import request from './request';

//
// assessment operations
//

export async function updateAssessment(id, answers) {
  const { response, json } = await request(`/api/assessments/${id}`).patch(answers);
  return formatResult(response, json);
}

export async function assignAssessment(assessment) {
  const { response, json } = await request(`/api/assessments`).post(assessment);
  return formatResult(response, json);
}

export async function deleteAssignedAssesssment(id) {
  const { response, json } = await request(`/api/assessments/${id}`).delete();
  return formatResult(response, json);
}

export async function getAssessment(id) {
  const { response, json } = await request(`/api/assessments/${id}`).get();
  return formatResult(response, json);
}

export async function getSharedAssessments(id) {
  const { response, json } = await request(`/api/assessments/sharedwithuser/${id}`).get();
  return formatResult(response, json);
}

//
// assessment template operations
//

export async function getCoachAssessmentTemplates(coachId) {
  const { response, json } = await request(`/api/assessmenttemplates/coach/${coachId}`).get();
  return formatResult(response, json);
}

export async function updateAssessmentTemplate(tempateId, data) {
  const { response, json } = await request(`/api/assessmenttemplates/${tempateId}`).patch(data);
  return formatResult(response, json);
}

export async function addAssessmentTemplate(data) {
  const { response, json } = await request(`/api/assessmenttemplates`).post(data);
  return formatResult(response, json);
}

export async function deleteAssessmentTemplate(id) {
  const { response, json } = await request(`/api/assessmenttemplates/${id}`).delete();
  return formatResult(response, json);
}

//
// assigned asset operations
//
export async function assignAsset(asset) {
  const { response, json } = await request(`/api/assignedassets`).post(asset);
  return formatResult(response, json);
}

export async function deleteAssignedAsset(id) {
  const { response, json } = await request(`/api/assignedassets/${id}`).delete();
  return formatResult(response, json);
}

//
// asset default operations
//

export async function getCoachDefaultAssets(coachId) {
  const { response, json } = await request(`/api/assets/user/${coachId}`).get();
  return formatResult(response, json);
}

export async function deleteAsset(id) {
  const { response, json } = await request(`/api/assets/${id}`).delete();
  return formatResult(response, json);
}

export async function addAssetMetaData(data) {
  const { response, json } = await request(`/api/assets`).post(data);
  return formatResult(response, json);
}

export async function updateAssetMetaData(id, data) {
  const { response, json } = await request(`/api/assets/${id}`).patch(data);
  return formatResult(response, json);
}

export async function getClientSharedAsset(id) {
  const { response, json } = await request(`/api/assets/user/${id}`).get();

  if (json.errorCode == 'NOT_FOUND') {
    return {
      result: []
    }
  }
  
  return formatResult(response, json);
}

export async function addSharedAsset(data) {
  const { response, json } = await request(`/api/assets`).post(data);
  return formatResult(response, json);
}

export async function deleteSharedAsset(id) {
  const { response, json } = await request(`/api/assets/${id}`).delete();
  return formatResult(response, json);
}

//
// log operations
//
export async function updateLog(id, data) {

  // API doesn't like empty values - force to null
  Object.keys(data).map( key => { if (data[key] === '') data[key] = null; });

  const { response, json } = await request(`/api/clientlog/${id}`).patch(data);
  return formatResult(response, json);
}

export async function addLog(data) {

  // API doesn't like empty values - force to null
  Object.keys(data).map( key => { if (data[key] === '') data[key] = null; });
  
  const { response, json } = await request(`/api/clientlog`).post(data);
  return formatResult(response, json);
}

export async function deleteLog(id) {

  const { response, json } = await request(`/api/clientlog/${id}`).delete();
  return formatResult(response, json);
}

export async function exportLog(data) {

  // data must be a valid js object. It should be structred as an array of
  // valid json objects.
  
  var opts = {
    method: 'POST',
    credentials: 'same-origin',
    headers: { 'Content-Type': 'application/json' }
  };
  opts.body = JSON.stringify(data); 

  try {
    return await fetch('/api/clientlog/export', opts);
  }
  catch(err) {
    throw 'Error exporting log';
  }
}

//
// user operations
//

export async function getAuthenticatedUser(username, password) {
  const { response, json } = await request('/api/login').post({ username, password });
  return formatResult(response, json);
}

export async function deauthenticateUser() {
  const { response, json } = await request('/api/logout').get();
  return formatResult(response, json);
}

export async function updateUser(id, data) {

  // Strip unecessary data from payload
  delete data.id;
  delete data.password;
  delete data.created;
  delete data.updated;

  // API doesn't like empty values - force to null
  Object.keys(data).map( key => { if (data[key] === '') data[key] = null; });

  const { response, json } = await request(`/api/users/${id}`).patch(data);
  return formatResult(response, json);
}

export async function addUser(data) {

  // API doesn't like empty values - force to null
  Object.keys(data).map( key => { if (data[key] === '') data[key] = null; });
  
  const { response, json } = await request(`/api/users`).post(data);
  return formatResult(response, json);
}

export async function VerifyInvite(data) {

  const { response, json } = await request(`/api/verifyinvite`).post(data);
  return formatResult(response, json);
}

export async function deleteUser(id) {

  const { response, json } = await request(`/api/users/${id}`).delete();
  return formatResult(response, json);
}

export async function GetUser(id) {
  const { response, json } = await request(`/api/users/${id}`).get();
  return formatResult(response, json);
}

//
// Branding operations
//
export async function ResetBrandingEmail(id, emailKey) {
  const { response, json } = await request(`/api/users/resetbrandingtemplate/${id}/${emailKey}`).post();
  console.log(response);
  console.log(json);
  return formatResult(response, json);
}

//
// Chat operations
//
export async function GetClientChat(id) {
  const { response, json } = await request(`/api/conversations/user/${id}`).get();

  if (json.errorCode == 'NOT_FOUND') {
    return {
      result: {
        conversation: {
          messages: []
        },
        id: -1
      }
    }
  }
  return formatResult(response, json);
}

export async function GetClientChatTimeStamp(id) {

  const { response, json } = await request(`/api/conversations/usertimestamp/${id}`).get();

  if (json.errorCode == 'NOT_FOUND') {
    return {
      result: null
    }
  }
  return formatResult(response, json);
  
}

export async function GetCoachChatTimeStamp(id) {

  const { response, json } = await request(`/api/conversations/coachtimestamp/${id}`).get();

  if (json.errorCode == 'NOT_FOUND') {
    return {
      result: null
    }
  }
  return formatResult(response, json);
  
}

export async function GetClientReadStatus(id) {

  const { response, json } = await request(`/api/conversations/getindicators/CLIENT/${id}`).get();
  return formatResult(response, json);
  
}

export async function GetCoachReadStatus(id) {

  const { response, json } = await request(`/api/conversations/getindicators/COACH/${id}`).get();
  return formatResult(response, json);
  
}

export async function SetClientReadStatus(id, value) {

  const { response, json } = await request(`/api/conversations/setindicators/seenByClient/${value}/${id}`).patch();
  return formatResult(response, json);
  
}

export async function SetCoachReadStatus(id, value) {

  const { response, json } = await request(`/api/conversations/setindicators/seenByCoach/${value}/${id}`).patch();
  return formatResult(response, json);
  
}

export async function AddClientChat(conversation) {
  const { response, json } = await request(`/api/conversations`).post(conversation);
  return formatResult(response, json);
}

export async function UpdateClientChat(id, conversation) {
  const { response, json } = await request(`/api/conversations/${id}`).patch(conversation);
  return formatResult(response, json);
}

//
// local helpers
//
// this helper "understands" the standard JSON envelope from the server, and sends back
// a flattened object in the form of:
// { response, body, success[, result][, errorMessage][, errorCode][, validationErrors] }
function formatResult(response, json) {
  const ret = { response, body: json };

  if (json) {
    ret.success = json.success;

    if (typeof json.result !== 'undefined') {
      ret.result = json.result;
    }

    if (!json.success) {
      const err = new Error('Error while making request to API server');
      err.errorMessage = json.errorMessage;
      err.errorCode = json.errorCode;
      err.response = response;
      err.json = json;

      if (json.validationErrors) {
        err.validationErrors = json.validationErrors;
      }

      throw err;
    }
  } else {
    const err = new Error('Error while making request to API server');
    err.response = response;
    throw err;
  }

  return ret;
}
