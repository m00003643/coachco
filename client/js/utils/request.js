/**
 * A simple request utility built on top of fetch(). It assumes we're dealing with JSON-based
 * endpoints and request bodies.
 *
 * Quick examples:
 *   request('/some/endpoint').get()
 *   request('/some/endpoint').query({ foo: 'bar' }).get()
 *   request('/some/endpoint').post({ hello: 'world' })
 *
 * All methods named after HTTP verbs (e.g., `get`, `post`, `put`, etc.) return a Promise,
 * where the completion value is an object containing both the raw response as well as
 * the parsed JSON response body, if there is one.
 **/
class Request {
  constructor(url) {
    if (typeof url !== 'string') {
      throw new Error('url is required');
    }

    this.url = url;
    this._query = {};
  }

  get() {
    return this._doRequest(buildOpts()).then(prepareResponse);
  }

  patch(body) {
    const opts = buildOpts({
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' }
    });

    if (body) {
      opts.body = JSON.stringify(body);
    }

    return this._doRequest(opts).then(prepareResponse);
  }

  post(body) {
    const opts = buildOpts({
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }
    });

    if (body) {
      opts.body = JSON.stringify(body);
    }

    return this._doRequest(opts).then(prepareResponse);
  }

  put(body) {
    const opts = buildOpts({
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' }
    });

    if (body) {
      opts.body = JSON.stringify(body);
    }

    return this._doRequest(opts).then(prepareResponse);
  }

  delete() {
    const opts = buildOpts({ method: 'DELETE' });

    return this._doRequest(opts).then(prepareResponse);
  }

  query(obj) {
    this._query = Object.assign(this._query, obj);

    return this;
  }

  _doRequest(opts) {
    return fetch(this._getUrlWithQueryString(), opts);
  }

  _getUrlWithQueryString() {
    const query = this._query;

    const params = Object.keys(query).map(key => {
      return `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`;
    });

    return this.url + (params.length ? '?' + params.join('&') : '');
  }
}

const defaultOpts = {
  method: 'GET',
  credentials: 'same-origin'
};

const defaultHeaders = { Accept: 'application/json' };

function buildOpts(opts = {}) {
  const ret = Object.assign({}, defaultOpts, opts);

  // one-off "deep merging" for headers
  ret.headers = Object.assign({}, defaultHeaders, opts.headers);

  return ret;
}

function prepareResponse(response) {

  const contentType = response.headers.get('content-type');

  if (contentType && contentType.indexOf('application/json') !== -1) {
    return response.json().then(json => ({ response, json }));
  }

  return Promise.resolve({ response, json: null });
}

export default url => new Request(url);
