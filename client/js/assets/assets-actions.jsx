import { createAction } from 'redux-actions';

export const updateAssets = createAction('UPDATE_ASSETS', defaultAssets => defaultAssets);
