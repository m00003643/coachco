import React from 'react';
import { Button, List, ListItem, Dialog, Input } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';
import closest from 'dom-closest';
import { deleteAssignedAsset, getClientSharedAsset, addSharedAsset,
  deleteSharedAsset } from '../utils/api';
import Dropzone from 'react-dropzone';

class AssetsList extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			showVerify: false,
			showVerify2: false,
			selectedId: -1,
			selectedId2: -1,
      message: '',
      clientShares: [],
      showUploadDialog: false,
      selectedFile: '',
      fileErrorMsg: '',
      sharedFile: '',
      docName: ''
		}

    this.handleDeleteAsset = this.handleDeleteAsset.bind(this);
    this.handleDeleteAsset2 = this.handleDeleteAsset2.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleToggle2 = this.handleToggle2.bind(this);
    this.handleToggle3 = this.handleToggle3.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleYes2 = this.handleYes2.bind(this);
    this.handleYes3 = this.handleYes3.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.onDrop = this.onDrop.bind(this);

	}

  componentWillMount() {

    getClientSharedAsset(this.props.userId)
      .then(data => {
        let shares = [];
        data.result.map(shared => {
          if (shared.assetType == 'C') {
            shares.push({
              name: shared.assetName,
              url: shared.assetUrl,
              id: shared.id
            })
          }
        })
        this.setState( {clientShares: shares} );
      },
      err => {
        console.log(err);
        this.setState( {message: 'Failed to fetch shared assets'} );
      }
    );

  }

  onDrop(files) {

    const fileWhiteList = ['jpg','tiff','png','bmp','pdf','txt','doc','docx','xls','xlsx','key','ppt','pptx'];

    // Check for allowed file type
    let filePrefix = files[0].name.split('.').pop(-1);
    if (fileWhiteList.indexOf(filePrefix) < 0) {
      this.setState( {fileErrorMsg: 'The type of file chosen is not allowed'} );
      return; // not a valid file type
    }

    // Check for allowed file size
    const fileSize = +files[0].size;
    const maxSize = 1000000;
    const strMaxSize = '1 MB';
    if ( fileSize > maxSize ) {
      this.setState( {fileErrorMsg: 'File size exceeds the maximum allowed of ' + strMaxSize} );
      return; // not a valid file type
    }

    this.setState( {fileErrorMsg: '', selectedFile: files[0], 
      showUploadDialog: true, message: ''} );
  }

  getFileType(fname) {
    let fileExt = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();

    let picType = ['jpg','png','bmp','tiff'].includes(fileExt);
    let pdfType = ['pdf'].includes(fileExt);

    return ( picType ? 'photo' : pdfType ? 'picture_as_pdf' : 'attachment' );
  }

  handleClick(e) {
    const el = closest(e.target, '.item_click');
    if (el != null) window.open(el.getAttribute('data-payload'), '_blank');
  }

	handleDeleteAsset(e) {
		const assignedId = e.target.getAttribute('data-assigned_id');
    this.setState({showVerify: !this.state.showVerify, selectedId: assignedId});
	}

	handleDeleteAsset2(e) {
		const sharedId = e.target.getAttribute('data-assigned_id');
    this.setState({showVerify2: !this.state.showVerify2, selectedId2: sharedId});
	}

	handleToggle() {
    this.setState({showVerify: !this.state.showVerify, selectedId: -1, message: ''});
	}

	handleToggle2() {
    this.setState({showVerify2: !this.state.showVerify2, selectedId2: -1, message: ''});
	}

	handleToggle3() {
    this.setState({showUploadDialog: !this.state.showUploadDialog, message: ''});
	}

	handleYes() {

    this.setState({showVerify: !this.state.showVerify, message: ''});
		if (this.state.selectedId < 0) return;

    try {
      deleteAssignedAsset(this.state.selectedId)
        .then(data => {window.location = '/documents';})
    }
    catch(err) {
      console.log(err);
      this.setState( {message: 'Failed to remove assigned document'} );
    }

	}

	handleYes2() {

    this.setState({showVerify2: !this.state.showVerify2, message: ''});
		if (this.state.selectedId2 < 0) return;

    try {
      deleteSharedAsset(this.state.selectedId2)
        .then(data => {window.location = '/documents';})
    }
    catch(err) {
      console.log(err);
      this.setState( {message: 'Failed to unshare document'} );
    }

	}

  handleChange(name, value) {
    this.setState( {docName: value} );
  };

  async handleUpload() {

    let asset = {
      userId: this.props.userId,
      assetType: 'C',
      assetName: this.state.docName,
      assetUrl: this.state.sharedFile
    }

    try {
      await addSharedAsset(asset)
      .then( data => {
        window.location = '/documents';
      });

    } catch (err) {
      console.log(err);
      this.setState( { message: 'Unable to add shared document info' } );
    }
    
  }

	handleYes3() {

    // Turn off yes/no dialog
    this.setState({showUploadDialog: !this.state.showUploadDialog});

    // Verify we have all needed input
		if (this.state.selectedFile == '' || this.state.docName == '') {
      this.setState( { message: 'Required input is missing' } );
      this.handleMsgToggle3();
      return;
    }

    // uploadFile will be called when we have received a valid signed request
    const uploadFile = (file, signedRequest, url) => {

      const xhr = new XMLHttpRequest();

      // Create request event handlers
      xhr.onreadystatechange = () => {

        if(xhr.readyState === 4){
          if(xhr.status === 200){
            // The file has been uploaded. Now update the database with asset meta data
            this.setState( {sharedFile: url} );
            this.handleUpload();

          } else {
            // Handle file upload error
            this.setState( { message: 'Unable to complete upload: failed to send document' } );
            this.handleMsgToggle3();
          }
        }
      };

      xhr.open('PUT', signedRequest);
      xhr.send(file);
    } // End uploadFile
    
    // Main function section - Make api call to get signed request url
    const file = this.state.selectedFile;
    const fileNameEncoded = encodeURIComponent(file.name);
    const fileTypeEncoded = encodeURIComponent(file.type);
    const url = `/api/sign-s3/${fileNameEncoded}/${fileTypeEncoded}`;
    const xhr = new XMLHttpRequest();

    // Create Request event handlers
    xhr.onreadystatechange = () => {

      if(xhr.readyState === 4){
        if(xhr.status === 200){
          const response = JSON.parse(xhr.responseText);
          // We have the signed request - now upload the file
          uploadFile(file, response.signedRequest, response.url);
        }
        else{
          // Handle get signed request error
          this.setState( {message: 'Unable to complete upload: failed to get signed signature'});
          this.handleMsgToggle();
        }
      }
    };

    // Call the api to get the signed request
    xhr.open('GET', url);
    xhr.send();

	}

  render() {

    const { assets } = this.props;

    return (
      <div>
        <List selectable ripple>
          {assets.map(asset => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={asset.assetId} 
              onClick={this.handleClick} data-payload={asset.assetUrl}>
              <ListItem
                key={asset.assetId}
                caption={asset.assetName}
                leftIcon={ this.getFileType(asset.assetUrl) }
                rightActions={[
                  <div key={asset.assetId} onClick={e => e.stopPropagation()}>
                    <Button label='Delete' name={`delAsset${asset.assetId}`} icon='cancel' 
                    onMouseUp={this.handleDeleteAsset} key={asset.assetId} data-assigned_id={asset.id}/>
                  </div>
                ]}
            />
           </div>
          ))}
        </List>

        <div style={{textAlign:'center',fontSize:'11px'}} >
          <label>Currently shared with coach</label>
        </div>

        <List selectable ripple>
          {this.state.clientShares.map( (share, index) => (
            <div className={AppStyles.custom_list_item + ' item_click'} key={share.id} 
              onClick={this.handleClick} data-payload={share.url}>
              <ListItem
                key={share.id}
                caption={share.name}
                leftIcon={ this.getFileType(share.url) }
                rightActions={[
                  <div key={index} onClick={e => e.stopPropagation()}>
                    <Button label='Delete' name={`delAsset${share.id}`} icon='cancel' 
                    onMouseUp={this.handleDeleteAsset2} key={share.id} data-assigned_id={share.id}/>
                  </div>
                ]}
            />
           </div>
          ))}
        </List>

        <p style={{textAlign:'center'}} >Upload a file to share. Use the drop zone below.</p>

        <div style={{marginTop: '40px'}} >
          <Dropzone className={AppStyles.drop_zone} onDrop={this.onDrop} multiple={false} 
            style={{height:'auto'}} >

            { 
              <div className={AppStyles.drop_zone_inner} >
                Drop a file here (or click to select one from your computer). 
              </div>
            }
          </Dropzone>
        </div>

        <div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

				<div>
					<Dialog 
						active={this.state.showVerify}
						onEscKeyDown={this.handleToggle}
						onOverlayClick={this.handleToggle}
						title='Confirm Delete' 
						actions={[
							{ label: "No", onClick: this.handleToggle },
							{ label: "Yes", onClick: this.handleYes }
						]} >

						<div>
							Are you sure you want to remove this from your list of available documents?
						</div>

					</Dialog>
				</div>

				<div>
					<Dialog 
						active={this.state.showVerify2}
						onEscKeyDown={this.handleToggle2}
						onOverlayClick={this.handleToggle2}
						title='Confirm Unshare' 
						actions={[
							{ label: "No", onClick: this.handleToggle2 },
							{ label: "Yes", onClick: this.handleYes2 }
						]} >

						<div>
							Are you sure you want to unshare this document?
						</div>

					</Dialog>
				</div>

        <div>
          <Dialog 
            active={this.state.showUploadDialog}
            onEscKeyDown={this.handleToggle3}
            onOverlayClick={this.handleToggle3}
            title='Confirm Upload' 
            actions={[
              { label: "Cancel", onClick: this.handleToggle3 },
              { label: "Ok", onClick: this.handleYes3 }
            ]} >

            <div>
              <p>Enter a name for this shared document and click Ok</p>
              <Input name="doc_name" label="Document Name" required={true} 
                value={this.state.docName} onChange={this.handleChange.bind(this, 'docName')} />
            </div>

          </Dialog>
        </div>
        <div style={{textAlign:'center',color:'red'}} >
          <p>{this.state.fileErrorMsg}</p>
        </div>
        
      </div>
    )
  }
}

export default AssetsList;