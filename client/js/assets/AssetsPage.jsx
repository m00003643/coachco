import React from 'react';
import { connect } from 'react-redux';
import AssetsList from './AssetsList.jsx';
import { Panel, AppBar} from 'react-toolbox';
import Page from '../common/Page.jsx';
import { selectors as assetSelectors } from './assets-state';
import { selectors as userSelectors } from '../user/user-state';
import AppStyles from '../common/styles/app.scss';

class AssetsPage extends React.Component {

	render() {

		const { assets, user } = this.props;

		return (
			<Page >
				<span className={AppStyles.page_title}>My Documents</span>
				<AssetsList assets={assets} userId={user.id} />
	    </Page >
		);
	}

}

const mapStateToProps = function(state, ownProps) {

	return {
    user: userSelectors.getUser(state),
		assets: assetSelectors.getAssets(state)
	};
};

// Returns a higer order component that wraps this component
export default connect(mapStateToProps)(AssetsPage);
