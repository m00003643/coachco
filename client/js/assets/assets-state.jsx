import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';

export const reducer = handleActions({}, 
  buildMapFromAssetsArray(window.INITIAL_STATE.assets));

export const stateKey = 'assets';

export const selectors = {
  getAssets: createSelector(
    state => state[stateKey],
    assetsMap => Object.values(assetsMap)
  ),
  getAsset: (state, id) => {
  	return state[stateKey][id] || null
  }
};

function buildMapFromAssetsArray(assets) {

	const ret = {};

	if (!assets) return ret;
	assets.forEach(asset => ret[asset.assetId] = asset);
	return ret;

}
