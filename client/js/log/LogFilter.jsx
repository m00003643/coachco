import React from 'react';
import { Button, Dropdown, DatePicker } from 'react-toolbox';
import AppStyles from '../common/styles/app.scss';


// @todo: might make sense to move these to a more reusable location.
const THEME_OPTIONS = [
  { value: 'PERSONAL', label: 'Personal' },
  { value: 'PROFESSIONAL', label: 'Professional' },
  { value: 'PERFORMANCE', label: 'Performance' },
  { value: 'BUSINESS', label: 'Business' },
  { value: 'RELATIONSHIP', label: 'Relationship' },
  { value: 'MARRIAGE', label: 'Marriage' },
  { value: 'TRANSITION', label: 'Transition' },
  { value: 'OTHER', label: 'Other' }
];

const SORT_OPTIONS = [
  { value: 'lastname', label: 'Last Name'},
  { value: 'theme', label: 'Theme'},
  { value: 'logDate', label: 'Log Date'}
];

class LogFilter extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      filters: {...props.currentFilters}
    };

    this.handleThemeChange = this.handleThemeChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSetFilters = this.handleSetFilters.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleSort1Change = this.handleSort1Change.bind(this);
    this.handleSort2Change = this.handleSort2Change.bind(this);

  }

  handleThemeChange(val) {
    this.setState({
      filters: { ...this.state.filters, theme: val }
    });
  }

  handleNameChange(val) {
    this.setState({
      filters: { ...this.state.filters, name: val }
    });
  }
  
  handleStartDateChange(val) {
    this.setState({
      filters: { ...this.state.filters, startDate: val }
    });
  }

  handleEndDateChange(val) {
    this.setState({
      filters: { ...this.state.filters, stopDate: val }
    });
  }

  handleSort1Change(val) {
    this.setState({
      filters: { ...this.state.filters, sortKey1: val }
    });
  }

  handleSort2Change(val) {
    this.setState({
      filters: { ...this.state.filters, sortKey2: val }
    });
  }

  handleSetFilters() {
    this.props.onChange(this.state.filters);
  }

  render() {
    const { onCancel, nameOptions } = this.props,
          { filters } = this.state;

    return (
      <div>

        <div>
					<span className={AppStyles.page_title}>Filter Log...</span>
          <div>
            <div style={{float: 'left', marginRight: '15px'}} >
              <Dropdown
                onChange={this.handleNameChange}
                source={nameOptions}
                value={filters.name}
                label='by Name...' />
              </div>
            <div style={{float: 'left'}} >
              <Dropdown
                onChange={this.handleThemeChange}
                source={THEME_OPTIONS}
                value={filters.theme}
                label='by Theme...' />
            </div>
          </div>

          <div style={{clear: 'both'}} >
            <div style={{float: 'left', marginRight: '15px'}} >
              <DatePicker
                onChange={this.handleStartDateChange}
                value={filters.startDate}
                inputFormat={(value) => `${value.getMonth() + 1}/${value.getDate()}/${value.getFullYear()}`}
                label='from Date...' />
            </div>
            <div style={{float: 'left'}} >
              <DatePicker
                onChange={this.handleEndDateChange}
                value={filters.stopDate}
                inputFormat={(value) => `${value.getMonth() + 1}/${value.getDate()}/${value.getFullYear()}`}
                label='to Date...' />
            </div>
          </div>
        </div>

        <div style={{clear: 'both'}}>
					<span className={AppStyles.page_title}>Sort Log...</span>
          <div>
            <div style={{float: 'left', marginRight: '15px'}} >
              <Dropdown
                onChange={this.handleSort1Change}
                source={SORT_OPTIONS}
                value={filters.sortKey1}
                label='by field...' />
              </div>
          </div>
          <div style={{float: 'left'}} >
            <Dropdown
              onChange={this.handleSort2Change}
              source={SORT_OPTIONS}
              value={filters.sortKey2}
              label='then by field...' />
          </div>
        </div>

        <div style={{clear: 'both'}} >
          <div style={{float: 'right'}} >
            <Button label='Cancel' onClick={onCancel} raised={true} 
              style={{marginRight: '15px'}} />
            <Button label={`Set`} onClick={this.handleSetFilters} raised={true} />
          </div>
        </div>

      </div>
    );
  };

};

export default LogFilter;
