import React from 'react';
import { connect } from 'react-redux';
import { Panel, List, ListItem, Button } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as clientSelectors } from '../clients/clients-state';
import {browserHistory} from 'react-router';
import ButtonTheme from '../common/styles/BackButton.scss';

class LogAddPage extends React.Component {

  handleOnClick() {
    window.location = '/log/add/' + this.clientid + '#0';
  }

	render() {
		
		const { clients } = this.props;

		return (
			<Page >
				<section>

          <div>
            <span className={AppStyles.page_title} >
              <Button icon='arrow_back' 
                href={'/logs'} 
                theme={ButtonTheme} ripple={false} />
              Add Log Entry for:
            </span>
          </div>
          
          <List selectable ripple>
            {clients.map(client => (
              <ListItem
                key={client.id}
                caption={`${client.firstname} ${client.lastname}`}
                onClick={this.handleOnClick}
                clientid={client.id}
              />
            ))}
          </List>

				</section>
			</Page>
		);
	}

}

const mapStateToProps = function(state) {

	return {
		clients: clientSelectors.getClients(state)
	};
};

export default connect(mapStateToProps)(LogAddPage);