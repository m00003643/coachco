import React from 'react';
import { Button, List, ListItem, Dialog } from 'react-toolbox';
import Field from '../common/Field.jsx';
import AppStyles from '../common/styles/app.scss';
import LogEntry from './LogEntry.jsx';
import LogFilter from './LogFilter.jsx';
import {browserHistory} from 'react-router';
import { connect } from 'react-redux';
import { selectors as logSelectors } from './log-state';
import { updateFilters } from './log-actions';
import { selectors as clientSelectors } from '../clients/clients-state';
import { exportLog } from '../utils/api';
import moment from 'moment';

class LogList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      filterDialogActive: false,
      total_hours: 99
    };

    this.toggleFilterDialog = this.toggleFilterDialog.bind(this);
    this.updateFilters = this.updateFilters.bind(this);
    this.removeFilters = this.removeFilters.bind(this);
    this.handlePrint = this.handlePrint.bind(this);
    this.handleExport = this.handleExport.bind(this);
  }

  getLogHourTotals(logs) {
    
    let ret = logs.reduce( (total, log) => {
      return total + +log.logHours;
    }, 0);

    return ret;
  }

  handlePrint() {

  }

  handleExport() {

    try {

      // Create an array of log objects to feed to the export

      let exportData = [];
      let logs = (this.props.call_origin == 'my_logs') ? this.props.filteredLog : this.props.clientLog;
      logs.map( log => {
        let l = {};
        l.LastName = log.lastname;
        l.FirstName = log.firstname;
        l.Date = moment(moment.utc(log.logDate).toDate()).format('MM/DD/YYYY');
        l.Time = moment(moment.utc(log.logTime).toDate()).format('HH:mm');
        l.Hours = log.logHours;
        l.Paid = (log.paid) ? 'Yes' : 'No';
        l.Theme = log.theme
        l.GroupName = log.groupName;
        l.GroupCount = log.groupCount;
        l.Description = log.description;
        exportData.push(l);
      });
      console.log(exportData);

      // Perform the export
      exportLog(exportData)
        .then(
          response => response.blob())
        .then(blob => {
            var url = window.URL.createObjectURL(blob);
            var link = document.createElement('a');
            link.href = url;
            //link.download = "log_export.csv";
            link.click();
          })
        .catch(error => {
            throw error.message;
          });
    } catch (err) {
      console.log(err);      
    }

  }

  toggleFilterDialog() {
    this.setState({ filterDialogActive: !this.state.filterDialogActive });
  }

  updateFilters(filters) {

    this.setState({ filterDialogActive: false });
    this.props.dispatch(updateFilters(filters));
  }

  removeFilters() {
    this.updateFilters({});
  }

  createNewLogEntry() {
    let url = (this.props.call_origin == 'my_logs') ? '/log/add#0' : `/log/add/${this.props.clientId}#1`;
    window.location = url;
  }

  render() {

    const { filteredLog, filters, call_origin, clientId, clientLog, nameOptions } = this.props,
          { filterDialogActive } = this.state;

    let logs = (call_origin == 'my_logs') ? filteredLog : clientLog;

    return (

			<div>

        <div >

          <div className={(call_origin == 'my_logs') ? AppStyles.show_me : AppStyles.hide_me} >
            <span className={AppStyles.page_title}>My Log</span>
          </div>

          <div style={{float: 'left', marginTop: '0px'}}
            className={(call_origin == 'my_logs') ? AppStyles.show_me : AppStyles.hide_me} >
            
            <Button label={(window.innerWidth < 441) ? 'Filter' : `Filter & Sort`} icon='filter_list' 
              onClick={this.toggleFilterDialog} raised={true} flat primary />
            <Button label='Remove Filters' icon='cancel' 
              onClick={this.removeFilters} raised={true} flat primary />

          </div>

          <div style={{clear: 'both'}} >
            <Button label={(window.innerWidth < 441) ? 'Add' : 'Add Log'} icon='add' 
              onClick={this.createNewLogEntry.bind(this)} raised={true} flat primary 
              style={{float:'left'}} />
            <Button label={(window.innerWidth < 441) ? '' : 'Export'} icon='grid_on' 
              onClick={this.handleExport} raised={true} flat primary 
              className={(logs.length == 0) ? AppStyles.hide_me : AppStyles.show_me} 
              style={{float:'left'}}/>
            <Button label={(window.innerWidth < 441) ? '' : 'Print'} icon='print' 
              onClick={this.handlePrint} raised={true} flat primary 
              className={(logs.length == 0) ? AppStyles.hide_me : AppStyles.show_me} 
              style={{float:'left'}}/>
          </div>

        </div>

        <div style={{clear:'both'}} ></div>

        <div style={{textAlign: 'center', paddingTop: '15px'}} 
          className={(logs.length == 0) ? AppStyles.hide_me : AppStyles.show_me} >
          <label>Total Hours Spent: {this.getLogHourTotals((call_origin == 'my_logs') ? filteredLog : clientLog)}</label>
        </div>

        <div style={{clear: 'both'}} >
          <ul style={{marginTop: '10px'}} >
            {logs.map( (logEntry) => (
              <li key={logEntry.id} >
                <LogEntry logEntry={logEntry} call_origin={call_origin} />
              </li>
            ))}
          </ul>
        </div>

        {filterDialogActive &&
          <Dialog active={true}
              onEscKeyDown={this.toggleFilterDialog}
              onOverlayClick={this.toggleFilterDialog} >
            <LogFilter
              nameOptions={nameOptions}
              currentFilters={filters}
              onCancel={this.toggleFilterDialog}
              onChange={this.updateFilters} />
          </Dialog>
        }

      </div>
    );
  };
};

const mapStateToProps = state => {

  // Build nameOptions prop. Needed by log filter Component
  let nameOptions = [];
  clientSelectors.getClients(state).map( client => {
    nameOptions.push( { 'value': client.firstname + ' ' + client.lastname, 'label': client.firstname + ' ' + client.lastname} );
  });

  return {
    filteredLog: logSelectors.getFilteredLogsSorted(state),
    filters: logSelectors.getFilters(state),
    nameOptions: nameOptions
  };
};

export default connect(mapStateToProps)(LogList);
