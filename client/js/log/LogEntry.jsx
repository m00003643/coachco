import React from 'react';
import AppStyles from '../common/styles/app.scss';

class LogEntry extends React.Component {

  getDateTime(logEntry) {
    let dDate = new Date(logEntry.logDate);
    return dDate.toLocaleDateString() + ' |';
  }
  
  render() {

    const { logEntry, call_origin } = this.props;

    // If we got here from My Logs set the hash to 0 otherwise set it to one.
    // This hash will help control routing when using the apps 'back' key.
    let hash = (call_origin == 'my_logs') ? '#0' : '#1';

    return (

      <div className={AppStyles.log_entry} >

        <a href={`/log/${logEntry.id}${hash}`} >
          <div style={{ float: 'left' }} >
            <span className={AppStyles.log_entry_title}>{`${logEntry.firstname} ${logEntry.lastname} |`}</span>
          </div>
          <div style={{ float: 'left' }} >
            <span className={AppStyles.log_entry_title}>{ this.getDateTime(logEntry) } </span>
          </div>
          <div style={{ float: 'left' }} >
            <span className={AppStyles.log_entry_title}>{ logEntry.logHours } | </span>
          </div>
          <div style={{ float: 'left' }} >
            <span className={AppStyles.log_entry_title}>{`${logEntry.description} | ${logEntry.theme}`} </span>
          </div>
          <div style={{ clear: 'both' }} >
            <span className={AppStyles.log_entry_subtitle}>{ (logEntry.notes || '').substring(0,49) + '...' } </span>
          </div>
        </a>

      </div>
    );
  };
};

export default LogEntry;