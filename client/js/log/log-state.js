import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { updateFilters } from './log-actions';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import sorty from 'sorty';

const DEFAULT_FILTERS = {
  theme: null,
  name: null,
  startDate: null,
  stopDate: new Date(),
  sortKey1: null,
  sortKey2: null,
};

const moment = extendMoment(Moment);

export const reducer = handleActions({
  [updateFilters]: (state, action) => {
    return {
      ...state,
      filters: {
        ...DEFAULT_FILTERS,
        ...action.payload
      }
    };
  }
}, {
  map: buildLogMapFromClientArray(window.INITIAL_STATE.clients),
  filters: DEFAULT_FILTERS
});

export const stateKey = 'logs';

export const selectors = {};

/*
selectors.getAllLogs = createSelector(
  state => state[stateKey].map,
  logsMap => Object.values(logsMap)
);
*/

selectors.getAllLogs = state => Object.values(state[stateKey].map);

selectors.getFilters = state => state[stateKey].filters;

selectors.getFilteredLogs = createSelector(
  selectors.getAllLogs,
  selectors.getFilters,
  (logs, filters) => logs.filter(log => {

    let logName = log.firstname + ' ' + log.lastname;

    // filter based on the theme attribute
    if (filters.theme && filters.theme !== log.theme) return false;
    // filter based on the name attribute
    if (filters.name && filters.name !== logName) return false;
    // filter based on date range provided
    if (filters.startDate || filters.stopDate) {
      //
      // We got at least one date so proceed with date filter.
      //
      // Given both dates: test log date is in that range
      // Givne just startDate: test log date >= to that date
      // Given just stopDate: test log date <= to that date
      //
      let logDate = moment(log.logDate);
      if ( filters.startDate && filters.stopDate ) {
        // Both dates given
        let range = moment.range(moment(filters.startDate), moment(filters.stopDate));
        if (!logDate.within(range)) return false;
      } else if 
      ( filters.startDate && !filters.stopDate ) {
        // Only startDate given
        if (logDate < moment(filters.startDate)) return false;
      } else {
        // Only stopDate given
        if (logDate > moment(filters.stopDate)) return false;
      }
        
    }

    // if we passed all our filters, then keep it
    return true;
  })
);

selectors.getFilteredLogsSorted = createSelector(
  selectors.getFilteredLogs,
  selectors.getFilters,
  (logs, filters) => {
    let sortOpts = [];
    if (filters.sortKey1) sortOpts.push({name: `${filters.sortKey1}`, dir: 'asc'});
    if (filters.sortKey2) sortOpts.push({name: `${filters.sortKey2}`, dir: 'asc'});
    return sorty(sortOpts,logs);
  }
);

// @todo: reselect doesn't support parameterized selectors, so if this proves to be
// too inefficient, we'll want to add some custom memoization
selectors.getLogByClient = (state, clientId) => {
  return selectors.getAllLogs(state).filter(log => log.clientId === clientId);
};

selectors.getLogEntry = (state, logId) => {
  return state[stateKey].map[logId] || null;
};

function buildLogMapFromClientArray(clients) {
  const ret = {};

  if (!clients) return ret;

  clients.forEach(client => {
    client.log.forEach(log => {
      // add client name to our log entry
      // Convert UTC date string to date Object
      let logDate = new Date(log.logDate);
      ret[log.id] = {
        ...log,
        firstname: client.firstname,
        lastname: client.lastname,
        paid: ( log.paid === 'Y' ) ? true : false,
        logDate: logDate,
        logTime: logDate
      };
    });
  });

  return ret;
}
