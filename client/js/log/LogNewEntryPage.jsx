import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as logSelectors } from './log-state';
import { selectors as clientSelectors } from '../clients/clients-state';
import LogEntryForm from './LogEntryForm.jsx';

class LogNewEntryPage extends React.Component {

	componentWillMount() {

		const { clientId, coachId } = this.props;

		this.initialLogEntry = {
			userId: coachId,
			clientId: clientId,
			description: '',
			groupCount: '1',
			paid: false,
			theme: 'PERSONAL',
			logDate: new Date(),
			logTime: new Date(),
			notes: '',
			logHours: '.25'
		};
	}

	render() {
		const { clientName } = this.props;
		const call_origin = window.location;
		const logEntry = this.initialLogEntry;

		return (
			<Page>
				<section>
					<LogEntryForm logEntry={logEntry} clientName={clientName} call_origin={call_origin} action='add'/>
				</section>
			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {
	const clientId = ownProps.params.id;
	const client = clientSelectors.getClient(state, clientId);
	const clientName = client.firstname + ' ' + client.lastname;
	const coachId = client.coachId;

	return { clientName, clientId, coachId };
};

export default connect(mapStateToProps)(LogNewEntryPage);