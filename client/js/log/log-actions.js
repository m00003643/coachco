import { createAction } from 'redux-actions';

export const updateFilters = createAction('UPDATE_FILTERS', filters => filters);
