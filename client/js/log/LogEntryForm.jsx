import React from 'react';
import { getFormValues, isInvalid, reduxForm, SubmissionError } from 'redux-form';
import { Button, Dialog } from 'react-toolbox';
import Field from '../common/Field.jsx';
import DatePickerField from '../common/DatePickerField.jsx';
import TimePickerField from '../common/TimePickerField.jsx';
import DropdownField from '../common/DropdownField.jsx';
import ComboboxField from '../common/ComboboxField.jsx';
import SwitchField from '../common/SwitchField.jsx';
import SliderField from '../common/SliderField.jsx';
import AppStyles from '../common/styles/app.scss';
import ButtonTheme from '../common/styles/BackButton.scss';
import { updateLog, deleteLog } from '../utils/api';
import { addLog } from '../utils/api';
import { roundLogHours } from '../common/Normalize.jsx';

class ClientLogForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      paid: false,
      showVerify: false,
      message: ''
    }

    this.togglePaid = this.togglePaid.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleVerify = this.handleVerify.bind(this);
    this.handleYes = this.handleYes.bind(this);
    
  }

  componentDidMount() {
    this.setState( {paid: this.props.logEntry.paid});
  }

  handleDelete() {
    this.setState({showVerify: !this.state.showVerify});
  }

	handleVerify() {
    this.setState({showVerify: !this.state.showVerify, message: ''});
	}

	handleYes() {

    this.setState({showVerify: !this.state.showVerify, message: ''});
    const hash = this.props.call_origin.hash;
    const clientId = this.props.initialValues.clientId;
 		const action = this.props.action || null;
    const returnUrl = (hash == '#0' && action == 'add') ?
                '/log/add/#0' :
                (hash == '#0' && action != 'add') ?
                '/logs' :
                (hash == '#1') ?
                `/clients/${clientId}#logs` :
                '/logs'                

    deleteLog(this.props.initialValues.id)
      .then(data => {
        window.location = returnUrl;
      },
      err => {
        console.log(err);
        this.setState( {message: 'Failed to remove log entry'} );
      }
    );
	}

  async handleSubmit(values) {

    try {

      // Prepare the values object for submission to the api...
      //
      // Convert selected values to api's required format
      var apiValues = Object.assign({}, values);

      // Convert paid from boolean to string representation
      apiValues.paid = (apiValues.paid) ? 'Y' : 'N';

      // update logDate by combining logDate and logTime then remove logTime from api call
      // ...Build a date object using date from logDate and time from logTime
      let newLogDate = new Date(apiValues.logDate.getFullYear(), apiValues.logDate.getMonth(), 
        apiValues.logDate.getDate(), apiValues.logTime.getHours(), apiValues.logTime.getMinutes(), 
        apiValues.logTime.getSeconds() );
      // ...Convert the date into a valid UTC string
      apiValues.logDate = newLogDate.toUTCString();
      // ...Remove logTime since our api doesn't use it.
      delete apiValues.logTime;

      // Call the api to either add a log or update one...
      //
      if (this.props.action == 'add') {
        await addLog(apiValues).then( (data) => {
          window.location = (typeof this.props.call_origin.hash === 'undefined' || this.props.call_origin.hash == '#0') ? 
            `/logs` : 
            `/clients/${values.clientId}#logs`
        });
      } else {
        await updateLog(apiValues.id, apiValues).then( data => {
          window.location = (typeof this.props.call_origin.hash === 'undefined' || this.props.call_origin.hash == '#0') ? 
            `/logs` : 
            `/clients/${values.clientId}#logs`
        });
      }

    } catch (err) {
      
      console.log(err);
      
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'An unknown error occurred.'
      });
    }

  }

  togglePaid() {
    this.setState({paid: !this.state.paid});
  }

  render () {

    const { error, handleSubmit, pristine, submitting, clientName, initialValues, call_origin } = this.props;
    const clientId = initialValues.clientId;
    const logId = initialValues.id;
 		const action = this.props.action || null;
    const hash = call_origin.hash;

    return (

      <form method='POST' >

        <div>
					<span className={AppStyles.page_title} >
						<Button icon='arrow_back' 
              href={ 
                (hash == '#0' && action == 'add') ?
                '/log/add/#0' :
                (hash == '#0' && action != 'add') ?
                '/logs' :
                (hash == '#1') ?
                `/clients/${clientId}#logs` :
                '/logs'                
              } 
              theme={ButtonTheme} ripple={false} />
						Log Detail for {clientName}
					</span>
        </div>

        <div>
          <Field name='description' type='text' label='Description' required={false} />     
        </div>

        <div>     
          <Field name='groupName' type='text' label="Group Name" required={false} />    
        </div>

        <div>
          <label>Group Count</label>
          <SliderField name='groupCount' min={1} max={50} step={1} pinned={false} 
            snaps={true} disabled={false} editable={true} required={true} />
        </div>

        <div style={{clear: 'both'}}>
          <div style={{float: 'left', 'width': '130px', 'paddingRight': '40px', 'paddingTop': '24px'}} >
            <SwitchField name='paid' 
            label={ (this.state.paid) ? 'Paid' : 'Pro bono'} required={true} onChange={this.togglePaid} />
          </div>
          <div style={{float: 'left'}} >
            <DropdownField name='theme' type='text' label='Theme' required={true}
              source={
                [
                  { value: 'BUSINESS', label: 'Business' },
                  { value: 'MARRIAGE', label: 'Marriage' },
                  { value: 'PERFORMANCE', label: 'Performance' },
                  { value: 'PERSONAL', label: 'Personal' },
                  { value: 'PROFESSIONAL', label: 'Professional'},
                  { value: 'RELATIONSHIP', label: 'Relationship'},
                  { value: 'TRANSITION', label: 'Transition'},
                  { value: 'OTHER', label: 'Other'}
                ]
              }
            />
          </div>
        </div>                  

        <div style={{clear: 'both'}}>
          <div style={{float: 'left', 'width': '130px', 'paddingRight': '40px'}} >
            <DatePickerField name='logDate' type='text' label='Date' autoOk={true} required={true} 
              inputFormat={(value) => `${value.getMonth() + 1}/${value.getDate()}/${value.getFullYear()}`}
            />
          </div>
          <div style={{float: 'left', 'width': '130px'}} >
            <TimePickerField name='logTime' type='text' label='Time' timeformat='ampm' 
              required={true} />
          </div>
        </div>

        <div style={{clear: 'both'}} >
          <label>Time Spent (Hours)</label>
          <SliderField name='logHours' min={.25} max={10} step={.25} pinned={false} 
            snaps={true} disabled={false} editable={true} required={true} 
            normalize={roundLogHours} />
        </div>
        
        <div style={{clear: 'both'}}>
          <Field name='notes' type='text' label="Notes" required={false}
            multiline={true} maxLength={2000} rows={3} />    
        </div>

        <div style={{color:'red', textAlign:'center'}}><p>{this.state.message}</p></div>

        <div className={AppStyles.center_div_content} style={{marginTop: '15px', width: '205px', margin: '0 auto'}} >

          <div style={ (action == 'add') ? {clear: 'both'} : {float:'left'} } >
            <Button type='submit' disabled={pristine || submitting} primary={true} raised={true}
              onMouseUp={handleSubmit(this.handleSubmit.bind(this))} 
              style={{marginRight:'10px'}} >
              { (action == 'add') ? 'Add' : 'Update' }
            </Button>
          </div>

          <div style={ (action == 'add') ? {clear: 'both'} : {float:'left'} } 
            className={(action == 'add') ? AppStyles.hide_me : AppStyles.show_me} >
            <Button primary={true} raised={true}
              onMouseUp={this.handleDelete} 
              className={(action == 'add') ? AppStyles.hide_me : AppStyles.show_me}>
              Delete
            </Button>
          </div>

        </div>

				<div>
					<Dialog 
						active={this.state.showVerify}
						onEscKeyDown={this.handleVerify}
						onOverlayClick={this.handleVerify}
						title='Confirm Delete' 
						actions={[
							{ label: "No", onClick: this.handleVerify },
							{ label: "Yes", onClick: this.handleYes }
						]} >

						<div>
							Are you sure you want to remove this log entry?
						</div>

					</Dialog>
				</div>

      </form>

    );
	}
}

const ClientLogFormContainer = reduxForm({
  form: 'logentryForm'
})(ClientLogForm);

export default props => {
  const initialValues = props.logEntry;
  return <ClientLogFormContainer {...props} initialValues={initialValues} />;
};
