import React from 'react';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import LogList from './LogList.jsx';

class LogPage extends React.Component {

	render() {

		// Call orgin tracks how we got here - either via My Logs or My Clients #Logs.
		// call_origin is used to manage routing behavior when a user clicks the back arrow in the app.
		
		return (
			<Page >
				<LogList call_origin='my_logs' />
			</Page>
		);
	}

}

export default LogPage;
