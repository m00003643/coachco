import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as logSelectors } from './log-state';
import { selectors as clientSelectors } from '../clients/clients-state';
import LogEntryForm from './LogEntryForm.jsx';

class LogEntryPage extends React.Component {

	render() {
		
		const { logEntry, clientName } = this.props;
		const call_origin = window.location;

		return (
			<Page>
				<section >
					<LogEntryForm logEntry={logEntry} clientName={clientName} call_origin={call_origin}/>
				</section>
			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

  const logId = ownProps.params.id;
	const logEntry = logSelectors.getLogEntry(state, logId);
	const client = clientSelectors.getClient(state, logEntry.clientId);
	const clientName = client.firstname + ' ' + client.lastname;

	return {
		logEntry,
		clientName
	};
};

export default connect(mapStateToProps)(LogEntryPage);