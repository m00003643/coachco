import React from 'react';
import { getFormValues, isInvalid, reduxForm, SubmissionError } from 'redux-form';
import { Button } from 'react-toolbox';
import Field from '../common/Field.jsx';
import DropdownField from '../common/DropdownField.jsx';
import AppStyles from '../common/styles/app.scss';
import request from '../utils/request';

class DevTestForm extends React.Component {

  async handleSubmit(data) {

    try {
      console.log('SUBMITTING : ' + data.type + ' : ' + data.url + ' (' + data.id + ')' + data.body);
      
      let retVal = undefined;
      // let parsedJson = undefined;
      try {
        let parsedJson = JSON.parse(data.body||'{}');
        console.log('parsedJson:');
        console.log(JSON.stringify(parsedJson,null,4));

        switch (data.type) {
          // POST (Create New) and PATCH (Update) get a Json passed in the body
          // PATCH, GET and DELETE all pass in the id of the record on the url
          // GET and DELETE don't require a body - just the id
          case 'POST' :  retVal = await request(data.url).post(parsedJson); break;
          case 'PATCH' :  retVal = await request(data.url + `/${data.id}`).patch(parsedJson); break;
          case 'GET' :  retVal = await request(data.url + `/${data.id}`).get(); break;
          case 'DELETE' :  retVal = await request(data.url + `/${data.id}`).delete(); break;
          case 'EXPORT' :  { 
                             var opts = {method: 'POST',credentials: 'same-origin',headers: { 'Content-Type': 'application/json' }};
                             opts.body = JSON.stringify(parsedJson); 
                             retVal = await fetch(data.url, opts)
                                .then(response => response.blob())
                                .then(blob => {
                                   var url = window.URL.createObjectURL(blob);
                                   var a = document.createElement('a');
                                   a.href = url;
                                   a.download = "export.csv";
                                   a.click();                    
                                });
                           } 
        }

        if (retVal) {
          console.log(retVal);
          console.log(' RETURNED : ' + JSON.stringify(retVal,null,2) );
        }
      } catch (err) {
        console.log('ERROR - INVALID JSON (' + err + ')');
      }

    } catch (err) {
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'There was an unknown error while attempting to save form.'
      });
    }
  }

  render () {

    const { error, handleSubmit, pristine, submitting } = this.props;

    const validate = values => {
      const errors = {}
      return errors;
    }

    return (

      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} method='POST' >
        <div>
          <div style={{clear: 'both'}}>
              <Field name='id' type='number' label="id" defaultValue="1" required={false} />  
              <DropdownField name='type' type='text' label='type' defaultValue="GET" required={true}
                source={
                  [
                    { value: 'POST', label: 'Post' },
                    { value: 'GET', label: 'Get' },
                    { value: 'PATCH', label: 'Patch' },
                    { value: 'EXPORT', label: 'Export' },
                    { value: 'DELETE', label: 'Delete' }
                  ]
                }
              />
              <Field name='url' type='text' label="url" defaultValue="/api/users" required={true} />  
              <Field name='body' type='text' label="body" defaultValue="{}" required={false} multiline={true} maxLength={2000} rows={10} />    
          </div>
        </div>

        <div className={AppStyles.center_div_content} >
          <Button type="submit" disabled={submitting} primary={true} 
          raised={true}>Submit</Button>
        </div>
      </form>
    );
	}
}

export default reduxForm({
  form: 'devtest'
})(DevTestForm);
