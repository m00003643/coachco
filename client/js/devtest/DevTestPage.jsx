import React from 'react';
import { connect } from 'react-redux';
import DevTestForm from './DevTestForm.jsx';
import { Panel, AppBar} from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';

class DevTestPage extends React.Component {

	render() {

		return (
			<Page >
				<section>
					<span className={AppStyles.page_title}>Dev Test</span>
					<span>API call results are written to Console Log...</span>
					<DevTestForm />
				</section>
	    </Page >
		);
	}

}

export default DevTestPage;