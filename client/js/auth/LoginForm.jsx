import React from 'react';
import { reduxForm, SubmissionError } from 'redux-form';
import { Button } from 'react-toolbox';
import Field from '../common/Field.jsx';
import { getAuthenticatedUser } from '../utils/api';
import { makeLowerCase } from '../common/Normalize.jsx';
import AppStyles from '../common/styles/app.scss';

class LoginForm extends React.Component {

  async handleSubmit(data) {

    try {
      // note: we don't need any of the return values because any failed login attempt
      // will throw, and on success we just perform a redirect.
      let result = await getAuthenticatedUser(data.username, data.password);
      
      if (result.body.result.role === 'COACH') 
        window.location = '/clients';
      else
        window.location = '/assessments';

    } catch (err) {

      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'Unable to log in. Make sure your Username and Password are correct '
      });
    }
  }

  render() {
    const { invalid, error, handleSubmit, pristine, submitting } = this.props;

    return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <div>
          <Field name='username' type='text' label='Username' 
            required={true} autoFocus={true} normalize={makeLowerCase} />
        </div>
        <div>
          <Field name='password' type='password' label='Password' required={true} />
        </div>

        {error && <div className={AppStyles.error_msg} style={{marginTop: '10px'}}>{error}</div>}
        
        <div style={{textAlign:'center', marginTop:'10px'}} >
          <Button type='submit' disabled={submitting || pristine} primary={true} raised={true}>
            Login
          </Button>
        </div>
      </form>
    );
  }

}


export default reduxForm({
  form: 'login'
})(LoginForm);
