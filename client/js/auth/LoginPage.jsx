import React from 'react';
import { connect } from 'react-redux';
import LoginForm from './LoginForm.jsx';
import { Layout, Panel, AppBar} from 'react-toolbox';
import styles from './auth.scss';
import appStyles from '../common/styles/app.scss';
import { selectors as brandSelectors } from '../administration/branding-state';

class LoginPage extends React.Component {

	render() {

		const { brand } = this.props;
		
		return (
			<div>
				<AppBar title={brand.companyInfo} />
				<section className={styles.main_panel} >

					<div style={{textAlign:"center"}}>
						<img src={brand.companyLogo} />
						<br />
						<h3>Welcome to {brand.companyName}</h3>
						<p>This site is intended for existing coaches and clients of {brand.companyName}</p>
						<br /><br />
					</div>
			
					<div className={styles.login_container}>
						<span className={appStyles.page_title}>Please log in</span>
						<LoginForm  />
					</div>

				</section>
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {

  return {
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(LoginPage);
