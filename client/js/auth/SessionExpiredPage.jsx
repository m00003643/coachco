import React from 'react';
import { connect } from 'react-redux';
import { Layout, Panel, AppBar, Button} from 'react-toolbox';
import styles from './auth.scss';
import appStyles from '../common/styles/app.scss';
import { selectors as brandSelectors } from '../administration/branding-state';

class SessionExpiredPage extends React.Component {

	render() {

		const { brand } = this.props;

		return (
			<div>
				<AppBar title={brand.companyName} />
				<section className={styles.main_panel} >

					<div style={{textAlign:"center"}}>
						<img src={brand.companyLogo} />
						<br />
						<h3>Welcome to CoachCo</h3>
						<p>This site is intended for existing coaches and clients of CoachCo</p>
						<br /><br />
					</div>
			
					<div style={{textAlign:'center'}} >
						<p>Your session has expired. Please login again.</p>
					</div>

          <div style={{textAlign:'center'}} >
						<Button label='Login' flat primary href='/login' />
          </div>

				</section>
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {

  return {
    brand: brandSelectors.getBranding(state)
  };
};

export default connect(mapStateToProps)(SessionExpiredPage);
