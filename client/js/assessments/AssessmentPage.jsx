import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as assessmentSelectors } from './assessments-state';
import AssessmentForm from './AssessmentForm.jsx';

class AssessmentPage extends React.Component {

	render() {
		
		const { params } = this.props;

		return (
			<Page >
				<AssessmentForm assessment={this.props.assessment} />
			</Page>
		);
	}

}

const mapStateToProps = function(state, ownProps) {

  const id = ownProps.params.id;

	return {
		assessment: assessmentSelectors.getAssessment(state, id)
	};
};

export default connect(mapStateToProps)(AssessmentPage);