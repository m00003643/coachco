import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { updateAssessments } from './assessments-actions';

export const reducer = handleActions({
  // action.payload == the assessments
  [updateAssessments]: (state, action) => action.payload
}, buildMapFromAssessmentArray(window.INITIAL_STATE.assessments));

export const stateKey = 'assessments';

export const selectors = {
  getAssessments: createSelector(
    state => state[stateKey],
    assessmentsMap => Object.values(assessmentsMap)
  ),
  getAssessment: (state, id) => {
  	return state[stateKey][id] || null
  }
};

function buildMapFromAssessmentArray(assessments) {

	const ret = {};
  
	if (!assessments) return ret;
	assessments.forEach(assessment => ret[assessment.id] = assessment);
	return ret;
}
