import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-toolbox';
import Page from '../common/Page.jsx';
import AppStyles from '../common/styles/app.scss';
import { selectors as assessmentSelectors } from './assessments-state';
import AssessmentsList from './AssessmentsList.jsx';

class AssessmentsPage extends React.Component {

	render() {
		
		const { assessments } = this.props;

		return (
			<Page >
				<span className={AppStyles.page_title}>My Assessments</span>
				<AssessmentsList assessments={assessments} />
			</Page>
		);
	}

}

const mapStateToProps = function(state) {

	return {
		assessments: assessmentSelectors.getAssessments(state)
	};
};

export default connect(mapStateToProps)(AssessmentsPage);