import { createAction } from 'redux-actions';

export const updateAssessments = createAction('UPDATE_ASSESSMENTS', assessments => assessments);
