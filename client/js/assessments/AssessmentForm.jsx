import React from 'react';
import { reduxForm, SubmissionError } from 'redux-form';
import { Button } from 'react-toolbox';
import { updateAssessment } from '../utils/api';
import Field from '../common/Field.jsx';
import SliderField from '../common/SliderField.jsx';
import AppStyles from '../common/styles/app.scss';
import updateAssessments from './assessments-actions';

class AssessmentForm extends React.Component {

  async handleSubmit(data) {

    // Get a copy of the original assessment. We will update this copy below with passed in values
    const a = {...this.props.assessment};

    // Get a copy of the incoming data.
    const d = {...data};

    // Update the feedback text data. If it wasn't changed just default to the original. 
    // data object for the next step.
    const feedback = d.feedback_text;
    a.assessment.feedback_text = feedback;

    // Remove feedback from our data copy. This will leave only the prompts section of our assesment
    delete d.feedback_text;

    // Update just the prompt values
    Object.values(d).map( (val, index) => {
      a.assessment.prompts[index] = { ...a.assessment.prompts[index], value: val};
    });

    // Update the dateCompleted
    const dt = new Date();
    a.assessment.dateCompleted = `${dt.getMonth() + 1}/${dt.getDate()}/${dt.getFullYear()}`;

    // Get the assessment's id for our api call
    const id = a.id;

    try {
      await updateAssessment(id, a)
        .then( data => {
          window.location = '/assessments';
        });
    } catch (err) {
      console.log(err);
      if (err.validationErrors) {
        throw new SubmissionError(err.validationErrors);
      }

      throw new SubmissionError({
        _error: err.errorMessage || 'There was an unknown error while attempting to save form.'
      });
    }
  }

	render() {

    const { invalid, error, handleSubmit, pristine, submitting } = this.props;
		const assessment = this.props.assessment.assessment;
    const questions = assessment.prompts;

		return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <div>
				<Button icon='arrow_back' href='/assessments' />
          
        <span className={AppStyles.page_title}>{assessment.type}</span>
        <p className={AppStyles.notes_header}>{assessment.directions_title}:</p>
        <label className={AppStyles.notes_content}>{assessment.directions}</label>
        <br /><br />

        {
            questions.map( (question, index) => (
              <div key={index}>
                <p>{question.prompt}</p>
                <SliderField name={`prompt-${index}`}
                  min={0} max={10} step={1} pinned={true} snaps={true} 
                  disabled={this.props.completed}/>
              </div>
            ))
        }

        <p className={AppStyles.notes_header}>{assessment.feedback_title}:</p>
        <Field type='text' name="feedback_text" multiline={true} label='' maxLength={2000}
          rows={2} placeholder='Your input is welcome' />

        </div>

        {error && <strong>{error}</strong>}

        <div style={{textAlign:"center"}}>
          <Button type='submit' disabled={submitting || pristine || this.props.completed} primary={true} raised={true}>
            Update
          </Button>
        </div>

      </form>
		);
	}

}

const AssessmentFormContainer = reduxForm({
  form: 'assessmentForm'
})(AssessmentForm);

export default props => {

  let initialValues = props.assessment.assessment.prompts.reduce((vals, prompt, i) => {
    vals[`prompt-${i}`] = prompt.value;
    return vals;
  }, {});

  initialValues = {...initialValues, feedback_text: props.assessment.assessment.feedback_text}
  const completed = (props.assessment.assessment.dateCompleted === null) ? false : true;

  return <AssessmentFormContainer {...props} initialValues={initialValues} completed={completed} />;
};
