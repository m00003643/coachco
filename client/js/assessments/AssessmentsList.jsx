import React from 'react';
import { List, ListItem } from 'react-toolbox';

class AssessmentsList extends React.Component {

	render() {

		const { assessments } = this.props;

		return (

			<div>
				<List selectable ripple>
					{assessments.map( (assessment, index) => (
						<ListItem
							key={assessment.id}
							caption={assessment.assessment.type}
							legend={(assessment.assessment.dateCompleted != null) ? 'Completed on ' + assessment.assessment.dateCompleted : 'Assigned on ' + assessment.assessment.dateAssigned}
							rightIcon='description'
							to={`/assessments/${assessment.id}`}
						/>
					))}
				</List>
			</div>
		);

	}
}

export default AssessmentsList;