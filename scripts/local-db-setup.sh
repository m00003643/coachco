echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!! WARNING: THIS BLOWS AWAY THE DEV !!!"
echo "!!! DATABASE AND CREATES A NEW ONE!  !!!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""

while true; do
   read -p "Do you want to *DELETE any existing dev database*, and create a new one? (y/n) " yn
   case $yn in
       [Yy]* ) break;;
       [Nn]* ) exit;;
       * ) echo -e "Please answer yes or no.\n";;
   esac
done

echo "";

HOST="localhost"
PORT="5432"
USER="postgres"

#export PGPASSWORD=""

PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -c "DROP DATABASE IF EXISTS ctk_dev;"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -c "CREATE DATABASE ctk_dev;"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -f "ctk-drop-tables.sql" "ctk_dev"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -f "ctk-create-tables.sql" "ctk_dev"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -f "ctk-create-sampledata.sql" "ctk_dev"
