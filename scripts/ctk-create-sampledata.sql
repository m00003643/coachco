/* 
 * Load sample data in to app_defaults table.
 */

INSERT INTO app_defaults (default_name, default_json) values (
  'userSettings',
  '{
      "sendEmailNotifications": {
        "scope": "both",
        "label":  "Send Email Notifications",
        "type": "boolean",
        "value": true
      },
      "sendSMSNotifications": {
        "scope": "both",
        "label":  "Send SMS Notifications",
        "type": "boolean",
        "value": false
      },
      "sendSMSNumber": {
        "scope": "both",
        "label":  "SMS Number",
        "type": "text",
        "value": "5551234567"
      },
      "allowedFileUploadSizeLimit": {
        "scope": "coach",
        "label":  "Maximum File Size (MB)",
        "type": "list",
        "options": [0.5, 1, 2, 5, 10],
        "value": 1
      },
      "chatPageRefreshInterval": {
        "scope": "both",
        "label":  "Chat Page Refresh Interval (seconds)",
        "type": "list",
        "options": [0, 1, 5, 10, 30, 60],
        "value": 10
      }
   }' 
);

INSERT INTO app_defaults (default_name, default_json) values (
  'coachBranding',
  '{
    "companyName": "CoachCo",
    "companyTagline": "Excellence in Coaching, Everyday!",
    "companyLogo": "https://s3.amazonaws.com/coachco/assets/Stock/DefaultLogo.png",
    "profileImage": "https://s3.amazonaws.com/coachco/assets/Stock/default-profile.png",
    "welcomeEmail": [ 
    "<p>%BRAND-EMAIL-LOGO%</p>", 
    "<p>Welcome to %BRAND-COMPANY%, %CLIENT-FIRSTNAME%!</p>", 
    "<p>Before you begin, you need accept your invitation and select a username and password. Your username needs to be all lower case letters and may contain numbers as well. You can accept your invitation by clicking the link below or entering your code at <a href=''%ACCEPT-URL%''>%ACCEPT-URL%</a>&nbsp;and entering the&nbsp;email and invitation code below.</p>",
    "<p>Thank you!</p>",
    "<p>%COACH-FIRSTNAME%&nbsp;%COACH-LASTNAME%</p>", 
    "<p><strong>User Email</strong>&nbsp;%CLIENT-EMAIL%</p>", 
    "<p><strong>Invitation Code</strong>&nbsp;%INVITE-CODE%</p>", 
    "<p><strong>Invitation Link</strong>&nbsp;<a href=''%ACCEPT-URL%?email=%CLIENT-EMAIL%&amp;invite=%INVITE-CODE%''>%ACCEPT-URL%?email=%CLIENT-EMAIL%&amp;invite=%INVITE-CODE%</a></p>"
    ],
    "assessmentNotificationEmail": [ 
    "<p>%BRAND-EMAIL-LOGO%</p>", 
    "<p>Hello %CLIENT-FIRSTNAME%!</p>", 
    "<p>I have assigned a new assessment that I would like you to complete.  Please log in at your convience and complete it.</p>",
    "<p>Thank you!</p>",
    "<p>%COACH-FIRSTNAME%&nbsp;%COACH-LASTNAME%</p>", 
    "<p><strong>Login at </strong>&nbsp;<a href=''%BASE-URL%''>%BASE-URL%</a></p>"
    ]
    }' 
);

/* 
 *  Load sample data into App_Users table. 
 *
 *  Note we need to alter the (automatically created) sequence for ID afterwards to point at the next available number after our sample data.
 *  Failure to do so will leave the PG CurrVal for this sequence unset which will cause our first insert ... returing to fail.
 *
 *  Note, BRANDING field for Coaches is populated with defaults from the APP_DEFAULTS table using an AFTER INSERT database trigger
 */

INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, company, title, website, address, city, state, postalcode, country, coach_id, role, status, master, notes) values 
  (1, 'jdoe@nowhere.com', 'jdoe', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'John', 'Doe', '(555) 123-4567', 'CoachCo', 'Consultant', 'www.coachco.com', '123 Main Street', 'Anywhere', 'WA', '98123-1234', 'USA', NULL, 'COACH', 'ACTIVE', 'Y', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, coach_id, role) values 
  (2, 'tsmith@nowhere.com', 'tsmith', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Thomas', 'Smith', '(555) 123-4567', 1, 'CLIENT');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, company, title, website, coach_id, role) values 
  (3, 'santhony@nowhere.com', 'santhony', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Susan', 'Anthony', '(555) 123-4567', 'Acme', 'VP Sales', 'www.acme.com', 1, 'CLIENT');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, coach_id, role) values 
  (4, 'bjones@nowhere.com', 'bjones', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Bridget', 'Jones', '(555) 123-4567', 1, 'COACH');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, coach_id, role) values 
  (5, 'ltoms@nowhere.com', 'ltoms', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Larry', 'Toms', '(555) 123-4567', 4, 'CLIENT');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, coach_id, role) values 
  (6, 'wless@nowhere.com', 'wless', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Wanda', 'Less', '(555) 123-4567', 4, 'CLIENT');
INSERT INTO app_user (id, email, username, password, firstname, lastname, phone, coach_id, role, status) values 
  (7, 'hzvala@nowhere.com', 'hzavala', '$2a$08$BL0b2krs4RQt6EtgRFD5c.i00rgc9vISVj6Al.uZE8FCnLyZ8w2LS', 'Henry', 'Zavala', '(555) 123-4567', 1, 'CLIENT', 'INACTIVE');

update app_user set branding = jsonb_set(branding::jsonb, '{"companyLogo"}', '"https://s3.amazonaws.com/coachco/assets/1/NVA_logo_green-silver_transparent_medium.png"'::jsonb, true) where id = 1;
update app_user set branding = jsonb_set(branding::jsonb, '{"companyName"}', '"Doe Coaching"'::jsonb, true) where id = 1;
update app_user set branding = jsonb_set(branding::jsonb, '{"companyLogo"}', '"https://s3.amazonaws.com/coachco/assets/4/NVA_logo_green-silver_transparent_medium.png"'::jsonb, true) where id = 4;
update app_user set branding = jsonb_set(branding::jsonb, '{"companyName"}', '"Jones Coaching"'::jsonb, true) where id = 4;

ALTER SEQUENCE app_user_id_seq RESTART WITH 8;

/* 
 *  Load sample data into Assessment_Template table
 *
 *  Note we need to alter the (automatically created) sequence for ID afterwards to point at the next available number after our sample data.
 *  Failure to do so will leave the PG CurrVal for this sequence unset which will cause our first insert ... returing to fail.
 */
  
INSERT INTO "assessment_template"(id, coach_id, assessment_template) VALUES (1, 1, 
  '{ 
    "type": "Career Alignment",
    "dateAssigned": null,
    "dateCompleted": null,
    "mentorReviewable": "N",
    "directions_title": "Directions",
    "directions": "For each section, select the number that represents your current level of satisfaction. The higher the number, the more satisfied you are in that area.",
    "feedback_title": "Feedback",
    "feedback_text": "Was this assessment motivational?",
    "prompts": [
      {"prompt" : "Leadership", "value" : 0},
      {"prompt" : "Achievement", "value" : 0},
      {"prompt" : "Personal Performance", "value" : 0},
      {"prompt" : "Vision", "value" : 0},
      {"prompt" : "Fulfillment", "value" : 0},
      {"prompt" : "Self Expression", "value" : 0},
      {"prompt" : "Organization", "value" : 0},
      {"prompt" : "Excellence", "value" : 0}
    ]
  }'
);

INSERT INTO "assessment_template"(id, coach_id, assessment_template) VALUES (2, 1, 
  '{ 
    "type": "Personal Life",
    "dateAssigned": null,
    "dateCompleted": null,
    "mentorReviewable": "N",
    "directions_title": "Directions",
    "directions": "For each section, select the number that represents your current level of satisfaction. The higher the number, the more satisfied you are in that area.",
    "feedback_title": "Feedback",
    "feedback_text": "Which question was the most difficult to answer and why?",
    "prompts": [ 
      {"prompt" : "Family / Parenting", "value" : 0},
      {"prompt" : "Personal Deveopment", "value" : 0},
      {"prompt" : "Spiritual Awareness", "value" : 0},
      {"prompt" : "Fun / Enjoyment", "value" : 0},
      {"prompt" : "Intimate Relationship", "value" : 0},
      {"prompt" : "Intimate Relationship", "value" : 0},
      {"prompt" : "Social Relationship", "value" : 0},
      {"prompt" : "Heath / Aging", "value" : 0},
      {"prompt" : "Personal Finance", "value" : 0},
      {"prompt" : "Career / Profession", "value" : 0}
    ]
  }'
);

INSERT INTO "assessment_template"(id, coach_id, assessment_template) VALUES (3, 4, 
  '{ 
    "type": "Personal Development",
    "dateAssigned": null,
    "dateCompleted": null,
    "mentorReviewable": "N",
    "directions_title": "Directions",
    "directions": "For each section, select the number that represents your current level of satisfaction. The higher the number, the more satisfied you are in that area.",
    "feedback_title": "Feedback",
    "feedback_text": "Which question was the most difficult to answer and why?",
    "prompts": [ 
      {"prompt" : "Leadership", "value" : 0},
      {"prompt" : "Achievement", "value" : 0},
      {"prompt" : "Personal Performance", "value" : 0},
      {"prompt" : "Vision", "value" : 0},
      {"prompt" : "Fulfillment", "value" : 0},
      {"prompt" : "Self Expression", "value" : 0},
      {"prompt" : "Organization", "value" : 0},
      {"prompt" : "Excellence", "value" : 0}
    ]
  }'
);

ALTER SEQUENCE assessment_template_id_seq RESTART WITH 4;

/* 
 *  Load sample data into Assessment table.  Note we are using the Assessment_Templates we just inserted above...
 */

insert into assessment (user_id, assessment) select '2', assessment_template from assessment_template where id = 1;
insert into assessment (user_id, assessment) select '2', assessment_template from assessment_template where id = 2;
insert into assessment (user_id, assessment) select '3', assessment_template from assessment_template where id = 3;
insert into assessment (user_id, assessment) select '4', assessment_template from assessment_template where id = 2;
insert into assessment (user_id, assessment) select '5', assessment_template from assessment_template where id = 2;
insert into assessment (user_id, assessment) select '5', assessment_template from assessment_template where id = 3;
insert into assessment (user_id, assessment) select '6', assessment_template from assessment_template where id = 2;
update assessment set assessment = jsonb_set(assessment, '{"dateAssigned"}', '"03/01/2017"'::jsonb, true);
update assessment set assessment = jsonb_set(assessment, '{"dateCompleted"}', 'null'::jsonb, true);
update assessment set assessment = jsonb_set(assessment, '{"mentorReviewable"}', '"Y"'::jsonb, true) where id in (6, 7);


/*
 * Load sample data into the assets table...  Aseset Type A means Automatic, U means Unique...  Automatic assets should automatically
 * be propogated to clients of a give user.
 *
 *  Note we need to alter the (automatically created) sequence for ID afterwards to point at the next available number after our sample data.
 *  Failure to do so will leave the PG CurrVal for this sequence unset which will cause our first insert ... returing to fail.
 */

/*
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (1, 1, 'A', 'Client Questionnaire', '/static/assets/StockQuestionnaire.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (2, 1, 'A', 'Wheel Sample', '/static/assets/SampleWheel.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (3, 1, 'I', 'Client Document', '/static/assets/UniqueSample.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (4, 4, 'A', 'Client Questionnaire', '/static/assets/StockQuestionnaire.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (5, 4, 'A', 'Wheel Sample', '/static/assets/SampleWheel.pdf');
*/

insert into assets (id, user_id, asset_type, asset_name, asset_url) values (1, 1, 'A', 'Client Questionnaire', 'https://coachco.s3.amazonaws.com/assets/1/StockQuestionnaire.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (2, 1, 'A', 'Wheel Sample', 'https://coachco.s3.amazonaws.com/assets/1/SampleWheel.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (3, 1, 'I', 'Client Document', 'https://coachco.s3.amazonaws.com/assets/1/UniqueSample.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (4, 4, 'A', 'Client Questionnaire', 'https://coachco.s3.amazonaws.com/assets/4/StockQuestionnaire.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (5, 4, 'A', 'Wheel Sample', 'https://coachco.s3.amazonaws.com/assets/4/SampleWheel.pdf');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (6, 1, 'L', 'Logo', 'https://s3.amazonaws.com/coachco/assets/1/NVA_logo_green-silver_transparent_medium.png');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (7, 4, 'L', 'Logo', 'https://s3.amazonaws.com/coachco/assets/4/NVA_logo_green-silver_transparent_medium.png');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (8, 2, 'C', 'Sample Image', 'https://s3.amazonaws.com/coachco/assets/2/imagesample.jpg');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (9, 3, 'C', 'Sample Image', 'https://s3.amazonaws.com/coachco/assets/3/imagesample.jpg');
insert into assets (id, user_id, asset_type, asset_name, asset_url) values (10, 4, 'C', 'Sample Image', 'https://s3.amazonaws.com/coachco/assets/4/imagesample.jpg');

ALTER SEQUENCE assets_id_seq RESTART WITH 11;

/*
 *  Load sample Assigned Assets...  Each client gets the AUTOMATIC assets from there coach, also add one or two INDIVIDUAL ones 
 */

insert into assigned_assets (user_id, asset_id) select '2', id from assets where user_id = 1 and asset_type = 'A';
insert into assigned_assets (user_id, asset_id) select '2', id from assets where user_id = 1 and asset_type = 'I';
insert into assigned_assets (user_id, asset_id) select '3', id from assets where user_id = 1 and asset_type = 'A';
insert into assigned_assets (user_id, asset_id) select '4', id from assets where user_id = 1 and asset_type = 'A';
insert into assigned_assets (user_id, asset_id) select '5', id from assets where user_id = 4 and asset_type = 'A';
insert into assigned_assets (user_id, asset_id) select '6', id from assets where user_id = 4 and asset_type = 'A';


/*
 *  Load sample Client Log data...
 */

insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 2, '01/03/2017 13:00:00', 'PERSONAL', 'Y', 1.0, null, 1, 'One-on-One', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 2, '01/14/2017 14:30:00', 'PERSONAL', 'Y', 0.5, null, 1, 'One-on-One', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 2, '01/22/2017 09:15:00', 'PERSONAL', 'Y', 0.75, null, 1, 'One-on-One', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 2, '01/22/2017 13:00:00', 'PERSONAL', 'N', 2.5, null, 1, 'One-on-One', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 3, '01/03/2017 11:45:00', 'PROFESSIONAL', 'Y', 1.0, 'Alpha Team', 6, 'Web-Ex', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 3, '01/14/2017 16:30:00', 'PROFESSIONAL', 'Y', 0.5, 'Beta Team', 4, 'Web-Ex', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');
insert into client_log (user_id, client_id, log_date, theme, paid, log_hours, group_name, group_count, description, notes) values (1, 4, '01/22/2017 08:45:00', 'BUSINESS', 'N', 1.5, null, 1, 'Telephone', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit eros, consectetur ac ornare mollis, interdum at enim. Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.');

/*
 *  Load sample converstations...
 */

INSERT INTO "conversations" (id, user_id, coach_id, conversation) VALUES (1, 2, 1, 
  '{ "messages" : [
     { 
       "commentWhen" : "2017-06-02T10:05:20-07:00",
       "commentMessage" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
       "commentLink" : "https://s3.amazonaws.com/coachco/assets/Stock/imagesample.jpg",
       "commentUserType" : "COACH"
     },
     { 
       "commentWhen" : "2017-06-02T10:04:20-07:00",
       "commentMessage" : "Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.",
       "commentLink" : null,
       "commentUserType" : "CLIENT"
     }
  ] }' 
);
INSERT INTO "conversations" (id, user_id, coach_id, conversation) VALUES (2, 3, 1, 
  '{ "messages" : [
     { 
       "commentWhen" : "2017-06-02T10:05:20-07:00",
       "commentMessage" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
       "commentLink" : "https://s3.amazonaws.com/coachco/assets/Stock/imagesample.jpg",
       "commentUserType" : "COACH"
     },
     { 
       "commentWhen" : "2017-06-02T10:04:20-07:00",
       "commentMessage" : "Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.",
       "commentLink" : null,
       "commentUserType" : "CLIENT"
     }
  ] }' 
);
INSERT INTO "conversations" (id, user_id, coach_id, conversation) VALUES (3, 4, 1, 
  '{ "messages" : [
     { 
       "commentWhen" : "2017-06-02T10:05:20-07:00",
       "commentMessage" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
       "commentLink" : "https://s3.amazonaws.com/coachco/assets/Stock/imagesample.jpg",
       "commentUserType" : "COACH"
     },
     { 
       "commentWhen" : "2017-06-02T10:04:20-07:00",
       "commentMessage" : "Donec ut ipsum pulvinar risus imperdiet vehicula convallis sed eros.",
       "commentLink" : null,
       "commentUserType" : "CLIENT"
     }
  ] }' 
);

ALTER SEQUENCE conversations_id_seq RESTART WITH 4;
