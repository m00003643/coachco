require('babel-polyfill');
require('babel-register')({
	extensions: ['.js'],
	only: /server/,
	plugins: ['transform-async-to-generator','babel-plugin-transform-object-rest-spread']
});
require('../server/app');