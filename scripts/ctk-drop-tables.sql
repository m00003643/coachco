/*
 *  Note order is important.  We must drop child tables (like assigned_assets) before we can drop the parent table (like assets)
 */ 
drop view if exists "client_assets";
drop table if exists "assessment_template";
drop table if exists "client_log";
drop table if exists "assessment";
drop table if exists "assigned_assets";
drop table if exists "assets";
drop table if exists "app_user";
drop table if exists "app_defaults";
drop table if exists "converstations";