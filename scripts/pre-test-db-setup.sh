HOST="localhost"
PORT="5432"
USER="postgres"

#export PGPASSWORD=""

PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -c "DROP DATABASE IF EXISTS ctk_test;"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -c "CREATE DATABASE ctk_test;"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -f "ctk-drop-tables.sql" "ctk_test"
PGOPTIONS='--client-min-messages=warning' psql -h $HOST -p $PORT -U $USER -f "ctk-create-tables.sql" "ctk_test"