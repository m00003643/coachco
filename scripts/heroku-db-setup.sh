echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!! WARNING: THIS BLOWS AWAY THE DEV !!!"
echo "!!! DATABASE AND CREATES A NEW ONE!  !!!"
echo "!!!                                  !!!"
echo "!!! YOU MUST BE LOGGED INTO THE      !!!"
echo "!!! HEROKU CLI FOR THIS TO WORK!     !!!"
echo "!!!                                  !!!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""

while true; do
   read -p "Do you want to *DELETE any existing stage database*, and create a new one? (y/n) " yn
   case $yn in
       [Yy]* ) break;;
       [Nn]* ) exit;;
       * ) echo -e "Please answer yes or no.\n";;
   esac
done

echo "";

DBNAME="postgresql-polished-41116"

cat "ctk-drop-tables.sql" | heroku pg:psql $DBNAME 
cat "ctk-create-tables.sql" | heroku pg:psql $DBNAME
cat "heroku-create-sampledata.sql" | heroku pg:psql $DBNAME 
