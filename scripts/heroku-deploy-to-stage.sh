echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!! WARNING: THIS PUSHES ALL CHANGES !!!"
echo "!!! TO HEROKU AND RESTARTS THE APP   !!!"
echo "!!!                                  !!!"
echo "!!! YOU MUST BE LOGGED INTO THE      !!!"
echo "!!! HEROKU CLI FOR THIS TO WORK!     !!!"
echo "!!!                                  !!!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""

while true; do
   read -p "Do you want to pusgh all changes to Heroku? Note, you must be logged into the Heroku CLI for this to work. (y/n) " yn
   case $yn in
       [Yy]* ) break;;
       [Nn]* ) exit;;
       * ) echo -e "Please answer yes or no.\n";;
   esac
done

echo "";

git add .
git commit -am "Latest Changes"
git push heroku master