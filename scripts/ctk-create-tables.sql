CREATE TABLE "app_user" (
   "id"                      serial PRIMARY KEY,
   "email"                   varchar(100) NOT NULL,
   "username"                varchar(20) NOT NULL,
   "password"                varchar(100) NOT NULL,
   "reset_token"             varchar(12),
   "firstname"               varchar(40) NOT NULL,
   "lastname"                varchar(40) NOT NULL,
   "title"                   varchar(50),
   "company"                 varchar(50),
   "website"                 varchar(512),
   "phone"                   varchar(18),
   "address"                 varchar(80),
   "city"                    varchar(50),
   "state"                   varchar(2),
   "postalcode"              varchar(10),
   "country"                 varchar(25),
   "coach_id"                integer,
   "role"                    varchar(12) NOT NULL DEFAULT 'CLIENT' CHECK (role IN ('COACH', 'CLIENT')),
   "status"                  varchar(12) NOT NULL DEFAULT 'ACTIVE' CHECK (status IN ('ACTIVE', 'INACTIVE', 'PENDING', 'REQUEST')),
   "master"                  varchar(1) NOT NULL DEFAULT 'N' CHECK (master IN ('Y','N')),
   "branding"                jsonb,
   "settings"                jsonb,
   "notes"                   varchar(255),
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   FOREIGN KEY (coach_id) REFERENCES "app_user"(id) ON DELETE SET NULL,
   CONSTRAINT unq_app_username UNIQUE(username),
   CONSTRAINT unq_app_email UNIQUE(email)
);

CREATE TABLE "assessment" (
   "id"                      serial PRIMARY KEY,
   "user_id"                 integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "assessment"              jsonb,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "assessment_template" (
   "id"                      serial PRIMARY KEY,
   "coach_id"                integer NOT NULL references "app_user"(id) ON DELETE CASCADE, 
   "assessment_template"     jsonb,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "assets" (
   "id"                      serial PRIMARY KEY,
   "user_id"                 integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "asset_type"              varchar(1) NOT NULL DEFAULT 'I' CHECK (asset_type IN ('A', 'I', 'L', 'C')),
   "asset_name"              varchar(25) NOT NULL DEFAULT 'Asset',
   "asset_url"               varchar(512) not null,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "assigned_assets" (
   "id"                      serial PRIMARY KEY,
   "user_id"                 integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "asset_id"                integer NOT NULL references "assets"(id) ON DELETE CASCADE,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "client_log" (
   "id"                      serial PRIMARY KEY,
   "user_id"                 integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "client_id"               integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "log_date"                timestamp NOT NULL,
   "theme"                   varchar(15) NOT NULL default 'PERSONAL' CHECK (theme IN ('PERSONAL', 'PROFESSIONAL', 'PERFORMANCE', 'BUSINESS', 'RELATIONSHIP', 'MARRIAGE', 'TRANSITION', 'OTHER')),
   "paid"                    varchar(1) NOT NULL DEFAULT 'Y' CHECK (paid IN ('Y', 'N')),
   "log_hours"               numeric(8,2) NOT NULL DEFAULT 0,
   "group_name"              varchar(25),
   "group_count"             integer not null default 1,
   "description"             varchar(25),
   "notes"                   varchar(255),
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "app_defaults" (
   "id"                      serial PRIMARY KEY,
   "default_name"            varchar(25) NOT NULL,
   "default_number"          integer,
   "default_boolean"         boolean,
   "default_text"            varchar(512), 
   "default_json"            jsonb,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "conversations" (
   "id"                      serial PRIMARY KEY,
   "user_id"                 integer NOT NULL references "app_user"(id) ON DELETE CASCADE,
   "coach_id"                integer NOT NULL references "app_user"(id),
   "conversation"            jsonb,
   "created"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   "updated"                 timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE VIEW "client_assets" AS 
  select assigned_assets.id, assigned_assets.user_id, assigned_assets.asset_id, assets.asset_name, assets.asset_url 
  from assets inner join assigned_assets on assets.id = assigned_assets.asset_id 
  order by assigned_assets.user_id, assigned_assets.asset_id;


/* 
 * Trigger called AFTER UPDATE on multiple tables
 *
 * Handles  - Setting value of UPDATED field to current timestamp whenever a record is modified
 */

CREATE OR REPLACE FUNCTION update_updated_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated = now();
   RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_user_updated BEFORE UPDATE ON "app_user" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_assessment_updated BEFORE UPDATE ON "assessment" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_assessment_template_updated BEFORE UPDATE ON "assessment_template" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_assets_updated BEFORE UPDATE ON "assets" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_assigned_assets_updated BEFORE UPDATE ON "assigned_assets" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_client_log_updated BEFORE UPDATE ON "client_log" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_defaults_updated BEFORE UPDATE ON "app_defaults" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

CREATE TRIGGER update_conversations_updated BEFORE UPDATE ON "conversations" 
   FOR EACH ROW EXECUTE PROCEDURE  update_updated_column();

/* 
 * Trigger called AFTER INSERT on APP+_USERS
 *
 * Handles  - Adding "automatic" Asset Assignments from their assigned coach_id
 *          - If new user is a Coach, set default values from APP_DEFAULTS TABLE
 *          - Default application settings for each new user
 */

CREATE OR REPLACE FUNCTION handle_new_user()
RETURNS TRIGGER AS $$
BEGIN
  UPDATE app_user set settings = (select default_json from app_defaults where default_name = 'userSettings') where id = NEW.id;  
  IF (NEW.coach_id IS NOT NULL) THEN 
    INSERT INTO assigned_assets (user_id, asset_id) (
      SELECT NEW.id, id FROM assets WHERE user_id = NEW.coach_id AND asset_type = 'A'
    );
  END IF;
  IF (NEW.role = 'COACH') THEN 
    UPDATE app_user set branding = (select default_json from app_defaults where default_name = 'coachBranding') where id = NEW.id;
  END IF;
  RETURN NULL;
END;
$$ language 'plpgsql';

CREATE TRIGGER user_after_insert_trigger AFTER INSERT ON "app_user"
   FOR EACH ROW EXECUTE PROCEDURE handle_new_user();


